---
title: "Donation received - Thank you!"
---

Thank you very much for your donation to KDE!

Remember you can become a "KDE Supporting Member" by making recurring donations. Learn more at <a href="https://relate.kde.org/">relate.kde.org</a>.

You can see your donation in the [list of previous donations](/community/donations/previousdonations).
