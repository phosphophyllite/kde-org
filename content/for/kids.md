---
title: KDE for Kids
description: KDE provides a wide array of applications for children covering both educational applications and games.
layout: kids
scssFiles:
- /scss/for/kids.scss
download: Download
kturberling: "KTuberling a simple construction game suitable for children and adults alike. The idea is simple and compelling: stick the features onto the potato and build your own character!"
klettres: KLettres helps you learn an alphabet in a new language and then read simple syllables. Appropriate for young children aged from two and up, and for adults wanting to learn the basics of a foreign language.
kolourpaint: Unleash your creativity with this easy to master paint program.
games: A collection of games for kids from 6 to 99 years old!
gamestitle: Games
---

{{< container class="text-center text-small" >}}

## GCompris

An educational software suite used by teachers and parents worldwide. GCompris includes a large number of activities for children aged 2 to 10, covering everything from basic numeracy and alphabetization activities, to math and logic gates used in electronics.

{{< for/app-links download="https://gcompris.net/downloads-en.html" learn="https://gcompris.net/" centered="true" >}}

![](https://cdn.kde.org/screenshots/gcompris/gcompris.png)

{{< /container >}}

{{< section class="ada py-5" >}}

## Ada & Zangemann - A tale of software, skateboards, and raspberry ice cream

This illustrated book tells the story of the famous inventor Zangemann and the girl Ada, a curious tinkerer. Ada begins to experiment with hardware and software, and in the process realises how crucial it is for her and others to control technology.

![A three way power adaptor, a tin of paint, a pair of pliers, a mouse,  a soldering iron, a phone, and a keyboard form a circle with a thin white curly line. Inside the circle, Ada and Zangemann face each other. Zangemann is an old man with glasses and Ada is a young girl with curly black hair.](https://pics.fsfe.org/uploads/medium/94/f4/aec90b9dfe2e0f5073003fca5d05.png)

The book is currently available:

* [in English by the publisher No Starch Press](https://nostarch.com/ada-zangemann) and can be ordered from your preferred book store world-wide with the ISBN 978-1-718-50320-5.
* [in German by the publisher O'Reilly Germany / Dpunkt Verlag (affiliate link - the FSFE gets 12%)](https://oreilly.de/produkt/ada-und-zangemann/?ref=10022) and can be ordered from your preferred book store with the ISBN 978-3-96009-190-5.

{{< for/app-links learn="https://fsfe.org/activities/ada-zangemann//" dark="true" >}}

{{< /section >}}
