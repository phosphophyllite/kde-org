---
date: 2021-02-23
changelog: 5.21.0-5.21.1
layout: plasma
youtube: ahEWG4JCA1w
asBugfix: true
---

+ KRunner: Launch runners KCM in systemsettings. [Commit.](http://commits.kde.org/plasma-desktop/fbce7c7fe105f342d205a025ad1f27a4514eaa87) Fixes bug [#433101](https://bugs.kde.org/433101)
+ Powerdevil: when we get request to wakeup turn dpms on. [Commit.](http://commits.kde.org/powerdevil/d1bf7bfca40dc619255f0c89b980b7e5107938dd) 
+ SDDM KCM: Allow for easier syncing of Plasma font. [Commit.](http://commits.kde.org/sddm-kcm/56449c24560e87a9a61c242b59483b057d22917f) Phabricator Code review [D23257](https://phabricator.kde.org/D23257). Fixes bug [#432930](https://bugs.kde.org/432930)
