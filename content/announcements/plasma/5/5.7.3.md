---
aliases:
- ../../plasma-5.7.3
changelog: 5.7.2-5.7.3
date: 2016-08-02
layout: plasma
youtube: A9MtFqkRFwQ
figure:
  src: /announcements/plasma/5/5.7.0/plasma-5.7.png
  class: text-center mt-4
asBugfix: true
---

- Fixed first time initialization. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=89cb478eb205e5586751311594f2dcf4ec447199">Commit.</a>
- Speed up loading KCM with FileDialog lazy loading. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=60e970472a5ed7cb3a7e58419fe42d80412a32d7">Commit.</a>
- Don't try to load layout before kactivitymanagerd starts. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f7ef6ee87b8957bebc976b6fc9e0df279cea05f1">Commit.</a>
