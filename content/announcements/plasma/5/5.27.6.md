---
date: 2023-06-20
changelog: 5.27.5-5.27.6
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Dpms: Don't crash if trying to interact with the fake screen. [Commit.](http://commits.kde.org/libkscreen/56f0286a1adc70d56b8ce44cabe0a3ffd7ee8d9a) Fixes bug [#470484](https://bugs.kde.org/470484)
+ Panel: mitigate plasmashell high CPU usage when moving windows. [Commit.](http://commits.kde.org/plasma-desktop/14d56ef4eecea4e6623ba45409e4e4042fcb5938)
+ Runners/calculator: implement thread-safety in QalculateEngine::evaluate. [Commit.](http://commits.kde.org/plasma-workspace/9d18e0821455366c00a763252515d48741316f6c) Fixes bug [#470219](https://bugs.kde.org/470219)
+ Fix ddcutil for laptop user. [Commit.](http://commits.kde.org/powerdevil/ddcabaf93712b953e3ec327416e0332d7e44fc40) Fixes bug [#469895](https://bugs.kde.org/469895)
+ Fix showing error messages. [Commit.](http://commits.kde.org/sddm-kcm/d8ca15e88a15df26859a79a376056a222f7d63bd) Fixes bug [#471040](https://bugs.kde.org/471040)
+ Fix UI with huge window sizes. [Commit.](http://commits.kde.org/plasma-thunderbolt/d8474c7594093b9cb58ea618a1c029425a3327cb) Fixes bug [#461102](https://bugs.kde.org/461102)

