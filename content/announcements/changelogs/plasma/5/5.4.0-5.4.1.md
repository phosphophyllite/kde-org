---
aliases:
- /announcements/plasma-5.4.0-5.4.1-changelog
hidden: true
plasma: true
title: Plasma 5.4.1 complete changelog
type: fulllog
version: 5.4.1
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Only use fixed icon size for QtQuickControls. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0cce762f03a017923bc5c29935a2fa9cbd9c35a1'>Commit.</a> See bug <a href='https://bugs.kde.org/339106'>#339106</a>
- Fix rendering of disabled radio buttons in menus. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6231186d9aa12576a35d4c7f1a3ea6ff61e51d0f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352160'>#352160</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- Fix name of kickerdash applet service file. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8e5a7cd0916d037097023c0e39605d7228bdb97c'>Commit.</a>

### <a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a>

- Fix interface name. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=3551aca3f1016d44e0b8d5a6d454859ba6a89773'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124918'>#124918</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Blur transparent 24bit windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0c71d514ce217a6f0c840643970815b84fafeb55'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124148'>#124148</a>
- Never trigger edges during the cooldown. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9139cca72c5612482c8c27e70196d583630772af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351627'>#351627</a>. See bug <a href='https://bugs.kde.org/351869'>#351869</a>. Code review <a href='https://git.reviewboard.kde.org/r/124888'>#124888</a>
- Tests test tests, not code. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b5e3e4ac4c037096fe5d378b09386f52a9b240c7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124888'>#124888</a>
- Use shader traits for magnifer effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0f4c51936e8d79dd15fe82bb2ba4bb18f77252c4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124942'>#124942</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- Update currentSize in Screen::apply. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=210a7a1f5a5748e60c431a5716f0de46a2b49d48'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352001'>#352001</a>. Code review <a href='https://git.reviewboard.kde.org/r/124992'>#124992</a>

### <a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a>

- Use kdesu instead of pkexec. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=ead3de1c9808523e65d43d276941187b12609764'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125003'>#125003</a>
- Correct signal to catch closing configuration dialog. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=90e2abeddba422fb958fc9624ae2600450d5ba13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352061'>#352061</a>
- Make sure the install button has a size. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=f4009a08992d440b4aac60514e201e33e392fe47'>Commit.</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Only use fixed icon size for QtQuickControls. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=84872f2f58e4e3fa9197cc528e32d42c5f7450fa'>Commit.</a> See bug <a href='https://bugs.kde.org/339106'>#339106</a>
- Added missing painter->restore() after rendering menu button. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=45c2b211d5f73092971db63ef073115e5ea7cc69'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346955'>#346955</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Provide press feedback for Edge/SizeHandle. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6f3b4a14817d3e42917598204e6240eaa064264c'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124945'>#124945</a>
- Save default file manager in newer file location. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ebd8e972a77fe701586ab638ede3184eb4eece19'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/120564'>#120564</a>
- Save the default file manager into group used by new specification. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=151a8f85bb813145700668fb7ce0ac11283e2ba0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/120565'>#120565</a>
- Save the default email client into the group [Default Applications]. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=35c2dc0250d2b5dff52ba9b645eea0d0e2c38c0e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/120569'>#120569</a>
- Open user_manager in Kickoff. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b3941e91f26601fcef983d76543bb5a0a077dbee'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125005'>#125005</a>
- Baloo KCM: Do not generate the dbus interface. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4b3e8c53b872397b06d19f0176527aaf7ececa36'>Commit.</a>
- Fix Bug 352005 - Autostart desktop files saved to the wrong location. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e4a5188b05092b1b99b3fdb0cfac1380d2d80d8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352005'>#352005</a>
- Fix widget explorer sometimes opening on the wrong side of the screen. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=544e98da26ffc6a71f9a0a8f1c38ce505cb930d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/332702'>#332702</a>. Code review <a href='https://git.reviewboard.kde.org/r/124869'>#124869</a>
- Fix issues in translation control module. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=adb75ec59296ff17ef9d07a7cae0614235c592a6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/124885'>#124885</a>. Fixes bug <a href='https://bugs.kde.org/345761'>#345761</a>. Fixes bug <a href='https://bugs.kde.org/347956'>#347956</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Update struts on screen size change. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d66d6d57d34f3fc46a1cb7886f06dfe5b7a61763'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349400'>#349400</a>. Code review <a href='https://git.reviewboard.kde.org/r/124996'>#124996</a>
- Remember to connect the button box to the dialog. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=98e63f95e74b5d1160d1eded92a03fe70718b20a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351981'>#351981</a>
- If we remove a config action, close any open dialogs for that action. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a084eaa48408f907a8038fa626cb46f38396b9d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351748'>#351748</a>. Code review <a href='https://git.reviewboard.kde.org/r/124944'>#124944</a>
- Don't delete containment actions config dialog twice. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8b16d75ad997a882c809fb11187e71e6d5640057'>Commit.</a>

### <a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a>

- Add parentheses to make GCC 5 happy. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=dddd12f0ce7977c1665b64d77d508fa53880cad2'>Commit.</a>