---
aliases:
- /announcements/plasma-5.12.1-5.12.2-changelog
hidden: true
plasma: true
title: Plasma 5.12.2 Complete Changelog
type: fulllog
version: 5.12.2
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Don't issue empty snap find calls. <a href='https://commits.kde.org/discover/1bcf7912490eeb27ec24b0d1b1a3af9460837fcb'>Commit.</a>
- Fix checking if the package is already on the ResProxyModel. <a href='https://commits.kde.org/discover/ac7a8e3588a49336edc2043af573009f29a2cec5'>Commit.</a>
- Add multimedia sub-categories. <a href='https://commits.kde.org/discover/f3247b1c38bb9a7c1d7d5c9e7ea87e5360fdbf4b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10622'>D10622</a>
- Put all systemwide add-ons in Plasma Addons category. <a href='https://commits.kde.org/discover/4959871c57d71209fff14eaea6b31b2c1d953d13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390594'>#390594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10611'>D10611</a>
- Sort by sortable rating. <a href='https://commits.kde.org/discover/089810bd3d445f25d3827f7e7ff476227fc4066f'>Commit.</a>
- Make it possible to debug sorting. <a href='https://commits.kde.org/discover/e1159d528a2d8d94503cf5e3513541de64147aba'>Commit.</a>
- Make sure sortableRating is initialised. <a href='https://commits.kde.org/discover/df3bc3061d63727fa837661f50a478fe71233e5f'>Commit.</a>
- Don't let the user write the first review for apps they haven't installed. <a href='https://commits.kde.org/discover/01ec02e97016ec17393f09d3cb95e40eb7c21bb2'>Commit.</a>
- Fix warning. <a href='https://commits.kde.org/discover/0910f9364b9ab1132daac336a4ede8126c362c1f'>Commit.</a>
- Only instantiate the ProgressView sheet when it's to be displayed. <a href='https://commits.kde.org/discover/5852e51c87b0a45affad8f2d612aee9c1046f951'>Commit.</a>
- Fix placement of the BusyIndicator over loading screenshots. <a href='https://commits.kde.org/discover/240e8e7e130c817ab91710854c7303343f0ff41f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390391'>#390391</a>
- Update copyright year. <a href='https://commits.kde.org/discover/fed1f5da1b1f0ffc99d242721e3abc0ad6bf3d27'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390358'>#390358</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Remove dead code & translation extraction from release branch. <a href='https://commits.kde.org/kdeplasma-addons/145981b6bd488978d575b78643f6ee2e13420c39'>Commit.</a>
- [Media Frame] Use Label instead of Text. <a href='https://commits.kde.org/kdeplasma-addons/ca059f3362c7322a9f9db4bb6fe7526a3c5a0c19'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389259'>#389259</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Update size hint of lnf config widget. <a href='https://commits.kde.org/kscreenlocker/639d1809e4d3605903e0efe379f104e35a03fc38'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389483'>#389483</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Disable positioning in popup folderviews. <a href='https://commits.kde.org/plasma-desktop/65bbc75f3ad67c3b1e3d151a5ca24dd9a331d5fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10407'>D10407</a>
- Keep item highlighted when context menu opens. <a href='https://commits.kde.org/plasma-desktop/4d229146fa4c076b29af4ee6104e16966fa0ee7f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10552'>D10552</a>
- [Look and feel KCM] Fix copying color scheme data. <a href='https://commits.kde.org/plasma-desktop/6ab44dcb86b1459cad36be2a403be50c7761679d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10259'>D10259</a>
- Unbreak DND. <a href='https://commits.kde.org/plasma-desktop/17777b771cc993979b5d78b5f7ecf2dd9bb59aa3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10550'>D10550</a>
- [Application Dashboard] Fix erroneously using Plasma icons for applications. <a href='https://commits.kde.org/plasma-desktop/a2c5ffe37a8604ef72e7636571e0f7537a72e00b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390353'>#390353</a>
- [Kickoff] Enable smooth scaling for icon. <a href='https://commits.kde.org/plasma-desktop/b7e212441cb993dde1c40d2a4d389c2b29f32179'>Commit.</a>
- Workaround the touchpad toggle button not working. <a href='https://commits.kde.org/plasma-desktop/2b098f69d354982fc783a01c8002e3ef2bfc8155'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10498'>D10498</a>
- [lookandfeel kcm] Do not declare plugin in lookandfeeltool code version. <a href='https://commits.kde.org/plasma-desktop/502e8dc1080fc42bd11611704484e251c23a9307'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10485'>D10485</a>
- Fix launching by touchscreen tap. <a href='https://commits.kde.org/plasma-desktop/31bcd0ac95906186a0fd734b7ee43d15c0273975'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366527'>#366527</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10513'>D10513</a>
- Fix window pin not showing. <a href='https://commits.kde.org/plasma-desktop/840694cc5bbee301b9ddb48b7c6aee5fbb916cea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390428'>#390428</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10510'>D10510</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix favicons in firefox bookmarks runner. <a href='https://commits.kde.org/plasma-workspace/93f0f5ee2c275c3328f37675b644c1ce35f75e70'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363136'>#363136</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10610'>D10610</a>
- Remove uninstalled themerc files for no longer existing Qt themes. <a href='https://commits.kde.org/plasma-workspace/4149b474d3013521f9ceb2ee1ed48c519bf5857a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10596'>D10596</a>
- Have all krunner plugin desktop files prefixed with "plasma-runner-". <a href='https://commits.kde.org/plasma-workspace/ddfc158a6715cb32308439e963123d996bacd51f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10515'>D10515</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Fix PowerDevil shortcuts migration. <a href='https://commits.kde.org/powerdevil/4ae36ddddaee91a23dcb0736418295269da14152'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389991'>#389991</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10668'>D10668</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Improve sidebar header visibility. <a href='https://commits.kde.org/systemsettings/6f5b6e41ec4dec6af9693c3a22e5181ee850414b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384638'>#384638</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10620'>D10620</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Fix build with Qt dev branch, where QCUPSSupport::cupsOptionsList was removed. <a href='https://commits.kde.org/xdg-desktop-portal-kde/25982ef6847d42873f2970651c8105bb3ee39ad6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10345'>D10345</a>