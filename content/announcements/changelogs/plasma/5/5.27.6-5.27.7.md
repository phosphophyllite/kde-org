---
title: Plasma 5.27.7 complete changelog
version: 5.27.7
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Rpm-ostree: Improve search and filter logic. [Commit.](http://commits.kde.org/discover/a3c5cd250b0a8aa436ff1b5f43a5aa11b651308b) Fixes bug [#468162](https://bugs.kde.org/468162)
+ Fix SteamOS dbus path. [Commit.](http://commits.kde.org/discover/31e05554222ac60127cbef760153e11b2f7b6684) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Coredump-gui: don't delete qprocess in a slot that connects to it. [Commit.](http://commits.kde.org/drkonqi/d42aa10c5989533674c9b1700d1d81f2348fca1f) 
{{< /details >}}

{{< details title="KDE Window Decoration Library" href="https://commits.kde.org/kdecoration" >}}
+ Set default value for EXCLUDE_DEPRECATED_BEFORE_AND_AT. [Commit.](http://commits.kde.org/kdecoration/bb3b56015d608ba56ff88fccb745a2c3d23d692f) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: use https in apodprovider. [Commit.](http://commits.kde.org/kdeplasma-addons/be3bd4eff3ac7e1a5bf67b365f0d391d5e0400da) 
+ Revert "wallpapers/potd: remove unsplash provider". [Commit.](http://commits.kde.org/kdeplasma-addons/9f544e2fa4a95bee5e45e081bfd294651b5710ef) 
+ Wallpapers/potd: remove unsplash provider. [Commit.](http://commits.kde.org/kdeplasma-addons/365b58ed5ca0e30e207ff8127ad51b9d5482a76b) Fixes bug [#471526](https://bugs.kde.org/471526)
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ PipeWireSourceItem: proceed itemChange in QQuickItem. [Commit.](http://commits.kde.org/kpipewire/4212e7a45b789560244074097b80edb4e7334328) See bug [#472748](https://bugs.kde.org/472748)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Gpu/nvidia: Discover data fields based on headers. [Commit.](http://commits.kde.org/ksystemstats/4f7213e6e742b993feeaf300181a67923e60c0f4) Fixes bug [#470474](https://bugs.kde.org/470474)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Revert "backends/drm: use glReadPixels for CPU copy". [Commit.](http://commits.kde.org/kwin/73c112e983000a4894ec11d414a5db0350852587) Fixes bug [#472142](https://bugs.kde.org/472142)
+ Backends/drm: use glReadPixels for CPU copy. [Commit.](http://commits.kde.org/kwin/fdbcb184ae914d67483264401eaf3d5e88015fb2) 
+ Add closed window checks in restacking requests. [Commit.](http://commits.kde.org/kwin/d8debb3177007fe4119785e902f151f47d7901d1) 
+ Add closed window checks in some window activation code paths. [Commit.](http://commits.kde.org/kwin/5bf333fbd527a630f380b5cd1c5a14ef3f7cd6d6) See bug [#438315](https://bugs.kde.org/438315)
+ X11: Sync frame extents in X11 native pixels. [Commit.](http://commits.kde.org/kwin/2131c9beed0efb723b51781af7095466bd6c806e) Fixes bug [#471132](https://bugs.kde.org/471132)
+ Backends/drm: handle mismatching stride with CPU copying. [Commit.](http://commits.kde.org/kwin/f1f7e2697d1ac4ebe9099006775717e5fd6f5777) Fixes bug [#471517](https://bugs.kde.org/471517)
+ Scene: Pad damage if scale factor is fractional. [Commit.](http://commits.kde.org/kwin/739454207b4b94e7ffb938d8c0fa5c43a38596e8) 
+ Locale1: fix use-after-free in xkb_keymap creation. [Commit.](http://commits.kde.org/kwin/2d09d7961b09693aa56a99eb3ba9680e84192936) 
+ Effects: Make OpenGL context current before deleting framebuffer. [Commit.](http://commits.kde.org/kwin/ed916ff21629f3e91ee987552d778b1a65d66702) Fixes bug [#444665](https://bugs.kde.org/444665). Fixes bug [#471139](https://bugs.kde.org/471139)
+ Plugins/backgroundcontrast,blur: ensure the effect is only applied behind the window. [Commit.](http://commits.kde.org/kwin/69151896615ec272d78860b2ef42e61657f435f1) Fixes bug [#469625](https://bugs.kde.org/469625)
+ Input: don't crash if the internal handle is nullptr. [Commit.](http://commits.kde.org/kwin/21d193506851e0727860927ab289869732b06102) Fixes bug [#471285](https://bugs.kde.org/471285)
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Make sure kscreen-doctor exits when using --dpms. [Commit.](http://commits.kde.org/libkscreen/4b3bf29cd5d88b7231306df9a1d1aff234eb46ea) 
+ Guard the deprecated Output::setPrimary. [Commit.](http://commits.kde.org/libkscreen/b51a11b9030f70af2f114470bebf0ae70c082d9e) Fixes bug [#471195](https://bugs.kde.org/471195)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Fix typo. [Commit.](http://commits.kde.org/libksysguard/751784bbe77e20a92ee80b48dded0e28f32fbe6d) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/kickoff: add blocker to prevent focus change in search view. [Commit.](http://commits.kde.org/plasma-desktop/717465388631796747cd8f2061650a0abed19bee) Fixes bug [#466192](https://bugs.kde.org/466192)
+ Kickoff: Fix grid view key nav in mirrored layout. [Commit.](http://commits.kde.org/plasma-desktop/8cd6e300e2bf85c8bc41471983643ec4b917e235) 
+ Update Unicode data for emojier to 15.0. [Commit.](http://commits.kde.org/plasma-desktop/5b2be7c874caab6f178656df68af7c28682890c6) 
+ Applets/taskmanager: use hardcoded value. [Commit.](http://commits.kde.org/plasma-desktop/ec599c798d45a78a34766dc5b0df8df5fe56d704) 
+ Applets/taskmanager: make PauseAnimation wait longer. [Commit.](http://commits.kde.org/plasma-desktop/723c3ba9b39af2104844adb3455af02a503e91e2) Fixes bug [#470135](https://bugs.kde.org/470135)
+ Desktoppackage: activate panel on "Activate Panel Widget" shortcut pressed. [Commit.](http://commits.kde.org/plasma-desktop/648e10cea66926e93b52b9a7e0cc65b4055448e6) Fixes bug [#455398](https://bugs.kde.org/455398). See bug [#352476](https://bugs.kde.org/352476). See bug [#453166](https://bugs.kde.org/453166)
+ Kaccess: Use kcfg files of kcm. [Commit.](http://commits.kde.org/plasma-desktop/06d7df560528c66186b3ad9406dcd5dc95ab2c0b) 
+ Kcms/access: Match kcfg default values to old defaults/kaccess. [Commit.](http://commits.kde.org/plasma-desktop/240cd2b51d001725cb1a29227d2d015f3f009924) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Avoid double delete of TabWidget. [Commit.](http://commits.kde.org/plasma-nm/2900df32d4129fa30ca899041f77e7a40f99e7c5) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Put volume change logic centrally in VolumeSlider. [Commit.](http://commits.kde.org/plasma-pa/91dcf51a0cda029519c917c93f330a6ced531784) 
+ Make setGenericVolume keep balance between channels. [Commit.](http://commits.kde.org/plasma-pa/ffe6a4f4b6f56296165cea8651f35563d168ac89) Fixes bug [#435840](https://bugs.kde.org/435840)
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Remove wrongly-generated translations. [Commit.](http://commits.kde.org/plasma-sdk/e72b76ce689eaa10dcac38624e1aecc3f56a5c9d) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Libtaskmanager: fix unable to change order of launchers. [Commit.](http://commits.kde.org/plasma-workspace/b1711a48ea745ba3eed189fc59726abf5b5fb050) Fixes bug [#472812](https://bugs.kde.org/472812)
+ Reorder removals to ensure launcher url is deallocated after last use. [Commit.](http://commits.kde.org/plasma-workspace/e80c21211a72c6f734693819b6b841acd92d6ed9) 
+ Fix crash in LauncherTasksModel::Private::requestRemoveLauncherFromActivities. [Commit.](http://commits.kde.org/plasma-workspace/9d9f80149096a04d34cbfdd4310d15e279f32bc3) Fixes bug [#472378](https://bugs.kde.org/472378)
+ Libtaskmanager: skip empty appId when matching '*.appId.desktop'. [Commit.](http://commits.kde.org/plasma-workspace/5b59a1334c92d3edaf44b774df3fa608016913e8) Fixes bug [#472576](https://bugs.kde.org/472576). Fixes bug [#428559](https://bugs.kde.org/428559)
+ Do not write empty path to desktop file. [Commit.](http://commits.kde.org/plasma-workspace/ced23fc177a665d1c9fec64fdda0de4b0b24b7d6) Fixes bug [#433304](https://bugs.kde.org/433304)
+ Fix crash in TasksModel::move. [Commit.](http://commits.kde.org/plasma-workspace/ea4724ce000a8740a499f4677e8f7e5cc09f4c8f) Fixes bug [#472524](https://bugs.kde.org/472524)
+ Kcm/region_language: fix enum order. [Commit.](http://commits.kde.org/plasma-workspace/74cbbf543298f8f3b516d3e595d7386416a41158) Fixes bug [#472108](https://bugs.kde.org/472108)
+ Kcms/region_language: Read from $LANGUAGE in options model. [Commit.](http://commits.kde.org/plasma-workspace/833384643821ffe15885c20c4181c20e58b89c13) 
+ Kcms/region_language: Improve isSupportedLanguage detection. [Commit.](http://commits.kde.org/plasma-workspace/ddeb05534b976be6bd2d0983bee2c29929808c80) 
+ Kcms/region_language: Chop off the UTF-8 codepoint again. [Commit.](http://commits.kde.org/plasma-workspace/6b952c31f6b260244e9d189f3b130f5df211c203) 
+ Kcms/region_language: Check if the language setting is default as well. [Commit.](http://commits.kde.org/plasma-workspace/5c105a66f37e11ec9a2351bb5125e221d3bc9a1d) 
+ Kcms/region_language: Simplify the language settings read. [Commit.](http://commits.kde.org/plasma-workspace/16ee5eaa3c4bada87a9b6438aa86a127713bb28b) 
+ Kcms/region_language: Read the LANGUAGE environment variable for the default language value. [Commit.](http://commits.kde.org/plasma-workspace/9a54e092566a09ba198920cb650ffcf0c723e02c) 
+ TriangleMouseFilter: remove unnecessary return. [Commit.](http://commits.kde.org/plasma-workspace/f8df66f7b45fbdfc53db7a8559cdf8dbc54e11de) 
+ TriangleMouseFilter: make `resendHoverEvents` actually work. [Commit.](http://commits.kde.org/plasma-workspace/cc80403daa9613c10666a3c48088ff470149d712) 
+ TriangleMouseFilter: fix incorrect handling of hover events and inactive state. [Commit.](http://commits.kde.org/plasma-workspace/9bdc101051d878addad8f59521a0b0d72be9e61f) Fixes bug [#467426](https://bugs.kde.org/467426)
+ TriangularMouseFilter: Filter event everytime when there was no movement at all. [Commit.](http://commits.kde.org/plasma-workspace/959404ce00e156142ef4e5e5e6087f2b4b850661) See bug [#467426](https://bugs.kde.org/467426)
+ Improve responsiveness of triangular filter. [Commit.](http://commits.kde.org/plasma-workspace/2186fa259708ded8ce8e9441da23ecad28824593) See bug [#467426](https://bugs.kde.org/467426)
+ Shell: move `forceActiveFocus` to `PanelView`. [Commit.](http://commits.kde.org/plasma-workspace/6f0cd573dcc3634a453c8bb6b504013dc8372a95) See bug [#455398](https://bugs.kde.org/455398). See bug [#352476](https://bugs.kde.org/352476). See bug [#453166](https://bugs.kde.org/453166)
+ Applets/mediacontroller: use PauseAnimation to delay showing busy indicator. [Commit.](http://commits.kde.org/plasma-workspace/a34cf8e90d7eb1d27185f24007b29490f6311ac5) See bug [#471248](https://bugs.kde.org/471248)
+ Applets/mediacontroller: remove duplicate opacity animation. [Commit.](http://commits.kde.org/plasma-workspace/7e2e98eae8761bf3fc8e2ca702410c8bcd76408d) See bug [#471248](https://bugs.kde.org/471248)
+ Applets/notifications: add workaround for QTBUG-100392. [Commit.](http://commits.kde.org/plasma-workspace/b61ef42d83cfc27cd0d70a55c309163571e69e84) Fixes bug [#468180](https://bugs.kde.org/468180)
+ Notifications: Also try X-SnapInstanceName for desktop file resolution. [Commit.](http://commits.kde.org/plasma-workspace/c842272b634736c16dea6c33c8026aae35600bc8) 
+ Libtaskmanager: fix a potential leak. [Commit.](http://commits.kde.org/plasma-workspace/2f67e4b63d664490310e485d59fdd500b7eb0bc5) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Fix loading theme.conf.user. [Commit.](http://commits.kde.org/sddm-kcm/05d2cf913f42a783bd4e3d0823761da25960183d) Fixes bug [#436272](https://bugs.kde.org/436272)
+ Fix multi-line strings in QML file. [Commit.](http://commits.kde.org/sddm-kcm/2aae3b01e5eb03b73d78fb98675bda7fea6370c9) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Properly show the custom header widget when started with non-QML KCMs. [Commit.](http://commits.kde.org/systemsettings/3d306d1ef5446db5925c887964ab75b2c82a2701) 
{{< /details >}}

