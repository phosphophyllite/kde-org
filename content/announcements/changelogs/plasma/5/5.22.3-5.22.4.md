---
title: Plasma 5.22.4 complete changelog
version: 5.22.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ [applet] Give FullRepresentation an implicit size. [Commit.](http://commits.kde.org/bluedevil/83b91913d7d5683521f85e9acdc2d7f9c1028573) Fixes bug [#439981](https://bugs.kde.org/439981)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Kns: Use ksplash.knsrc for the test. [Commit.](http://commits.kde.org/discover/50327ae72be083f96d7f38d8c5095038f91cf0fe) 
+ Address the keyboard shortcut tooltip. [Commit.](http://commits.kde.org/discover/a7fcb85803b151f5e36c399453fa0bc739853911) Fixes bug [#438916](https://bugs.kde.org/438916)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Use dynamic sorting to support different locales. [Commit.](http://commits.kde.org/kdeplasma-addons/586d0ef8fd85125c57b2137d6e8a80dccdd8951e) 
+ Sort Unsplash POTD image categories alphabetically. [Commit.](http://commits.kde.org/kdeplasma-addons/ed7c2c218ee3cae72115f7e66ee57ee2878bdda8) Fixes bug [#422971](https://bugs.kde.org/422971)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Unbreak about CPU value when solid is missing a product string. [Commit.](http://commits.kde.org/kinfocenter/4cfd889c6fe3aa27b8eb99ebc104d4eb2fb39b10) Fixes bug [#439464](https://bugs.kde.org/439464)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Handle ConvPutAuthAbort as an authentication failure. [Commit.](http://commits.kde.org/kscreenlocker/fca315cf72826f93eda7a026016b33818b9d1f39) Fixes bug [#438099](https://bugs.kde.org/438099)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Gpu: Only scan pci devices. [Commit.](http://commits.kde.org/ksystemstats/6b255d80dd82bb4aada98b5132065934b5043530) 
+ Don't crash if there's no input subfeature. [Commit.](http://commits.kde.org/ksystemstats/782dbe81acaa3d364d12c1e42e44fc90c447df22) Fixes bug [#439615](https://bugs.kde.org/439615)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Emit the committed() signal after the state is applied. [Commit.](http://commits.kde.org/kwayland-server/8e851ee77e06af909d52ca45eb14cb7d2969735f) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Wayland: Force decoration update. [Commit.](http://commits.kde.org/kwin/c6d61f5f7966ec7df3b8e27ca56ed005fe1296d4) 
+ Scripting: Make QTimer constructible. [Commit.](http://commits.kde.org/kwin/71a1ff04b55ee62cdc2b37583c1ade6a7297847c) Fixes bug [#439630](https://bugs.kde.org/439630)
+ Platforms/drm: don't delete connectors in DrmGpu::removeOutput. [Commit.](http://commits.kde.org/kwin/54ce400764184eee067dc4f3d8d81cee2ec25537) Fixes bug [#438789](https://bugs.kde.org/438789)
+ Platforms/drm: always populate atomic req with connector props. [Commit.](http://commits.kde.org/kwin/a6b318537afdc10ad1d4c93f784867b95da08de4) 
+ Platforms/drm: only show cursor on dpms on if not hidden. [Commit.](http://commits.kde.org/kwin/4e81abfc11419684bd25f65a7a9058659071488c) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ [History Runner] Request favicon with device pixel ratio. [Commit.](http://commits.kde.org/plasma-browser-integration/92f5361c1a59c39968abc2b23ed21980aa4d429a) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [panel] When in adaptive transparency go transparent when in show desktop mode. [Commit.](http://commits.kde.org/plasma-desktop/5e0b8c1f37b97df6a1c30ba326f806a74b31af64) Fixes bug [#439209](https://bugs.kde.org/439209)
+ Kcms/keys: Use storageId instead of desktopEntryName. [Commit.](http://commits.kde.org/plasma-desktop/b2797b48eb38ea931cc54255b52219100c74a1b9) Fixes bug [#438204](https://bugs.kde.org/438204)
+ Kcm/keys: Always try looking up services by storageId. [Commit.](http://commits.kde.org/plasma-desktop/d451d703f3dc208bbc21fb498053245fbcd0a75b) 
+ Fix shift-action modifier in context menu. [Commit.](http://commits.kde.org/plasma-desktop/48ecda47defb671b654ee9633b2a8e623699e7d5) Fixes bug [#425997](https://bugs.kde.org/425997)
+ [kcms/keyboard] Fix translations of OSD config. [Commit.](http://commits.kde.org/plasma-desktop/306f6f200b6e1f9a2786e02a0019d36537d9c98f) Fixes bug [#439394](https://bugs.kde.org/439394)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ [ListItemBase] Open menu on press. [Commit.](http://commits.kde.org/plasma-pa/aafd1a3fad3530db59e87ec92424d14554f3e952) 
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Make sure the closed signal is emitted upon close. [Commit.](http://commits.kde.org/plasma-phone-components/c8771519e47ffa766005aac2b61e1d60fd52315b) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Systemtray: Let plasmashell handle visibility when configuring. [Commit.](http://commits.kde.org/plasma-workspace/b5a791bd483ef141607cb19a8e0894ed9fb6b11a) Fixes bug [#440263](https://bugs.kde.org/440263)
+ [applets/clipboard] Fix the alignment of delegate buttons. [Commit.](http://commits.kde.org/plasma-workspace/a136c4c65f632e50f4917c7370355e9fe1625a50) Fixes bug [#437044](https://bugs.kde.org/437044)
+ Allow plasmashell to use both screenshot and screenshot2. [Commit.](http://commits.kde.org/plasma-workspace/4a357eca147c920cd4d248e216263f6ab1e61a98) 
+ [Notifications] Set Plasma linkColor on notification label. [Commit.](http://commits.kde.org/plasma-workspace/7c1f57c5d509e3069e344dfa5392a04d690e4618) Fixes bug [#438366](https://bugs.kde.org/438366)
+ Add kde-baloo.service to Wants=. [Commit.](http://commits.kde.org/plasma-workspace/b018181af88d5d7569c910241c09eb8ede623857) 
+ Prevent fractional positioning in systray HiddenItemsView. [Commit.](http://commits.kde.org/plasma-workspace/32f798f76247fd7a5d2b1d3c9c87c6d9ed31402c) 
+ Set GDK scale explictily on wayland. [Commit.](http://commits.kde.org/plasma-workspace/14cd19138cd514ea753a842d01bd2265ee8ad0b8) Fixes bug [#438971](https://bugs.kde.org/438971)
{{< /details >}}

