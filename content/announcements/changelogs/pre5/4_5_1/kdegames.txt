------------------------------------------------------------------------
r1168421 | coates | 2010-08-26 17:07:00 +0100 (dj, 26 ago 2010) | 8 lines

Initialise KGameDifficulty before calling setupGUI() in Kollision.

Backport of 1168420.

XmlGuiWindow only knows about actions created before the call to
setupGUI(). It can't properly manage any actions created after that
point.

------------------------------------------------------------------------
r1168419 | coates | 2010-08-26 16:59:14 +0100 (dj, 26 ago 2010) | 8 lines

Initialise KGameDifficulty before calling setGUI() in KBattleShip.

Backport of 1168417.

XmlGuiWindow only knows about actions created before the call to
setupGUI(). It can't properly manage any actions created after that
point.

------------------------------------------------------------------------
r1168412 | coates | 2010-08-26 16:44:44 +0100 (dj, 26 ago 2010) | 8 lines

Initialise KGameDifficulty before calling setGUI() in KNetwalk.

XmlGuiWindow only knows about actions created before the call to
setGUI(). It can't properly manage any actions created after that point.

BUG:249074
FIXED-IN:4.5.1

------------------------------------------------------------------------
r1166724 | wrohdewald | 2010-08-22 17:41:39 +0100 (dg, 22 ago 2010) | 5 lines

fix appDataDir() again: The server does not initialize
KDE, so KGlobal() knows the application as kde instead of
kajongg. So we do not use KGlobal() for the server, only
$KDEHOME

------------------------------------------------------------------------
r1166713 | wrohdewald | 2010-08-22 15:14:14 +0100 (dg, 22 ago 2010) | 3 lines

The server did not always find the correct KDE directory.
Fixing KDE bug 248688

------------------------------------------------------------------------
r1166070 | wrohdewald | 2010-08-20 19:32:44 +0100 (dv, 20 ago 2010) | 3 lines

Server, DeferredBlock: Enforce correct usage, thus eliminating
a major memory leak with degradation of server responsiveness

------------------------------------------------------------------------
r1165984 | wrohdewald | 2010-08-20 14:50:19 +0100 (dv, 20 ago 2010) | 3 lines

Errors while determining answer to server were suppressed,
now they are shown and the client aborts

------------------------------------------------------------------------
r1165983 | wrohdewald | 2010-08-20 14:49:11 +0100 (dv, 20 ago 2010) | 2 lines

Human player robbing the kong aborted the game

------------------------------------------------------------------------
r1165982 | wrohdewald | 2010-08-20 14:48:30 +0100 (dv, 20 ago 2010) | 2 lines

Server: do not ignore client failures

------------------------------------------------------------------------
r1165976 | wrohdewald | 2010-08-20 14:45:54 +0100 (dv, 20 ago 2010) | 2 lines

Sound: sometimes the own sound was not played

------------------------------------------------------------------------
r1165964 | wrohdewald | 2010-08-20 14:44:36 +0100 (dv, 20 ago 2010) | 5 lines

only enable hand of active player while she is allowed to
select a tile, disable hand after discarding. Do not wait
until next player gets active (we wait until all other players
said "no claim" or timeout happens)

------------------------------------------------------------------------
r1165956 | wrohdewald | 2010-08-20 14:42:48 +0100 (dv, 20 ago 2010) | 1 line

when starting a game against localhost, the last used ruleset was not always found
------------------------------------------------------------------------
r1164531 | scripty | 2010-08-17 03:39:00 +0100 (dt, 17 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
