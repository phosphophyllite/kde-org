2008-02-20 11:02 +0000 [r777286]  cguthrie

	* branches/KDE/3.5/kdelibs/kate/part/katesupercursor.cpp: Revert
	  r772352 as it causes an error when pasting multi-line text
	  blocks, leaving the caret at the end of the first line of the
	  pasted block. BUG: 158069 CCBUG: 146300

2008-03-10 14:52 +0000 [r784050]  dfaure

	* branches/KDE/3.5/kdelibs/kdecore/tests/kurltest.cpp: yes, it
	  worked the same in kde3

2008-03-12 00:20 +0000 [r784663]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp: Launch kdeinit
	  with --new-startup also here. Fixes e.g. medianotifier being
	  loaded outside of KDE.

2008-03-14 18:32 +0000 [r785684]  lunakl

	* branches/KDE/3.5/kdelibs/dcop/dcopserver.cpp: Shut down kded when
	  dcopserver gets SIGTERM (i.e. dcopserver_shutdown). This avoids
	  the 10 seconds delay when doing dcopserver_shutdown when kdeinit
	  was launched with --suicide (i.e. outside of KDE). I checked that
	  #97716 remains fixed.

2008-03-17 14:13 +0000 [r786635]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/netwm.cpp: Fix error that leads
	  to skipping the last icon in the set (bnc:371677).

2008-03-20 17:01 +0000 [r788097]  ggarand

	* branches/KDE/3.5/kdelibs/khtml/rendering/render_block.cpp,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_object.h,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_layer.cpp,
	  branches/KDE/3.5/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_object.cpp,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_flow.cpp,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_box.cpp: backport
	  626225, 626425, 626451, 626630 to fix the ennoying double
	  scrollbar bug. CCBUG: 138874

2008-03-20 17:10 +0000 [r788099]  ggarand

	* branches/KDE/3.5/kdelibs/khtml/misc/htmlattrs.c,
	  branches/KDE/3.5/kdelibs/khtml/misc/htmlattrs.in,
	  branches/KDE/3.5/kdelibs/khtml/misc/htmlattrs.h,
	  branches/KDE/3.5/kdelibs/khtml/html/html_elementimpl.cpp,
	  branches/KDE/3.5/kdelibs/khtml/html/html_baseimpl.cpp: and also
	  the youtube miniature thingy because my daughter says so
	  CCBUG:140711

2008-03-27 19:59 +0000 [r790900]  bram

	* branches/KDE/3.5/kdelibs/kate/data/haskell.xml: Highlight
	  'import'.

2008-03-31 16:07 +0000 [r792190]  lunakl

	* branches/KDE/3.5/kdebase/kde3 (added),
	  branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp,
	  branches/KDE/3.5/kdebase/Makefile.am.in: Make KDE3 applications
	  restart in saved session using a wrapper script 'kde3' if not
	  running in a KDE3 session
	  (http://lists.kde.org/?t=120569055200005&r=1&w=2).

2008-04-02 17:02 +0000 [r792956]  porten

	* branches/KDE/3.5/kdelibs/kparts/browserrun.cpp: Protect against
	  file names with a '%' encoding character in them.

2008-04-05 17:42 +0000 [r793952]  orlovich

	* branches/KDE/3.5/kdelibs/khtml/rendering/render_object.h,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_container.cpp,
	  branches/KDE/3.5/kdelibs/khtml/rendering/render_object.cpp:
	  Backport Allan's r786289: "Protect anonymous blocks from being
	  deleted while they are actively insterting a new child"
	  BUG:160249 BUG:160292 BUG:160388

2008-04-14 18:53 +0000 [r797027]  dfaure

	* branches/KDE/3.5/kdelibs/kwallet/backend/kwalletbackend.cc: Fix
	  assert due to KGlobal::dirs()->saveLocation("kwallet") called
	  before defining the "kwallet" resource. BUG: 160817

2008-04-21 17:05 +0000 [r799487]  dfaure

	* branches/KDE/3.5/kdelibs/dcop/dcopidl/Makefile.am,
	  branches/KDE/3.5/kdelibs/dcop/dcopidl2cpp/Makefile.am: Apply
	  patch from 151095, to fix "dcop kdelibs 3.5.8 compile errors,
	  missing -lqt-mt". I guess some recent versions of automake don't
	  support file-global AM_LDFLAGS. In fact it would have been more
	  common to write $(all_libraries) directly in those changed lines,
	  but well, this patch has been tested so I'm not taking chances.
	  BUG: 151095

2008-04-23 00:16 +0000 [r799980]  pino

	* branches/KDE/3.5/kdelibs/kate/data/debianchangelog.xml: give a
	  name to the default context

2008-04-23 09:01 +0000 [r800078]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/ktempfile.cpp: Do not paranoidly
	  sync every time, it causes I/O performance problems for some
	  users. People who still want it for whatever reason like using
	  XFS can set $KDE_EXTRA_FSYNC to 1.
	  (http://lists.kde.org/?l=kde-devel&m=120880682813170&w=2)

2008-04-24 08:00 +0000 [r800416]  pino

	* branches/KDE/3.5/kdelibs/kate/data/debiancontrol.xml: give a name
	  to the default context

2008-04-25 23:57 +0000 [r801222]  mueller

	* branches/KDE/3.5/kdelibs/kinit/start_kdeinit.c: fix
	  CVE-2008-1671: integer overflows and arbitrary process kill
	  vulnerability

2008-04-30 08:22 +0000 [r802657]  lunakl

	* branches/KDE/3.5/kdelibs/kdesu/process.cpp,
	  branches/KDE/3.5/kdelibs/kdesu/process.h: Backport readAll() from
	  KDE4 to prevent kdesu -t from splitting long lines (bnc#335507).
	  Quite poor naming, readLine() for something that doesn't
	  necessarily read a line and readAll() for something that may not
	  read all :-/.

2008-04-30 08:30 +0000 [r802658]  lunakl

	* branches/KDE/3.5/kdelibs/kdesu/process.cpp: And I doubt this only
	  matched at line starts.

2008-05-05 17:34 +0000 [r804313]  tyrerj

	* branches/KDE/3.5/kdelibs/pics/crystalsvg/cr32-key_enter.png
	  (added): BUG: 111915 32x32 icon made for "key_enter"

2008-05-14 13:51 +0000 [r807710]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp: I hate C APIs.

2008-05-15 23:30 +0000 [r808196]  staniek

	* branches/KDE/3.5/kdelibs/win/kde_file_win.c: make calls like
	  KDE_stat("C:\") work

2008-05-17 21:56 +0000 [r808943]  aacid

	* branches/KDE/3.5/kdelibs/kdecore/all_languages.desktop: these two
	  were missing

2008-05-20 08:48 +0000 [r810190]  rpedersen

	* branches/KDE/3.5/kdelibs/kate/data/ruby.xml: Fixed parsing of
	  division operator and regular expression delimiters. BUG: 134517

2008-05-20 09:22 +0000 [r810224]  rpedersen

	* branches/KDE/3.5/kdelibs/kate/data/ruby.xml: Added myself as
	  author, because I have made quite a few changes.

2008-05-20 21:14 +0000 [r810486]  rpedersen

	* branches/KDE/3.5/kdelibs/kate/data/ruby.xml: Check division
	  operator contexts doesn't eat whitespace when there's no match.
	  This fixes a problem with comments. BUG: 134517

2008-06-02 12:25 +0000 [r815639]  rpedersen

	* branches/KDE/3.5/kdelibs/kate/data/ruby.xml: Do not require
	  white-space or word boundary in front of comments. These are all
	  interpreted as comments by ruby: foo =# comment foo!#comment 10
	  +#comment etc. Remove some hacks that are not necessary anymore.

2008-06-09 12:47 +0000 [r818733]  dfaure

	* branches/KDE/3.5/kdelibs/kioslave/ftp/ftp.cc: Backport fix for
	  Bug 127793: Renaming in FTP overwrites existing files without
	  confirmation

2008-06-11 09:52 +0000 [r819455]  pino

	* branches/KDE/3.5/kdelibs/kdeui/kdepackages.h: Update the
	  application list. (KDE 3.5) CCBUG: 157724

2008-06-12 10:16 +0000 [r819841]  pino

	* branches/KDE/3.5/kdelibs/kate/data/debiancontrol.xml: set the
	  email regexps as minimal

2008-06-23 23:23 +0000 [r823745]  bettio

	* branches/KDE/3.5/kdelibs/kate/data/c.xml: Added missing C99
	  standard types: (u)intN_t and wchar_t. See also stdint.h and
	  wchar.h.

2008-06-28 06:47 +0000 [r825406]  ossi

	* branches/KDE/3.5/kdelibs/kdecore/kgrantpty.c,
	  branches/KDE/3.5/kdelibs/kdecore/kpty.cpp: cleanup PTY control
	  char magic. patch by CCMAIL: Ed Schouten <ed@80386.nl>

2008-06-29 22:28 +0000 [r826163]  anagl

	* branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.graphics.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-makefile.desktop,
	  branches/KDE/3.5/kdelibs/kparts/kpart.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.formula.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-type1.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/msword.desktop,
	  branches/KDE/3.5/kdelibs/kio/misc/kpac/proxyscout.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-objcsrc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/rtf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.writer-global.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-pn-realaudio-plugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-krita.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.writer.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-pak.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/sgml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-applixgraphics.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/illustrator.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pgp-signature.desktop,
	  branches/KDE/3.5/kdelibs/kdeprint/kdeprintd.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-python.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-vnd.kde.kplato.desktop,
	  branches/KDE/3.5/kdelibs/arts/kde/mcop-dcop/kmcop.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-awk.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/uri/mms.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-ogm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tar.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-core.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.draw.template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-wav.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/xml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/multipart/x-mixed-replace.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-latex.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/mbox.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-graphite.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-tcl.desktop,
	  branches/KDE/3.5/kdelibs/kate/plugins/wordcompletion/ktexteditor_docwordcompletion.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tex-pk.desktop,
	  branches/KDE/3.5/kdelibs/khtml/khtmlimage.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/avi.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-adasrc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/cgm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-speex.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/inode/chardevice.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-vnd.kde.kexi.desktop,
	  branches/KDE/3.5/kdelibs/kabc/plugins/dir/dir.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-rgb.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/mp4.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-xcf-gimp.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-mplayer2.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-mp2.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-sharedlib.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-python.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-vorbis.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-musepack.desktop,
	  branches/KDE/3.5/kdelibs/kabc/plugins/net/net.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-gzip.desktop,
	  branches/KDE/3.5/kdelibs/kparts/tests/notepad.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-python-bytecode.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.spreadsheet-template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-dbase.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-pn-realaudio.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-webarchive.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-troff.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-ruby.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-msod.desktop,
	  branches/KDE/3.5/kdelibs/kspell2/plugins/aspell/kspell_aspell.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/calendar.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kspread.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tgif.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/inode/directory-locked.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-dds.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/ktexteditor/ktexteditorplugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.text-template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.writer.master.desktop,
	  branches/KDE/3.5/kdelibs/kabc/plugins/sql/sql.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-hdr.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/docbook.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/vorbis.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/png.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-bibtex.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/fax-g3.desktop,
	  branches/KDE/3.5/kdelibs/kio/kpasswdserver.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/tiff.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/uri/rtspt.desktop,
	  branches/KDE/3.5/kdelibs/kspell2/plugins/ispell/kspell_ispell.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-arc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-zoo.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/model/vrml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-targa.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/inode/socket.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/gif.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-vcalendar.desktop,
	  branches/KDE/3.5/kdelibs/kspell2/kspellclient.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-tex.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kpresenter.desktop,
	  branches/KDE/3.5/kdelibs/kioslave/bzip2/kbzip2filter.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-egon.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-ms-wmv.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/inode/directory.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-deb.desktop,
	  branches/KDE/3.5/kdelibs/kparts/krop.desktop,
	  branches/KDE/3.5/kdelibs/khtml/java/kjavaappletviewer.desktop,
	  branches/KDE/3.5/kdelibs/kio/kcmodule.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-chdr.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-perl.desktop,
	  branches/KDE/3.5/kdelibs/kabc/plugins/evolution/evolution.desktop,
	  branches/KDE/3.5/kdelibs/kabc/kabc_manager.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.graphics-template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-raw.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.presentation.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-xliff.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pgp-encrypted.desktop,
	  branches/KDE/3.5/kdelibs/kabc/kab2kabc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-matroska.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/pjpeg.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-midi.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/allfiles.desktop,
	  branches/KDE/3.5/kdelibs/kcert/kcertpart.desktop,
	  branches/KDE/3.5/kdelibs/kresources/kresources.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.math.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-portable-greymap.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pgp-keys.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-compress.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/msexcel.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-applixspread.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.ms-excel.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-ttc.desktop,
	  branches/KDE/3.5/kdelibs/kio/kcomprfilter.desktop,
	  branches/KDE/3.5/kdelibs/kio/application.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-msaccess.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-log.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-zip.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-java.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.ms-powerpoint.desktop,
	  branches/KDE/3.5/kdelibs/kio/kfileplugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/svg-xml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/uri/mmst.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/khexedit/kbytesedit.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-vcard.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.impress.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-java.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-dvi.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/xhtml+xml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-pascal.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.calc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-c++hdr.desktop,
	  branches/KDE/3.5/kdelibs/kio/misc/kssld/kssld.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-applixword.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kscript/sample/shellscript.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pkcs10.desktop,
	  branches/KDE/3.5/kdelibs/kparts/browserview.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/all.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-oggflac.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.calc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kontour.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-portable-bitmap.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/mspowerpoint.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kscript/scriptinterface.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-object.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-desktop.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-msvideo.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/vnd.rn-realvideo.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-quattropro.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-x509-ca-cert.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-sqlite3.desktop,
	  branches/KDE/3.5/kdelibs/kate/data/katepart.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-amipro.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/message/news.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/quicktime.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/xsd.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/relaxng.desktop,
	  branches/KDE/3.5/kdelibs/kio/kscan.desktop,
	  branches/KDE/3.5/kdelibs/kio/misc/kio_uiserver.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-afm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-lha.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tgz.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.impress.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-vnd.kde.kugar.mixed.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-csv.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kchart.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/wordperfect.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kimproxy/interface/dcopinstantmessenger.desktop,
	  branches/KDE/3.5/kdelibs/kio/kdatatool.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/uri/pnm.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/ktexteditor/ktexteditoreditor.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-scpls.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/uninstall.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-xpm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-shockwave-flash.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-pw.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-msmetafile.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-rar-compressed.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/vnd.wap.wml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-php.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tex-gf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-shellscript.desktop,
	  branches/KDE/3.5/kdelibs/kio/kurifilterplugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/enriched.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-bmp.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-bdf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kudesigner.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-mswinurl.desktop,
	  branches/KDE/3.5/kdelibs/kabc/plugins/ldapkio/ldapkio.desktop,
	  branches/KDE/3.5/kdelibs/kresources/kresources_plugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/ogg.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-csrc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tarz.desktop,
	  branches/KDE/3.5/kdelibs/kresources/kresources_manager.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.draw.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-adpcm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-portable-pixmap.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-gzdvi.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-theora.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-cda.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kregexpeditor/kregexpeditor.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/jpg.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.draw.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-jng.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.chart.desktop,
	  branches/KDE/3.5/kdelibs/kded/kdedmodule.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/vnd.rn-realaudio.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/octet-stream.desktop,
	  branches/KDE/3.5/kdelibs/kate/plugins/isearch/ktexteditor_isearch.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kmediaplayer/kmediaplayerengine.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-pkcs12.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/css.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-xbm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-eps.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/sieve.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/inode/fifo.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-zerosize.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-lyx.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-exr.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/html.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-flic.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-speedo.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kword.desktop,
	  branches/KDE/3.5/kdelibs/kdeui/kdetrayproxy/kdetrayproxy.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-abiword.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-cpio.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-magicpoint.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.mozilla.xul+xml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-bittorrent.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-mp3.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/mp4.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.base.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-linguist.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/jpeg.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-ms-wma.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tbz.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-katefilelist.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-hancomword.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-c++src.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-flac.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-aiff.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/mpegurl.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-zip-compressed.desktop,
	  branches/KDE/3.5/kdelibs/kio/misc/kwalletd/kwalletd.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-wmf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kugar.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-pcx.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/ktexteditor/ktexteditor.desktop,
	  branches/KDE/3.5/kdelibs/kio/kfile/kpropsdlgplugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-tzo.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-ghostscript.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-mod.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kformula.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/inode/block.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pgp.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pkcs7-signature.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.text.desktop,
	  branches/KDE/3.5/kdelibs/khtml/kmultipart/kmultipart.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-gettext.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-diff.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-otf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/smil.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.calc.template.desktop,
	  branches/KDE/3.5/kdelibs/kspell2/plugins/hspell/kspell_hspell.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-vnd.adobe.photoshop.desktop,
	  branches/KDE/3.5/kdelibs/kio/tests/dummymeta.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/xml-dtd.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-bzip.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/uri/rtspu.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-hex.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pkcs7-mime.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.writer.template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-pcf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-arj.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-xfig.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/aac.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.presentation-template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-trash.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/message/rfc822.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-siag.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-xcursor.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-mng.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/mpeg.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-ace.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kde-wallet.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-ico.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-mimearchive.desktop,
	  branches/KDE/3.5/kdelibs/arts/knotify/knotify.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-ica.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-objchdr.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-rpm.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-java-jnlp-file.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-ldif.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/ac3.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-vnd.trolltech.qpicture.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.writer.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-designer.desktop,
	  branches/KDE/3.5/kdelibs/kparts/krwp.desktop,
	  branches/KDE/3.5/kdelibs/kioslave/http/http_cache_cleaner.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/uninstall.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-java-applet.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/postscript.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/prs.sid.desktop,
	  branches/KDE/3.5/kdelibs/kabc/formats/binary.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-jar.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-ogg.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/java.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.image.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.sun.xml.impress.template.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kivio.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.rn-realmedia.desktop,
	  branches/KDE/3.5/kdelibs/kate/plugins/autobookmarker/ktexteditor_autobookmarker.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-archive.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-rar.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-djvu.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-ms-asf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-snf.desktop,
	  branches/KDE/3.5/kdelibs/kabc/plugins/file/file.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-7z.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-bz2dvi.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-font-ttf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-kcsrc.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kspeech/dcoptexttospeech.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/uri/mmsu.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.ms-word.desktop,
	  branches/KDE/3.5/kdelibs/khtml/khtml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/basic.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-gzpostscript.desktop,
	  branches/KDE/3.5/kdelibs/kio/renamedlgplugin.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/xml.desktop,
	  branches/KDE/3.5/kdelibs/interfaces/kmediaplayer/kmediaplayer.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/multipart/mixed.desktop,
	  branches/KDE/3.5/kdelibs/kioslave/gzip/kgzipfilter.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.stardivision.chart.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.ms-asf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/x-mpegurl.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-debian-package.desktop,
	  branches/KDE/3.5/kdelibs/kutils/kplugininfo.desktop,
	  branches/KDE/3.5/kdelibs/kate/plugins/insertfile/ktexteditor_insertfile.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/audio/mpeg.desktop,
	  branches/KDE/3.5/kdelibs/kded/test/test.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-sqlite2.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/uninstall.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-perl-module.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-bzip2.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-djvu-2.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/vnd.abc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-mswrite.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/x-photo-cd.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/x-moc.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/fits.desktop,
	  branches/KDE/3.5/kdelibs/kate/scripts/jstest.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/vnd.oasis.opendocument.spreadsheet.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/pdf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/jp2.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/svg+xml.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-gnumeric.desktop,
	  branches/KDE/3.5/kdelibs/kioslave/http/kcookiejar/kcookiejar.desktop,
	  branches/KDE/3.5/kdelibs/kate/plugins/kdatatool/ktexteditor_kdatatool.desktop,
	  branches/KDE/3.5/kdelibs/kdeprint/tools/escputil/escputil.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/plain.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/image/fits.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-executable.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/rss.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-executable-script.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-lzop.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/video/x-matroska.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/text/rdf.desktop,
	  branches/KDE/3.5/kdelibs/mimetypes/application/x-troff-man.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-24 09:41 +0000 [r837226]  lunakl

	* branches/KDE/3.5/kdelibs/kdecore/kapplication.cpp: The
	  KDE_FULL_SESSION property is always on root window of screen 0.
	  BUG: 166598

2008-08-18 09:52 +0000 [r848634]  lunakl

	* branches/KDE/3.5/kdelibs/kdeprint/management/kmjobviewer.cpp:
	  Don't cancel shutdown (AKA ++systray_sucks).

2008-08-19 09:08 +0000 [r849216]  duffeck

	* branches/KDE/3.5/kdelibs/kio/kpasswdserver/kpasswdserver.cpp: Fix
	  the authentication-dialogue being put behind the application's
	  window. BUG:121803

2008-08-19 19:48 +0000 [r849600]  coolo

	* branches/KDE/3.5/kdelibs/kdelibs.lsm: update for 3.5.10

2008-08-19 19:57 +0000 [r849617]  coolo

	* branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdelibs/README: update for 3.5.10

2008-08-20 11:49 +0000 [r849884]  coolo

	* branches/KDE/3.5/kdelibs/kdeui/kdepackages.h,
	  branches/KDE/3.5/kdelibs/kdeui/Makefile.am: updated packages

