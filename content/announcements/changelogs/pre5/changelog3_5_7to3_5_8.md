---
aliases:
- ../changelog3_5_7to3_5_8
hidden: true
title: KDE 3.5.8 Changelog
---

<h2>Changes in KDE 3.5.8</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="3_5_8/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="core libs">core libs</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Small performance improvements.</li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Reworked KCmdLineArgs::makeURL to make "kpdf a:b" work when a:b is an existing file in the current directory. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=111760">111760</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=146105">146105</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=669594&amp;view=rev">669594</a>. </li>
        <li class="bugfix ">Be more robust against addressbar spoofing. See SVN commit <a href="http://websvn.kde.org/?rev=700053&amp;view=rev">700053</a>. </li>
        <li class="bugfix ">A hostname may resolve to multiple addresses. When connection to one timeouts, we should not immediately give up. Instead, we try the other addresses. See SVN commit <a href="http://websvn.kde.org/?rev=715274&amp;view=rev">715274</a>. </li>
      </ul>
      </div>
      <h4><a name="http ioslave">http ioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash on broken servers. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=130104">130104</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=689709&amp;view=rev">689709</a>. </li>
        <li class="bugfix ">Fix Connection failed Url Frame Redirection. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=124593">124593</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707523&amp;view=rev">707523</a>. </li>
        <li class="bugfix ">Fix session cookies not always accepted. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=86208">86208</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=708880&amp;view=rev">708880</a>. </li>
        <li class="bugfix ">Fix persistent cookies are treated like session cookies. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145244">145244</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=709516&amp;view=rev">709516</a>. </li>
        <li class="bugfix ">Fix a cross-site cookie injection vulnerability. See SVN commit <a href="http://websvn.kde.org/?rev=707375&amp;view=rev">707375</a>. </li>
        <li class="bugfix ">Fix for correctly propagating HTTP response headers &gt; 8K. See SVN commit <a href="http://websvn.kde.org/?rev=717341&amp;view=rev">717341</a>. </li>
        <li class="bugfix ">Fix http responses containing just a header cause Konqueror to wait indefinitely for a response. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147812">147812</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707413&amp;view=rev">707413</a>. </li>
        <li class="bugfix ">Fix problem with cookies in konqueror when server specified as IP adress. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134753">134753</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707428&amp;view=rev">707428</a>. </li>
        <li class="bugfix ">Fix per-site cookie policy does not apply to cookies set for subdomains. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=135175">135175</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707873&amp;view=rev">707873</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">KHTML</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix handling of font-variant:inherit. See SVN commit <a href="http://websvn.kde.org/?rev=681835&amp;view=rev">681835</a>. </li>
        <li class="bugfix ">Fix GMail/Firefox yet again. See SVN commit <a href="http://websvn.kde.org/?rev=668699&amp;view=rev">668699</a>. </li>
        <li class="bugfix ">Fix confused cursor shapes if mail and new-window links appear on the same page. See SVN commit <a href="http://websvn.kde.org/?rev=671262&amp;view=rev">671262</a>. </li>
        <li class="bugfix ">Fix cannot type text into boxes on riteaid.com. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=141230">141230</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=681861&amp;view=rev">681861</a>. </li>
        <li class="bugfix ">Optimize descendant selector matching. See SVN commit <a href="http://websvn.kde.org/?rev=710724&amp;view=rev">710724</a>. </li>
        <li class="bugfix ">Fix invalid placing of image. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149809">149809</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=712197&amp;view=rev">712197</a>. </li>
        <li class="bugfix ">Default to "UTF-8" per section 2 of the draft W3C "The XMLHttpRequest Object" specification. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=130234">130234</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=718830&amp;view=rev">718830</a>. </li>
      </ul>
      </div>
      <h4><a name="kjs">kjs</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not convert accented latin1 characters into undrawable char in websites using advanced JavaScript. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150381">150381</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=719916&amp;view=rev">719916</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="3_5_8/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="pop3 ioslave">POP3 ioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix SASL Authentication fails if another client of sasl is loaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146582">146582</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=674951&amp;view=rev">674951</a>. </li>
      </ul>
      </div>
      <h4><a name="smtp ioslave">SMTP ioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix SASL Authentication fails if another client of sasl is loaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146582">146582</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=674951&amp;view=rev">674951</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kate.kde.org" name="kate">Kate</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Add .hh Extension for C++ syntax highlighting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149801">149801</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=711659&amp;view=rev">711659</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Highlight 'read' and 'write', used when declaring properties in Pascal. See SVN commit <a href="http://websvn.kde.org/?rev=666333&amp;view=rev">666333</a>. </li>
        <li class="bugfix ">Fix code folding breaks highlighting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=139578">139578</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=668607&amp;view=rev">668607</a>. </li>
        <li class="bugfix ">Fix tabs in wrapped line broke cursor navigation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=94693">94693</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=668824&amp;view=rev">668824</a>. </li>
        <li class="bugfix ">Fix save doesn't work after redo. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=143754">143754</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=676516&amp;view=rev">676516</a>. </li>
        <li class="bugfix ">Do not disable quit button when no session is selected but the open session button. See SVN commit <a href="http://websvn.kde.org/?rev=715536&amp;view=rev">715536</a>. </li>
        <li class="bugfix ">Pressing X now triggers the open session button, when it should trigger quit button. See SVN commit <a href="http://websvn.kde.org/?rev=715536&amp;view=rev">715536</a>. </li>
      </ul>
      </div>
      <h4><a name="kcontrol">KControl</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">When delete all is pressed, only delete the visible cookies if the view is filtered. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=113497">113497</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707822&amp;view=rev">707822</a>. </li>
        <li class="bugfix ">Do not loose the domain-specific cookie policies when cookie support is disabled and re-enabled again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=117302">117302</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707819&amp;view=rev">707819</a>. </li>
        <li class="bugfix ">Newly added fonts are not available. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146637">146637</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=673664&amp;view=rev">673664</a>. </li>
      </ul>
      </div>
      <h4><a name="kdesktop">kdesktop</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not shutdown at autologout. See SVN commit <a href="http://websvn.kde.org/?rev=666082&amp;view=rev">666082</a>. </li>
      </ul>
      </div>
      <h4><a name="kdm">kdm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not attempt to auto-login after a post-session shutdown confirmation. See SVN commit <a href="http://websvn.kde.org/?rev=666082&amp;view=rev">666082</a>. </li>
        <li class="bugfix ">Fix CVE-2007-4569. See SVN commit <a href="http://websvn.kde.org/?rev=714420&amp;view=rev">714420</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix restarting KWin messes up active border order. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146040">146040</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=669899&amp;view=rev">669899</a>. </li>
        <li class="bugfix ">Fix block global shortcuts is broken. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149011">149011</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=703895&amp;view=rev">703895</a>. </li>
      </ul>
      </div>
      <h4><a href="http://konsole.kde.org" name="konsole">Konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Real transparency support without side effects, if qt-copy patch #0078 is available. See SVN commit <a href="http://websvn.kde.org/?rev=669488&amp;view=rev">669488</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">Konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Konqueror not autodetecting new plugins. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126744">126744</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=705205&amp;view=rev">705205</a>. </li>
        <li class="bugfix ">Avoid preview crashes when view is invalid. See SVN commit <a href="http://websvn.kde.org/?rev=693291&amp;view=rev">693291</a>. </li>
        <li class="bugfix ">Resolve media: and system: urls to local files in the popup menu. See SVN commit <a href="http://websvn.kde.org/?rev=716991&amp;view=rev">716991</a>. </li>
        <li class="bugfix ">Fix crash when the window is deleted from onmousedown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149736">149736</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=721013&amp;view=rev">721013</a>. </li>
        <li class="bugfix ">Fix nspluginviewer crashes on amd64. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150241">150241</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=719539&amp;view=rev">719539</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeartwork"><a name="kdeartwork">kdeartwork</a><span class="allsvnchanges"> [ <a href="3_5_8/kdeartwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="screen savers">Screen savers</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix slideshow screensaver crash when "Random Position" is selected. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=138161">138161</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=666124&amp;view=rev">666124</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="3_5_8/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/kturtle" name="kturtle">KTurtle</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix UTF8 bug when user language was UTF8. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=82462">82462</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=676298&amp;view=rev">676298</a> and <a href="http://websvn.kde.org/?rev=676589&amp;view=rev">676589</a>. </li>
        <li class="bugfix ">Make "else" work. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=115555">115555</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=676365&amp;view=rev">676365</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/kig" name="kig">Kig</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">If the loading of the document fails, completely cleanup ourselves to be able to open a new document in the same (untouched) window. See SVN commit <a href="http://websvn.kde.org/?rev=701849&amp;view=rev">701849</a>. </li>
      </ul>
      </div>
      <h4><a name="kvoctrain">KVocTrain</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Enable word wrap for the labels in multiple choice query. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149921">149921</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=713250&amp;view=rev">713250</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix handling of hints. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147503">147503</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=714140&amp;view=rev">714140</a>. </li>
        <li class="bugfix ">Fix handling of remote files. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=147535">147535</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=147535">147535</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=714209&amp;view=rev">714209</a> and <a href="http://websvn.kde.org/?rev=714203&amp;view=rev">714203</a>. </li>
        <li class="bugfix ">Fix search not searching in all columns. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=93449">93449</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=717408&amp;view=rev">717408</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/kstars" name="kstars">KStars</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Galactic/Equatorial coordinates conversion does not work. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149261">149261</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=705228&amp;view=rev">705228</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="3_5_8/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="svg thumbnailer">SVG thumbnailer</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix problem with with "#" in foldername on svg-files. See SVN commit <a href="http://websvn.kde.org/?rev=719657&amp;view=rev">719657</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kpdf.kde.org" name="kpdf">KPDF</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Add a slotTogglePresentation() DCOP method to toggle the presentation mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=148026">148026</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=704875&amp;view=rev">704875</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Improve handling of page sizes when printing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=144935">144935</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=663027&amp;view=rev">663027</a>. </li>
        <li class="bugfix ">Do not crash when leaving the presentation mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=143951">143951</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=665684&amp;view=rev">665684</a>. </li>
        <li class="bugfix ">Do no print the boxes around links. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145907">145907</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=668258&amp;view=rev">668258</a>. </li>
        <li class="bugfix ">Reset the title of the KPDF window correctly when reloading a document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=103362">103362</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=686888&amp;view=rev">686888</a>. </li>
        <li class="bugfix ">Take into account the accessibility settings when drawing an empty page waiting for the real page to be rendered. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=148188">148188</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=692291&amp;view=rev">692291</a>. </li>
        <li class="bugfix ">Security fix for CVE-2007-3387. See SVN commit <a href="http://websvn.kde.org/?rev=694346&amp;view=rev">694346</a>. </li>
        <li class="bugfix ">Correctly open documents in paths with special characters (like #). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149102">149102</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=703564&amp;view=rev">703564</a>. </li>
        <li class="bugfix ">Save the case sensitivity setting of the Find dialog during each session. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149164">149164</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=704964&amp;view=rev">704964</a>. </li>
        <li class="bugfix ">Don't crash when we don't have a CTU. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149416">149416</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=707656&amp;view=rev">707656</a>. </li>
        <li class="bugfix ">Correctly take into account the margins when printing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149560">149560</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=708815&amp;view=rev">708815</a>. </li>
        <li class="bugfix ">Properly pass the standard accelerators to the presentation mode, and make the presentation mode able to handle those. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=138451">138451</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=712996&amp;view=rev">712996</a>. </li>
        <li class="bugfix ">Provide horizontal scrollbar in table of contents sidebar instead of cropping the entries. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147233">147233</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=680874&amp;view=rev">680874</a>. </li>
        <li class="bugfix ">Increase time between accessing /proc/memory to check available memory. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150325">150325</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=721994&amp;view=rev">721994</a>. </li>
      </ul>
      </div>
      <h4><a name="libkscan">libkscan</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Correctly retrieve the x/y scan resolution and store it into the scanned image. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=119110">119110</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=689188&amp;view=rev">689188</a>. </li>
      </ul>
      </div>
      <h4><a href="http://www.kolourpaint.org" name="kolourpaint">KolourPaint</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Always enable the paste actions to guarantee that pasting from non-Qt applications is always allowed (non-Qt applications do not notify KolourPaint when they place objects into the clipboard).</li>
        <li class="bugfix ">Paste transparent pixels as white instead of uninitialized colors, when the app does not support pasting transparent pixels (such as OpenOffice.org). See SVN commit <a href="http://websvn.kde.org/?rev=722007&amp;view=rev">722007</a>. </li>
        <li class="bugfix ">Make "Edit / Paste in New Window" always paste white pixels as white (it used to paste them as transparent when the selection transparency mode was set to Transparent).</li>
        <li class="bugfix "> Saving, exporting and printing a document with an active text box, that has opaque text and a transparent background, antialiases the text with the document below.</li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="3_5_8/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="audiocd ioslave">audiocd ioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix powerpc support. See SVN commit <a href="http://websvn.kde.org/?rev=677986&amp;view=rev">677986</a>. </li>
      </ul>
      </div>
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix getting cover from internet doesn't work. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=116181">116181</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=677818&amp;view=rev">677818</a>. </li>
        <li class="bugfix ">Fix last item in play queue is played twice. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126032">126032</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=679721&amp;view=rev">679721</a>. </li>
        <li class="bugfix ">Fix right click in History playlist shows wrong column. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=131238">131238</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=679762&amp;view=rev">679762</a>. </li>
        <li class="bugfix ">Fix empty flac file crashes JuK. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134312">134312</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=683614&amp;view=rev">683614</a>. </li>
      </ul>
      </div>
      <h4><a name="kaudiocreator">KAudioCreator</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when removing all jobs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=119600">119600</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=701441&amp;view=rev">701441</a>. </li>
        <li class="bugfix ">Allow creating group writable directories if umask permits it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=144752">144752</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=711111&amp;view=rev">711111</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="3_5_8/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kopete.kde.org/" name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix adding of yahoo contacts. See SVN commit <a href="http://websvn.kde.org/?rev=674906&amp;view=rev">674906</a>. </li>
        <li class="bugfix ">Fix crash after duplicate login. See SVN commit <a href="http://websvn.kde.org/?rev=674921&amp;view=rev">674921</a>. </li>
        <li class="bugfix ">Jabber groupchat history is only shown for currently present members. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=135211">135211</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=667758&amp;view=rev">667758</a>. </li>
        <li class="bugfix ">Enable Kopete to respond to chats started by others who are blocking you. See SVN commit <a href="http://websvn.kde.org/?rev=668060&amp;view=rev">668060</a>. </li>
        <li class="bugfix ">Newlines seen when sending richtext to gaim and trillian clients. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146380">146380</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=673027&amp;view=rev">673027</a>. </li>
        <li class="bugfix ">Fix webcam problems. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=140528">140528</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=143376">143376</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=673109&amp;view=rev">673109</a>. </li>
        <li class="bugfix ">Kopete does not keep toolbar settings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=59080">59080</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=673118&amp;view=rev">673118</a>. </li>
        <li class="bugfix ">&lt; and &gt; in contact nicknames aren't well supported. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133186">133186</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=673127&amp;view=rev">673127</a>. </li>
        <li class="bugfix ">"Message handling" unset when setting it to "Open messages instantly" and reopening the configuration dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146882">146882</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=678555&amp;view=rev">678555</a>. </li>
        <li class="bugfix ">Deactivate the Join Chatrooms action when offline. See SVN commit <a href="http://websvn.kde.org/?rev=692802&amp;view=rev">692802</a>. </li>
        <li class="bugfix ">Fix error in chatroom search so that all chatrooms are listed. See SVN commit <a href="http://websvn.kde.org/?rev=692802&amp;view=rev">692802</a>. </li>
        <li class="bugfix ">Fix msn live emoticons. See SVN commit <a href="http://websvn.kde.org/?rev=704382&amp;view=rev">704382</a>. </li>
        <li class="bugfix ">Fix issue with MSN webcam images being garbled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=126996">126996</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=717104&amp;view=rev">717104</a>. </li>
        <li class="bugfix ">Fix crash on Yahoo protocol. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145514">145514</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=718647&amp;view=rev">718647</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="modulehomepage"> [ <a href="http://pim.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="3_5_8/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="core libs">core libs</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not drop frecuently used address that are not in the addresbook from recent address list. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=85539">85539</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=671909&amp;view=rev">671909</a>. </li>
        <li class="bugfix ">Fix Free/Busy information not regenerated for recurring events with end date after fb-period. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=124495">124495</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=719675&amp;view=rev">719675</a>. </li>
      </ul>
      </div>
      <h4><a name="birthdays resource">Birthdays resource</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Reminders for imported birthdays from KAdressbook don't work as expected. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=143230">143230</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=689074&amp;view=rev">689074</a>. </li>
      </ul>
      </div>
      <h4><a name="groupwise resource">Groupwise resource</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't crash if hostname or port are wrong. See SVN commit <a href="http://websvn.kde.org/?rev=695171&amp;view=rev">695171</a>. </li>
      </ul>
      </div>
      <h4><a name="imap ioslave">IMAP ioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix SASL Authentication fails if another client of sasl is loaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146585">146585</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=674932&amp;view=rev">674932</a>. </li>
      </ul>
      </div>
      <h4><a name="sieve ioslave">Sieve ioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix SASL Authentication fails if another client of sasl is loaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146585">146585</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=674932&amp;view=rev">674932</a>. </li>
        <li class="bugfix ">Fix cannot login When using TLS. See SVN commit <a href="http://websvn.kde.org/?rev=696980&amp;view=rev">696980</a>. </li>
        <li class="bugfix ">Fix retrieving scripts from the server always failed with a protocol error report. See SVN commit <a href="http://websvn.kde.org/?rev=696980&amp;view=rev">696980</a>. </li>
      </ul>
      </div>
      <h4><a name="libkholidays">libkholidays</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Add Argentina holidays file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150203">150203</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=717082&amp;view=rev">717082</a>. </li>
      </ul>
      </div>
      <h4><a name="kaddressbook">KAddressBook</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix using X-KADDRESSBOOK-X-Department where ORG:organization;department should be used. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=115219">115219</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=702333&amp;view=rev">702333</a>. </li>
        <li class="bugfix ">Fix import bug for vCards when running as Kontact part. See SVN commit <a href="http://websvn.kde.org/?rev=708653&amp;view=rev">708653</a>. </li>
      </ul>
      </div>
      <h4><a href="http://www.astrojar.org.uk/linux/kalarm.html" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Provide a Stop button in sound file dialog, to allow sound file playing to be stopped. See SVN commit <a href="http://websvn.kde.org/?rev=673273&amp;view=rev">673273</a>. </li>
        <li class="feature">Allow delay times up to 999 hours to be entered. See SVN commit <a href="http://websvn.kde.org/?rev=720288&amp;view=rev">720288</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix time value in template alarms not being stored. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145575">145575</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=666061&amp;view=rev">666061</a>. </li>
        <li class="bugfix ">Fix handling of exception dates in recurrences, which not only didn't work but also erroneously suppressed valid recurrence dates. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146953">146953</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=696820&amp;view=rev">696820</a>. </li>
        <li class="bugfix ">Fix deferrals of recurring alarms not triggering correctly. See SVN commit <a href="http://websvn.kde.org/?rev=709812&amp;view=rev">709812</a>. </li>
        <li class="bugfix ">Fix font selection not being saved for individual alarms. See SVN commit <a href="http://websvn.kde.org/?rev=711534&amp;view=rev">711534</a>. </li>
        <li class="bugfix ">Fix incorrect email headers being generated, which prevented some emails being sent. See SVN commit <a href="http://websvn.kde.org/?rev=722729&amp;view=rev">722729</a>. </li>
      </ul>
      </div>
      <h4><a name="karm">KArm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Allow negative times for budgeting time for a task. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146679">146679</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=674691&amp;view=rev">674691</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Right click on "task complete" box should not tick/untick box. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149589">149589</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=710766&amp;view=rev">710766</a>. </li>
        <li class="bugfix ">Fix marking task done does not stop the time. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=149302">149302</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=705813&amp;view=rev">705813</a>. </li>
      </ul>
      </div>
      <h4><a name="kitchensync">Kitchensync</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Added configuration dialogs for Evo2, SynCE, Moto and Jescs. See SVN commit <a href="http://websvn.kde.org/?rev=682311&amp;view=rev">682311</a>. </li>
        <li class="feature">Added configuration dialog for Sunbird plugin. See SVN commit <a href="http://websvn.kde.org/?rev=690263&amp;view=rev">690263</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kmail.kde.org" name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not show the dragging menu when dragging to ourselves or to our parent. See SVN commit <a href="http://websvn.kde.org/?rev=668801&amp;view=rev">668801</a>. </li>
        <li class="bugfix ">Do not ask the user to go online each time a job finished if he decided to be offline. See SVN commit <a href="http://websvn.kde.org/?rev=714452&amp;view=rev">714452</a>. </li>
        <li class="bugfix ">Fix rebuilding index files breaks subject threading for existing messages. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=125920">125920</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=672014&amp;view=rev">672014</a>. </li>
        <li class="bugfix ">Fix copying of messages on the same online IMAP server. See SVN commit <a href="http://websvn.kde.org/?rev=686453&amp;view=rev">686453</a>. </li>
        <li class="bugfix ">Fix crash when moving folders. See SVN commit <a href="http://websvn.kde.org/?rev=687345&amp;view=rev">687345</a>. </li>
        <li class="bugfix ">Fix crashes during drag&amp;drop of imap folders. See SVN commit <a href="http://websvn.kde.org/?rev=689897&amp;view=rev">689897</a>. </li>
        <li class="bugfix ">Fix crash after reply to a revoked mail. See SVN commit <a href="http://websvn.kde.org/?rev=698167&amp;view=rev">698167</a>. </li>
        <li class="bugfix ">Fix event loop handling. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=108312">108312</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=148700">148700</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=143003">143003</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=136365">136365</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=90467">90467</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=143776">143776</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=147016">147016</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=137279">137279</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=136413">136413</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=699554&amp;view=rev">699554</a> and <a href="http://websvn.kde.org/?rev=700023&amp;view=rev">700023</a>. </li>
        <li class="bugfix ">Fix crash when cancelling GnuPG singing a new empty message. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=144303">144303</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=700038&amp;view=rev">700038</a>. </li>
        <li class="bugfix ">Fix crash on retrieving pop mail. See SVN commit <a href="http://websvn.kde.org/?rev=706425&amp;view=rev">706425</a>. </li>
        <li class="bugfix ">Fix crashing after startup. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=140490">140490</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=715297&amp;view=rev">715297</a>. </li>
        <li class="bugfix ">Fix templates for forwarding do not work with inline mails. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=140549">140549</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=685749&amp;view=rev">685749</a>. </li>
        <li class="bugfix ">Fix draft messages forget the default GPG action. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=103240">103240</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=703251&amp;view=rev">703251</a>. </li>
        <li class="bugfix ">Fix encrypt to self function beign always on. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=93436">93436</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=705215&amp;view=rev">705215</a>. </li>
        <li class="bugfix ">Fix mails started from a template have wrong date. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146534">146534</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=674569&amp;view=rev">674569</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kontact.kde.org" name="kontact">Kontact</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when editing toolbars while a plugin without part is active. See SVN commit <a href="http://websvn.kde.org/?rev=675061&amp;view=rev">675061</a>. </li>
        <li class="bugfix ">Fix crash if no plugin is active when editing toolbars. See SVN commit <a href="http://websvn.kde.org/?rev=675090&amp;view=rev">675090</a>. </li>
      </ul>
      </div>
      <h4><a href="http://korganizer.kde.org/" name="korganizer">KOrganizer</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix loading the freebusy list for people with a preferred email address. See SVN commit <a href="http://websvn.kde.org/?rev=668801&amp;view=rev">668801</a>. </li>
        <li class="bugfix ">Fix unability to get freebusy information from an eGroupware server. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=85630">85630</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=691427&amp;view=rev">691427</a>. </li>
        <li class="bugfix ">Fix publish free/busy list automatically not working. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=77223">77223</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=111419">111419</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=691427&amp;view=rev">691427</a>. </li>
        <li class="bugfix ">Fix a crash when moving recurring multidays events. See SVN commit <a href="http://websvn.kde.org/?rev=695525&amp;view=rev">695525</a>. </li>
        <li class="bugfix ">Fix audio notification open file dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146648">146648</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=716784&amp;view=rev">716784</a>. </li>
      </ul>
      </div>
      <h4><a name="korn">korn</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the Subject and Sender parsing. See SVN commit <a href="http://websvn.kde.org/?rev=686216&amp;view=rev">686216</a>. </li>
        <li class="bugfix ">Fix fetching the password of a kmail account from kmailrc. See SVN commit <a href="http://websvn.kde.org/?rev=686216&amp;view=rev">686216</a>. </li>
        <li class="bugfix ">Fix using pop3-mailboxes from the kmail configuration. See SVN commit <a href="http://websvn.kde.org/?rev=686216&amp;view=rev">686216</a>. </li>
      </ul>
      </div>
      <h4><a name="akregator">Akregator</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix storage backend waking up CPU every 3 seconds. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=150389">150389</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=721755&amp;view=rev">721755</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="3_5_8/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://cervisia.kde.org" name="cervisia">Cervisia</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix invalid date warnings when browsing CVS. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=148162">148162</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=711210&amp;view=rev">711210</a>. </li>
      </ul>
      </div>
      <h4><a href="http://uml.sourceforge.net" name="umbrello">Umbrello</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Code generator for D language. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=124805">124805</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix import of Rose MDL files containing ISO-8859 Latin1 characters. See SVN commit <a href="http://websvn.kde.org/?rev=687101&amp;view=rev">687101</a>. </li>
        <li class="bugfix ">Preprocessor keywords ignored which causes endless loop in code import. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=119125">119125</a>. </li>
        <li class="bugfix ">Cannot move text and label correctly after enabling snap to grid. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=137041">137041</a>. </li>
        <li class="bugfix ">Unstable saves and loads, class names become dirty. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145709">145709</a>. </li>
        <li class="bugfix ">Crash on deleting class in list view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145762">145762</a>. </li>
        <li class="bugfix ">Class attribute documentation not generated for python. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145916">145916</a>. </li>
        <li class="bugfix ">Python code generator does not wrap lines properly. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145918">145918</a>. </li>
        <li class="bugfix ">Attribute documentation not generated for 'Export to XHTML'. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145972">145972</a>. </li>
        <li class="bugfix ">Crash when moving a class in a Java UML diagram. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146058">146058</a>. </li>
        <li class="bugfix ">Umbrello gratuitously appends ".xmi" to the saved file name. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146061">146061</a>. </li>
        <li class="bugfix ">Arrowheads are not shown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146064">146064</a>. </li>
        <li class="bugfix ">Crash when creating a class that refers to more than one other classes/datatypes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146367">146367</a>. </li>
        <li class="bugfix ">Wrong Pascal code generation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146676">146676</a>. </li>
        <li class="bugfix ">Crash when linking to undefined xmi.id. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146748">146748</a>. </li>
        <li class="bugfix ">End Activity Symbol gets invalid when line thickness is increased. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146925">146925</a>. </li>
        <li class="bugfix ">The size of a fork/join is not restored. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147069">147069</a>. </li>
        <li class="bugfix ">Crash when changing the association type to containment. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147202">147202</a>. </li>
        <li class="bugfix ">Moving component results in absurd shape of a self-association. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147810">147810</a>. </li>
        <li class="bugfix ">Crash when changing the attribute name. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147919">147919</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="3_5_8/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kfloppy">kfloppy</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Check all available densities instead of a hardcoded list. See SVN commit <a href="http://websvn.kde.org/?rev=670409&amp;view=rev">670409</a>. </li>
      </ul>
      </div>
      <h4><a name="klaptopdaemon">klaptopdaemon</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix lock &amp; hibernate allowing unauthorised access . Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=143859">143859</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=721296&amp;view=rev">721296</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdewebdev"><a name="kdewebdev">kdewebdev</a><span class="allsvnchanges"> [ <a href="3_5_8/kdewebdev.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kimagemapeditor">KImageMapEditor</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not limit map elements to be at 2000 as bigger x/y coordinate. See SVN commit <a href="http://websvn.kde.org/?rev=682934&amp;view=rev">682934</a>. </li>
      </ul>
      </div>
      <h4><a href="http://quanta.kdewebdev.org" name="quanta plus">Quanta Plus</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix recursive symlink handling. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145651">145651</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=720608&amp;view=rev">720608</a>. </li>
        <li class="bugfix ">Fix add to project when saving a new file into a symlinked directory. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=148529">148529</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=720634&amp;view=rev">720634</a>. </li>
        <li class="bugfix ">Do not lose CSS selectors after editing inside the dialog if they are repeated. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145413">145413</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=720675&amp;view=rev">720675</a>. </li>
        <li class="bugfix ">Fix help button in the New Project wizard. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145324">145324</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=721001&amp;view=rev">721001</a>. </li>
        <li class="bugfix ">Fix crashing while editing html with php files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145324">145324</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=721001&amp;view=rev">721001</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kommander.kdewebdev.org" name="kommander">Kommander</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">add DCOP method to get the winID Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=111161">111161</a>. </li>
        <li class="feature">add DCOP method to change the cursor to the wait cursor and back Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=109630">109630</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=722155&amp;view=rev">722155</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make multiplying work correctly for floating point values. See SVN commit <a href="http://websvn.kde.org/?rev=722062&amp;view=rev">722062</a>. </li>
        <li class="bugfix ">Do not remove the whole text after undoing the changes in the editor. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=147817">147817</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=722069&amp;view=rev">722069</a>. </li>
        <li class="bugfix ">Do not crash if opening a file the second time when the first try failed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=141110">141110</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=722079&amp;view=rev">722079</a>. </li>
        <li class="bugfix ">Function str_findrev works now. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Fix crash when diving by zero. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Make function names case insensitive. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Fix dcop() function (external DCOP). See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Add missing EOLs to array_values(). See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Do not open files with no filename given. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Fix crash on incorrect syntax. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Fix input_file() function. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Fix importing tables with empty cells. See SVN commit <a href="http://websvn.kde.org/?rev=718834&amp;view=rev">718834</a>. </li>
        <li class="bugfix ">Use icon instead of "..." in the file selector. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=109985">109985</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=722109&amp;view=rev">722109</a>. </li>
        <li class="bugfix ">Fix ExecButton.setEnabled(). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=113624">113624</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=722103&amp;view=rev">722103</a>. </li>
        <li class="bugfix ">Don't close the dialogs with ESC (just like in real applications). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=123071">123071</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=722089&amp;view=rev">722089</a>. </li>
        <li class="bugfix ">make @echo really work Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=138705">138705</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=138705&amp;view=rev">138705</a>. </li>
      </ul>
      </div>
    </div>
  
    <h3 id="arts"><a name="arts">arts</a><span class="allsvnchanges"> [ <a href="3_5_8/arts.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdeaccessibility"><a name="kdeaccessibility">kdeaccessibility</a><span class="allsvnchanges"> [ <a href="3_5_8/kdeaccessibility.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdeaddons"><a name="kdeaddons">kdeaddons</a><span class="allsvnchanges"> [ <a href="3_5_8/kdeaddons.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdeadmin"><a name="kdeadmin">kdeadmin</a><span class="allsvnchanges"> [ <a href="3_5_8/kdeadmin.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdebindings"><a name="kdebindings">kdebindings</a><span class="allsvnchanges"> [ <a href="3_5_8/kdebindings.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="3_5_8/kdegames.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdetoys"><a name="kdetoys">kdetoys</a><span class="allsvnchanges"> [ <a href="3_5_6/kdetoys.txt">all SVN changes</a> ]</span></h3>