------------------------------------------------------------------------
r1230446 | mpyne | 2011-05-05 15:04:07 +1200 (Thu, 05 May 2011) | 5 lines

Backport history playlist crash fix to 4.6.

This backports some history playlist column handling fixes (including a crash
fix) to KDE SC 4.6.4. The source revision is r1230444.

------------------------------------------------------------------------
r1230447 | mpyne | 2011-05-05 15:07:44 +1200 (Thu, 05 May 2011) | 7 lines

Fix saving of tag editor position on shutdown.

This is a backport of Giorgos Kylafas's tag editor show/hide status saving fix
to KDE SC 4.6.4, from r1229816.

CCMAIL:gekylafas@gmail.com

------------------------------------------------------------------------
r1231603 | scripty | 2011-05-13 03:35:06 +1200 (Fri, 13 May 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1233430 | kylafas | 2011-05-25 00:54:19 +1200 (Wed, 25 May 2011) | 3 lines

juk: fix showing the items of all the selected playlists.

Backport the fix from trunk's r1233429 to branch 4.6 (KDE SC 4.6.4).
------------------------------------------------------------------------
r1233439 | kylafas | 2011-05-25 01:44:57 +1200 (Wed, 25 May 2011) | 6 lines

juk: correctly update column widths when contents change.

This is a backport from trunk of a fix contained in r1230441 plus its
supplemental fix in r1230877. The former correctly marked column weights
as "dirty" when contents changed, while the latter kept the previously
calculated weights for columns that did not change.
------------------------------------------------------------------------
