---
aliases:
- ../4.14
title: KDE Software Compilation 4.14
date: "2014-08-20"
description: KDE Ships Applications and Platform 4.14.
---

This release is dedicated to Volker Lanz, a long time KDE member who passed away last April. Volker was the author and maintainer of KDE Partition Manager, and a regular IRC participant (Torch) providing user support and being active in the KDE Community.

The KDE Community announces the latest major updates to KDE Applications delivering primarily improvements and bugfixes. Plasma Workspaces and the KDE Development Platform are frozen and receiving only long term support; those teams are focused on the transition to Plasma 5 and Frameworks 5.

{{<figure src="/announcements/4/4.14.0/screenshots/plasma-4.11.png" class="text-center" width="600px" >}}

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/mascot_20140702_konqui-framework_wee_0.png" class="text-center float-right ml-3" >}}
In the past, KDE has jointly released the three major divisions of KDE software—Plasma Workspaces, KDE Development Platform and KDE Applications. The KDE Development Platform has been reworked into KDE Frameworks. The monolithic libraries that comprise the Development Platform are now independent, cross platform modules (KDE Frameworks 5) that are available to all Qt developers. Plasma Workspaces has been moved to a new technology foundation based on Qt5 and KDE Frameworks 5. With the 3 major KDE software components moving at different paces, their release schedules are now separated. For the most part, 4.14 involves KDE Applications.

## Development Platform/KDE Frameworks 5

The <a href="https://dot.kde.org/2013/09/25/frameworks-5">modular Frameworks structure</a> will have widespread benefits for KDE software. In addition, Frameworks is a substantial <a href="https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers">contribution to the Qt ecosystem</a> by making KDE technology available to all Qt developers. <a href="http://inqlude.org/">Inqlude, the Qt library archive</a> simplifies the search for Qt libraries, while the <a href="http://rubygems.org/gems/inqlude/versions/0.7.0">alpha release</a> of the <a href="https://github.com/cornelius/inqlude">Inqlude tool</a> offers a command line interface for accessing Inqlude.

## Plasma Workspaces

<a href="https://dot.kde.org/2014/07/15/plasma-5.0">Plasma 5</a> was recently released after 3 years of work; it is on its own release schedule with feature releases every three months and bugfix releases in the intervening months. The Plasma team has built a solid foundation that will support Plasma Workspaces for many years.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/mascot_20140702_konqui-plasma_wee_0.png" class="text-center float-right ml-3" >}}

## KDE Applications

KDE Applications 4.14 is not about lots of &quot;new and improved stuff&quot;. Many KDE developers are focused on the Next Experience (Plasma 5) or porting to KDE Frameworks (based on <a href="http://qt-project.org/qt5">Qt5</a>). Mostly, the 4.14 release is needed by aspects of our workflow (such as translations). This release offers more software stability, with little emphasis on new and less-proven stuff.

There are over 200 actively maintained <a href="https://apps.kde.org/">KDE applications</a>. Many of them are listed in the <a href="https://userbase.kde.org/Applications">KDE userbase</a>. Wikipedia also has another <a href="http://en.wikipedia.org/wiki/List_of_KDE_applications">list of KDE applications</a>.

Most previous releases had highlights of new features and prominent applications. This gave some people the impression that KDE developers favored new-and-shiny over quality, which is not true. So for this announcement of the 4.14 Release, developers were asked for details—small, incremental improvements and bugfixes that might not even be noticeable to most users. These are the kinds of tasks that most developers work on, the kinds of tasks that allow beginners to make meaningful, mostly invisible contributions. Here are a few examples of the kinds of improvements that KDE developers have made in this release:

<a href="http://kopete.kde.org/">Kopete</a>

- Support for SOCKS5 proxy in ICQ protocol (before only HTTP type was supported)

- Support for using system proxy settings in ICQ protocol
<ul>
<li> Support for audio calls (both protocols Google libjingle and real jingle) for _all_ <a href="http://www.jabber.org/">jabber</a> accounts, enabled by default

<li>Updated libiris library from upstream which implements jabber/xmpp protocol in kopete

<li> Contact property custom name and support for preferred display name type, making it possible to distinguish and choose between custom names stored on a server list and contact custom/nick name

<li> Wrap PGP-signed or encrypted messages into <a href="http://www.xmpp.org/extensions/xep-0027.html">XEP-0027</a> XML block instead of the normal body of message
</ul>

- Show full range of jabber priorities in account config dialog

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" class="text-center float-right ml-3" >}}

<a href="http://edu.kde.org/cantor/">Cantor</a>

<ul>
<li> New <a href="http://oproj.tuxfamily.org/math/lua/kde/2014/08/04/cantor.html">Lua backend</a>
<li> <a href="http://en.wikipedia.org/wiki/UTF-8">UTF-8</a> on LaTeX entries
</ul>

- Add CTRL+Space as an alternative default code completion
- Support to plot extension in Python 2 backend
- Support to linear algebra extension in Python 2 backend
- Support to packaging extension in Sage, Octave backends
- Support to autorun scripts in Python 2, Scilab, Octave, Sage, Maxima, Qalculate and KAlgebra backends

<a href="http://edu.kde.org/kanagram/">Kanagram</a> got a new QML <a href="http://debjitmondal.blogspot.com/2014/07/brand-new-kanagram.html">User Interface and some features</a>.

<a href="http://okular.kde.org/">Okular</a> got bugfixes, small features and internal refactoring

<a href="http://www.kde.org/applications/utilities/kate/">Kate</a>

- New highlighting rules for languages; bugfixes and improvements to existing languages
- Improved VI mode
- Comment blocks can be folded automatically to save screen space
- Improved support and auto-generation of dark system color schemes
- Multiple bug fixes

<a href="http://umbrello.kde.org/">Umbrello</a>

- UML2 ports on components
- UML2 interface ball and socket notation
- Improved C++ import (map declarations to correct namespace)
- Crash fixes in all language importers
<ul>
<li> Improved loading of <a href="http://en.wikipedia.org/wiki/IBM_Rational_Rose_XDE">Rose models</a>: Added support for controlled units and class diagrams

<li> Support for loading <a href="http://en.wikipedia.org/wiki/ArgoUML">ArgoUML</a> files (without diagrams)

<li> Support for loading <a href="http://www.embarcadero.com/data-modeling">Embarcadero</a> describe files (without diagrams)

<li> Ada now can generate multiple classes per package (<a href="http://bugs.kde.org/336933">bugfix</a>)

<li> New &quot;Find in diagram&quot; function (<a href="http://bugs.kde.org/116354">bugfix</a>)

<li> Stabilized positions of activity pins (<a href="http://bugs.kde.org/335399">bugfix</a>)

<li> Fixed sluggish UI reaction in state diagram (<a href="http://bugs.kde.org/337463">bugfix</a>)

<li> Crash fixes: <a href="http://bugs.kde.org/256716">bugfix</a>, <a href="http://bugs.kde.org/332612">bugfix</a>, <a href="http://bugs.kde.org/337606">bugfix</a>
</ul>

<a href="http://dolphin.kde.org/">Dolphin</a> has mostly bug fixes and small changes such as:

- Highlighting of the current item in the Places Panel is prettier.
- &quot;Free space&quot; notification in the status bar is now always up-to-date in all windows and views.
- Refactoring of the huge class that represents Dolphin's main window has been started to make the code more maintainable and easier to understand.

<a href="http://edu.kde.org/marble/">Marble</a>

- Dolphin now shows thumbnails of .gpx, .kml and other file types supported by Marble

<ul>
<li> <a href="http://en.wikipedia.org/wiki/Keyhole_Markup_Language">KML</a> improvements: The list of supported KML features has been extended

<li> The new political vector map now shows governmental boundaries in different colors; a <a href="https://www.google-melange.com/gsoc/homepage/google/gsoc2014">Google Summer of Code</a> project by Abhinav Gangwar.
</ul>

- <a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&amp;bug_status=VERIFIED&bug_status=CLOSED&amp;chfieldfrom=2013-01-01&amp;chfieldto=Now&amp;f1=cf_versionfixedin&amp;f2=cf_versionfixedin&amp;j_top=OR&amp;o1=equals&amp;o2=equals&amp;query_format=advanced&amp;resolution=FIXED&amp;v1=4.14&amp;v2=4.14.0&amp;order=product%2Cbug_id%20DESC">Many, many bugfixes</a>

## Support KDE

<a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5"><img src="/announcements/4/4.13.0/images/join-the-game.png" width="231" height="120" alt="Join the Game" align="left" class="mr-3"/> </a>

KDE e.V.'s <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5">Supporting Member program</a> is open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.

You can also make a one time donation at <a href="/community/donations/">our donations page</a>.

#### Installing 4.14 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.14 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/4/4.14.0#binary'>%4.14.0 Info Page</a>.

#### Compiling 4.14

The complete source code for 4.14.0 may be <a href='http://download.kde.org/stable/4.14.0/src/'>freely downloaded</a>. Instructions on compiling and installing 4.14.0 are available from the <a href='/info/4/4.14.0'>4.14.0 Info Page</a>.



