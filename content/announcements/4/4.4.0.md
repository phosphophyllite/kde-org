---
aliases:
- ../4.4
date: '2010-02-09'
description: KDE Ships New KDE SC 4.4.0.
title: KDE SC 4.4.0 Caikaku Release Announcement
---

<h3 align="center">
  KDE Software Compilation 4.4.0 Introduces Netbook Interface, Window Tabbing and Authentication Framework
</h3>

<p align="justify">
  <strong>
    KDE Software Compilation 4.4.0 (Codename: <i>"Caikaku"</i>) Released
  </strong>
</p>

<p align="justify">
 Today <a href="/">KDE</a> announces the immediate availability of the KDE Software Compilation 4.4, <i>"Caikaku"</i>, bringing an innovative collection of applications to Free Software users. Major new technologies have been introduced, including social networking and online collaboration features, a new netbook-oriented interface and infrastructural innovations such as the KAuth authentication framework. According to KDE's bug-tracking system, 7293 bugs have been fixed and 1433 new feature requests were implemented. The KDE community would like to thank everybody who has helped to make this release possible.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="The KDE Plasma Desktop Workspace">
	</a> <br/>
	<em>The KDE Plasma Desktop Workspace</em>
</div>
<br/>

<p align="justify">
 Read the <a href="/announcements/4/4.4.0/guide">Visual Guide To KDE Software Compilation 4.4</a> for more details on the improvements in 4.4 or read on for an overview.
</p>

<h3>
  Plasma Workspace introduces social and web features
</h3>
<br />
<p align="justify">
 The KDE Plasma Workspaces offer the basic functionality a user needs to start and manage applications, files and global settings. The KDE workspace developers deliver a new netbook interface, refined artwork and a better workflow to Plasma. The introduction of social and on-line service features emerge from the cooperative nature of the KDE community as a Free Software team.
</p>

<!-- Plasma Screencast -->

<div class="text-center">
	<em>Improved Interaction with the Plasma Desktop Shell</em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">Download</a></div>
<br/>

<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong> debuts in 4.4.0. Plasma Netbook is an alternative interface to the Plasma Desktop, specifically designed for ergonomic use on netbooks and smaller notebooks. The Plasma framework has been built from the beginning with non-desktop target devices in mind as well. Plasma Netbook shares many components with the Plasma Desktop, but is specifically designed to make good use of the small space, and to be more suitable also for touchscreen input.
    The Plasma Netbook shell features a full-screen application launcher and search interface, and a Newspaper which offers many widgets to display content from the web and small utilities already known from Plasma Netbook's sibling.
  </li>
  <li>
    The <strong>Social Desktop</strong> initiative brings improvements to the Community widget (formerly known as Social Desktop widget), allowing users to send messages and find friends right from within the widget. The new Social News widget shows a livestream of what is going on in the social network of the user and the new Knowledge Base widget allows users to search for answers and questions from different providers including openDesktop.org's own knowledge base.
  </li>
  <li>
    The new tabbing feature in KWin allows the user to <strong>group windows together</strong> in a tabbed interface, making the handling of large numbers of applications easier and more efficient. Further improvements to window handling include snapping windows to a side of the screen and maximizing windows by dragging them. The KWin Team also cooperated with the Plasma developers on further improving the interaction between the workspace applications bringing smoother animations and increased performance. Finally artists can more easily develop and share themes for the windows thanks to the development of a more configurable window decorator with the ability to use scalable graphics.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->

<div class="text-center">
	<em>Managing Windows Made Easier  </em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">Download</a></div>
<br/>

<h3>
  KDE Applications Innovate
</h3>
<p align="justify">
The KDE community offers a large number of powerful yet easy to use applications. This release introduces a variety of incremental improvements and some innovative new technologies.
</p>
<p align="justify">
<ul>
  <li>
    With this release of the KDE Software Compilation the <strong>GetHotNewStuff interface</strong> has been heavily improved, the result of a long term design and planning process. This framework was envisioned to allow the massive third-party contributor community in KDE to <strong>connect more easily</strong> with the millions of users of their content. Users can download data like <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">new levels in KAtomic</a>, new stars or functionality-enhancing scripts right from within the application. New are <strong>social features</strong> like commenting on and rating content and becoming a fan of a product which will show the user updates to those products in the Social News widget. Users can <strong>Upload the results</strong> of their own creativity to the web from several applications, eliminating the boring process of packaging and manually uploading to a website.
  </li>
  <li>
    Two other long-term projects by the KDE community come to fruition in this release. Nepomuk, an international research effort financed by the European Community, has reached the point of sufficient stability and performance. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">Dolphin's integration of desktop search</a> </strong> makes use of this Nepomuk's semantic framework to aid the user in locating and organizing his data. The new timeline view shows recently used files organized chronologically. Meanwhile the KDE PIM team has ported the first applications to make use of the new data storage and retrieval system, <strong>Akonadi</strong>. The KDE addressbook application has been rewritten, sporting a new easy 3-pane interface. A more substantial migration of applications to these new technologies will come in future releases of the KDE Software Compilation.
  </li>
  <li>
    Besides the integration of these major technologies the various developer teams have improved their applications in many ways. The KGet developers have added support for digital signature checking and downloading files from multiple sources and Gwenview now includes an easy to use photo import tool. Furthermore, new or entirely reworked applications see the light of day in this release. Palapeli is a puzzle game which lets the user solve jigsaw puzzles on his or her computer. Users can also create and share their own puzzles. Cantor is an easy and intuitive interface to powerful statistical and scientific software (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a>, and <a href="http://maxima.sourceforge.net/">Maxima</a>). The KDE PIM suite introduces Blogilo, a new blogging application.
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/dolphin-search.png">
	<img src="/announcements/4/4.4.0/thumbs/dolphin-search_thumb.png" class="img-fluid" alt="Desktop Search integrated in KDE's Dolphin File Manager">
	</a> <br/>
	<em>Desktop Search integrated in KDE's Dolphin File Manager</em>
</div>
<br/>

<h3>
  Platform Accelerates Development
</h3>
<p align="justify">
The strong focus on excellent technology in the KDE community has resulted in one of the most complete, consistent and efficient development platforms in existence. The 4.4 release introduces many new collaboration and social network related technologies in the KDE libraries. We provide powerful and flexible, open alternatives to existing technologies. Instead of striving to lock-in users into using our products, we focus on enabling innovation in both the on-line and desktop spaces.
</p>
<p align="justify">
<ul>
  <li>
    The underlying infrastructure of the KDE software has had a number of significant upgrades. First of all, <strong>Qt 4.6</strong> introduces support for the symbian platform, a new animation framework, multitouch and better performance. The <strong>Social Desktop technology</strong> introduced in the previous release has been improved with a central identity management and introduces libattica as a transparent web-service access library. <strong>Nepomuk</strong> now makes use of a much more stable backend, making it a the perfect choice for anything metadata, search or indexing related in applications.
  </li>
  <li>
    With KAuth, a new authentication framework is introduced. <strong>KAuth provides secure authentication</strong> and related user interface elements to developers who want to run tasks with elevated privileges. On Linux, KAuth uses PolicyKit as a backend, providing <strong>seamless cross-desktop integration</strong>. KAuth is already used in a few selected dialogs in System Settings and will be further integrated into the KDE Plasma Desktop and the KDE Applications over the next months. KAuth supports granting and revoking fine-grained authorization policies, caching of passwords and a number of dynamic UI elements providing visual hints for use in applications.
  </li>
  <li>
    <strong>Akonadi</strong>, the Free Desktop groupware cache sees its introduction into KDE Applications with 4.4.0. The address book application delivered with KDE SC 4.4 is the first KDE Application to make use of the new Akonadi infrastructure. KAddressbook supports local addressbooks as well as various groupware servers. KDE SC 4.4.0 marks the beginning of the Akonadi-era in KDE SC, with more applications to follow in upcoming releases. Akonadi is slated to be the central interface for emails, contacts, calendaring data and other personal information. It acts as a transparant cache to email-, groupware-servers and other online resources.
  </li>
</ul>
</p>
<p>
Liberally licensed under the LGPL (allowing for both proprietary and open source development) and cross-platform (Linux, UNIX, Mac and MS Windows).
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/social-web-widgets.png">
	<img src="/announcements/4/4.4.0/thumbs/social-web-widgets_thumb.png" class="img-fluid" alt="The Web and Social Networks on the Plasma Desktop">
	</a> <br/>
	<em>The Web and Social Networks on the Plasma Desktop</em>
</div>
<br/>

<h4>
More Changes
</h4>
<p align="justify">
As mentioned, the above is just a selection of the changes and improvements to the KDE Desktop Workspace, KDE Application Suites and KDE Application Development Framework. A more comprehensive yet still incomplete list can be found in the <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">KDE SC 4.4 feature plan</a> on <a href="http://techbase.kde.org">TechBase</a>. Information about applications developed by the KDE community outside of the KDE Application Suites can be found on <a href="http://kde-apps.org">the kde-apps website</a>.
</p>

<h4>
    Spread the Word and See What Happens
</h4>
<p align="justify">
The KDE Community encourages everybody to <strong>spread the word</strong> on the Social Web.
Submit stories to news sites, use channels like delicious, digg, reddit, twitter,
identi.ca. Upload screenshots to services like Facebook, FlickR,
ipernity and Picasa and post them to appropriate groups. Create screencasts and
upload them to YouTube, Blip.tv, Vimeo and others. Do not forget to tag uploaded
material with the <em>tag <strong>kde</strong></em> so it is easier for everybody to find the
material, and for the KDE team to compile reports of coverage for the KDE SC 4.4
announcement. <strong>Help us spreading the word, be part of it!</strong></p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>
  Installing KDE SC 4.4.0
</h4>
<p align="justify">
KDE, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.4.0/">download.kde.org</a> and can
also be obtained on <a href="/download">CD-ROM</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.4.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.4.0">KDE SC 4.4.0 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.4.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE SC 4.4.0 may be <a
href="http://download.kde.org/stable/4.4.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.4.0
  are available from the <a href="/info/4/4.4.0#binary">KDE SC 4.4.0 Info
Page</a>.
</p>


