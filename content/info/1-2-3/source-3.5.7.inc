<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/arts-1.5.7.tar.bz2">arts-1.5.7</a></td>
   <td align="right">957kB</td>
   <td><tt>28ac10541e5d8daf9009f6af1f7857af</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdeaccessibility-3.5.7.tar.bz2">kdeaccessibility-3.5.7</a></td>
   <td align="right">8.3MB</td>
   <td><tt>49a3ffc5303a0c59abf9dcfef185f8bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdeaddons-3.5.7.tar.bz2">kdeaddons-3.5.7</a></td>
   <td align="right">1.6MB</td>
   <td><tt>7b50fa8e103bd722dfcdfc329126ff28</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdeadmin-3.5.7.tar.bz2">kdeadmin-3.5.7</a></td>
   <td align="right">2.0MB</td>
   <td><tt>fdf4e7e230d9b5688d72f0e1a8039e12</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdeartwork-3.5.7.tar.bz2">kdeartwork-3.5.7</a></td>
   <td align="right">15MB</td>
   <td><tt>4ce75cd6f98b8662e450be735bc0b060</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdebase-3.5.7.tar.bz2">kdebase-3.5.7</a></td>
   <td align="right">23MB</td>
   <td><tt>b421e01b3ee712549ee967f58ed24de0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdebindings-3.5.7.tar.bz2">kdebindings-3.5.7</a></td>
   <td align="right">5.3MB</td>
   <td><tt>bc8a95f0cfd52ad0559a775cf045f230</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdeedu-3.5.7.tar.bz2">kdeedu-3.5.7</a></td>
   <td align="right">28MB</td>
   <td><tt>e2568148df3bf5aecec2ed21c4a0e0a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdegames-3.5.7.tar.bz2">kdegames-3.5.7</a></td>
   <td align="right">10MB</td>
   <td><tt>49ada123885195673d8bcbada4e9c82c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdegraphics-3.5.7.tar.bz2">kdegraphics-3.5.7</a></td>
   <td align="right">7.0MB</td>
   <td><tt>eae753e80c5f8dd304e7fd0dca84ae67</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdelibs-3.5.7.tar.bz2">kdelibs-3.5.7</a></td>
   <td align="right">14MB</td>
   <td><tt>50ed644f2ec91963570fe2b155652957</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdemultimedia-3.5.7.tar.bz2">kdemultimedia-3.5.7</a></td>
   <td align="right">6.0MB</td>
   <td><tt>3d18574ca14258fb565160aa84bf217c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdenetwork-3.5.7.tar.bz2">kdenetwork-3.5.7</a></td>
   <td align="right">8.9MB</td>
   <td><tt>d7f2a05a7e304b33128123c3f67ea636</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdepim-3.5.7.tar.bz2">kdepim-3.5.7</a></td>
   <td align="right">13MB</td>
   <td><tt>8571db6dcf4168614c96bb72c493f931</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdesdk-3.5.7.tar.bz2">kdesdk-3.5.7</a></td>
   <td align="right">4.9MB</td>
   <td><tt>6a8f7b7fea753e2a4517301dee76d84a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdetoys-3.5.7.tar.bz2">kdetoys-3.5.7</a></td>
   <td align="right">3.1MB</td>
   <td><tt>946e58b53ac4e6374051736a0eb4cf92</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdeutils-3.5.7.tar.bz2">kdeutils-3.5.7</a></td>
   <td align="right">2.9MB</td>
   <td><tt>5f167f53bdbf0b8c71c2d0f0ff7593fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdevelop-3.4.1.tar.bz2">kdevelop-3.4.1</a></td>
   <td align="right">8.9MB</td>
   <td><tt>abc6cc2831ad4c0f4da9fba9e38edce1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.7/src/kdewebdev-3.5.7.tar.bz2">kdewebdev-3.5.7</a></td>
   <td align="right">5.7MB</td>
   <td><tt>1329e0aea45947a14faa3d936f9edb5d</tt></td>
</tr>

</table>
