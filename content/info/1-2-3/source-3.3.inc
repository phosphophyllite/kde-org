<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/arts-1.3.0.tar.bz2">arts-1.3.0</a></td>
   <td align="right">952kB</td>
   <td><tt>6a0a03c24a20c43a8e443a5484bb9fef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdeaccessibility-3.3.0.tar.bz2">kdeaccessibility-3.3.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>17dc4ae94d0307a00e2b676818f49d63</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdeaddons-3.3.0.tar.bz2">kdeaddons-3.3.0</a></td>
   <td align="right">1.9MB</td>
   <td><tt>6ba019d8b3ce0811ae2c861d6819aaab</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdeadmin-3.3.0.tar.bz2">kdeadmin-3.3.0</a></td>
   <td align="right">1.9MB</td>
   <td><tt>7a4480c4e3b73206252397345d2055b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdeartwork-3.3.0.tar.bz2">kdeartwork-3.3.0</a></td>
   <td align="right">17MB</td>
   <td><tt>6e8ea5c980a770708ab639c49a8d5e0f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdebase-3.3.0.tar.bz2">kdebase-3.3.0</a></td>
   <td align="right">19MB</td>
   <td><tt>e8fc098ffb09fcc0a8fdc4446149a8e3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdebindings-3.3.0.tar.bz2">kdebindings-3.3.0</a></td>
   <td align="right">7.2MB</td>
   <td><tt>63f7cd3ae52397c2182527899efb4c80</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdeedu-3.3.0.tar.bz2">kdeedu-3.3.0</a></td>
   <td align="right">21MB</td>
   <td><tt>da972b3d4090290b6852dd50a32a2eee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdegames-3.3.0.tar.bz2">kdegames-3.3.0</a></td>
   <td align="right">9.3MB</td>
   <td><tt>bac48b11e98f7722954ec7327d36b74f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdegraphics-3.3.0.tar.bz2">kdegraphics-3.3.0</a></td>
   <td align="right">6.6MB</td>
   <td><tt>7bb9843f7b03cd2716079ac83b9c9304</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kde-i18n-3.3.0.tar.bz2">kde-i18n-3.3.0</a></td>
   <td align="right">176MB</td>
   <td><tt>f1927048807146969f6497b5d789fb5d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdelibs-3.3.0.tar.bz2">kdelibs-3.3.0</a></td>
   <td align="right">15MB</td>
   <td><tt>1c208724987433fc1929d22928c1a358</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdemultimedia-3.3.0.tar.bz2">kdemultimedia-3.3.0</a></td>
   <td align="right">5.6MB</td>
   <td><tt>2579f41004b39168da25cb4db0043f00</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdenetwork-3.3.0.tar.bz2">kdenetwork-3.3.0</a></td>
   <td align="right">7.1MB</td>
   <td><tt>ae7d989594ec2a7c073478e4535c284b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdepim-3.3.0.tar.bz2">kdepim-3.3.0</a></td>
   <td align="right">10MB</td>
   <td><tt>94520aeae0db2fac5da7d1ece7b575a5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdesdk-3.3.0.tar.bz2">kdesdk-3.3.0</a></td>
   <td align="right">4.6MB</td>
   <td><tt>d3f02d71b7211265a89a7e499faa1b61</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdetoys-3.3.0.tar.bz2">kdetoys-3.3.0</a></td>
   <td align="right">3.1MB</td>
   <td><tt>935e3e2e6d84c9fedb2f8acb8e36cdc7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdeutils-3.3.0.tar.bz2">kdeutils-3.3.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>9a3788e7ee386080b66254e515fa6e49</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdevelop-3.1.0.tar.bz2">kdevelop-3.1.0</a></td>
   <td align="right">8.0MB</td>
   <td><tt>a08e2792f895d4c96723edec17617567</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.3/src/kdewebdev-3.3.0.tar.bz2">kdewebdev-3.3.0</a></td>
   <td align="right">5.0MB</td>
   <td><tt>e29a344f426bb9875f6e731678bc159a</tt></td>
</tr>

</table>
