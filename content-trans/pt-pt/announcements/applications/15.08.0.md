---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: O KDE Lança as Aplicações do KDE 15.08.0.
layout: application
release: applications-15.08.0
title: O KDE Lança as Aplicações do KDE 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`O Dolphin com o novo visual - agora baseado nas Plataformas do KDE 5` class="text-center" width="600px" caption=`O Dolphin com o novo visual - agora baseado nas Plataformas do KDE 5`>}}

19 de Agosto de 2015. O KDE anuncia hoje o lançamento das Aplicações do KDE 15.08.

Com esta versão, um conjunto de 107 aplicações foi migrado para as <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Plataformas do KDE 5</a>. A equipa tenta ao máximo trazer a melhor qualidade para o seu ambiente de trabalho e para essas aplicações. Como tal, contamos consigo para enviar as suas reacções.

Com esta versão, existem novas adições à lista de aplicações baseadas nas Plataformas do KDE 5, incluindo o <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>o pacote Kontact</a>, o <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Antevisão técnica do pacote Kontact

Nos últimos meses, a equipa do KDE PIM aplicou bastante esforço na migração do Kontact para o Qt 5 e as Plataformas do KDE 5. Para além disso, a performance no acesso aos dados foi bastante melhorado, usando uma camada de comunicações optimizada. A equipa do KDE PIM está a trabalhar bastante nas melhorias ao pacote do Kontact e está à espera das suas reacções. Para mais informações detalhadas sobre as alterações no KDE PIM, veja o <a href='http://www.aegiap.eu/kdeblog/'>'blog' do Laurent Montel</a>.

### Kdenlive e Okular

Esta versão do Kdenlive inclui diversas correcções no assistente de DVD's, em conjunto com uma grande quantidade de correcções e outras funcionalidades, que incluem a integração de algumas reformulações importantes. Poderá encontrar mais informações sobre as alterações do Kdenlive no seu <a href='https://kdenlive.org/discover/15.08.0'>registo de alterações extenso</a>. Do mesmo modo, o Okular agora suporta a transição de Desvanecimento no modo de apresentação.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`O KSudoku com um 'puzzle' de letras` class="text-center" width="600px" caption=`O KSudoku com um 'puzzle' de letras`>}}

### Dolphin, Educação e Jogos

O Dolphin também foi migrado para as Plataformas do KDE 5. O Marble tem um suporte para <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> melhorado, assim como um melhor suporte para anotações, edição e camadas no KML.

O Ark teve uma quantidade espantosa de alterações que incluem diversas correcções pequenas. O Kstars recebeu um grande conjunto de alterações, incluindo a melhoria no algoritmo ADU plano e a verificação dos valores fora dos limites, a gravação da Inversão do Meridiano, o Desvio da Guia e o limite de Auto-Focagem HRF no ficheiro de sequência, adicionando também o suporte para a classificação e desbloqueio do telescópio. O KSudoku ficou ainda melhor. As alterações incluem: a adição de uma GUI e um motor para os 'puzzles' do Matedoku e do Sudoku Assassino, assim como a adição de um motor de resolução baseado no algoritmo de Ligações Dançarinas (DLX) de Donald Knuth.

### Outros Lançamentos

Em conjunto com esta versão, o Plasma 4 será publicado na sua versão LTS (suporte de longo prazo) pela última vez, como sendo a versão 4.11.22.
