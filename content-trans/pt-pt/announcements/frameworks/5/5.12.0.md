---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos Extra do CMake

- Melhoria da comunicação de erros da macro query_qmake

### BluezQt

- Remoção de todos os dispositivos do adaptador antes da remoção do adaptador (erro 349363)
- Actualização das referências no README.md

### KActivities

- Adição da opção para não registar dados do utilizador em determinadas actividades (de forma semelhante à 'navegação privada' num navegador Web)

### KArchive

- Preservação das permissões executáveis dos ficheiros no copyTo()
- Clarificação do ~KArchive com a remoção de código inactivo.

### KAuth

- Possibilidade de usar o 'kauth-policy-gen' a partir de diferentes fontes

### KBookmarks

- Não adicionar um favorito com o URL vazio e o texto em branco
- Codificação do URL do KBookmark para corrigir a compatibilidade com as aplicações do KDE4

### KCodecs

- Remoção do teste 'x-euc-tw'

### KConfig

- Instalação do 'kconfig_compiler' na 'libexec'
- Nova opção de geração de código TranslationDomain=, para usar com o TranslationSystem=kde; normalmente necessário nas bibliotecas.
- Possibilidade de usar o 'kconfig_compiler' a partir de diferentes fontes

### KCoreAddons

- KDirWatch: Só estabelecer uma ligação ao FM se for pedida
- Permitir a filtragem de 'plugins' e aplicações pelo formato do ecrã
- Possibilidade de usar o 'desktoptojson' a partir de diferentes fontes

### KDBusAddons

- Clarificação do valor de saída para as instâncias Unique

### KDeclarative

- Adição do clone para QQC do KColorButton
- Atribuição de um QmlObject para cada instância 'kdeclarative' se possível
- Resolução do Qt.quit() a partir do código em QML
- Junção da ramificação 'mart/singleQmlEngineExperiment'
- Implementação do 'sizeHint' com base no 'implicitWidth/height'
- Sub-classe do QmlObject com um motor estático

### Suporte para a KDELibs 4

- Correcção da implementação do KMimeType::Ptr::isNull.
- Reactivação do suporte para a serialização do KDateTime para o kDebug/qDebug, para mais SC
- Carregamento do catálogo de traduções correcto para o kdebugdialog
- Não ignorar a documentação de métodos obsoletos, para que as pessoas possam ler as sugestões de migração

### KDESU

- Correcção do CMakeLists.txt para passar o KDESU_USE_SUDO_DEFAULT à compilação, para que seja usado pelo 'suprocess.cpp'

### KDocTools

- Actualização dos modelos Docbook do KF5

### KGlobalAccel

- A API de execução privada é instalada para permitir ao KWin oferecer 'plugins' para o Wayland.
- Contingência na resolução de nomes do 'componentFriendlyForAction'

### KIconThemes

- Não tentar pintar o ícone, caso o tamanho seja inválido

### KItemModels

- Novo modelo de 'proxy': KRearrangeColumnsProxyModel. Suporta a reordenação e ocultação de colunas no modelo de origem.

### KNotification

- Correcção dos tipos de imagens no org.kde.StatusNotifierItem.xml
- [ksni] Adição de um método para obter a acção pelo seu nome (erro 349513)

### KPeople

- Implementação das funcionalidades de filtragem do PersonsModel

### KPlotting

- KPlotWidget: adição do setAutoDeletePlotObjects, correcção de fuga de memória no replacePlotObject
- Correcção dos traços em falta quando o x0 &gt; 0.
- KPlotWidget: não é necessário o 'setMinimumSize' ou o 'resize'.

### KTextEditor

- debianchangelog.xml: adição do Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Correcção do par subjugado do UTF-16 para o comportamento do Backspace/Delete.
- Permitir ao QScrollBar lidar com os WheelEvents - eventos da roda (erro 340936)
- Aplicação da modificação da actualização em desenvolvimento do KWrite para o realce básico e puro - "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Correcção da activação/desactivação do botão Ok

### Plataforma da KWallet

- Importação e melhoria da ferramenta da linha de comandos 'kwallet-query'.
- Suporte para a sobreposição de itens dos mapas.

### KXMLGUI

- Não mostrar a "Versão das Plataformas do KDE" na janela "Acerca do KDE"

### Plataforma do Plasma

- Tornou-se o tema escuro completamente escuro, e foi feito o mesmo para o grupo complementar
- 'Cache' do 'naturalsize' em separado por factor de escala
- ContainmentView: Não estoirar com meta-dados inválidos do Corona
- AppletQuickItem: Não aceder ao KPluginInfo se não for válido
- Correcção das páginas ocasionais de configuração vazias (erro 349250)
- Melhoria do suporte de altos DPI's no componente de grelha do Calendário
- Verificação se o KService tem informações válidas do 'plugin' antes de o usar
- [calendário] Certificação que a grelha é actualizada com as mudanças de temas
- [calendário] Começar sempre a contar as semanas a partir da Segunda-Feira (erro 349044)
- [calendário] Actualização da grelha quando a opção para mostrar os números da semana muda
- Agora é usado um tema opaco quando só está disponível o efeito de borrão (erro 348154)
- Lista de permissões das versões/'applets' para um motor separado
- Introdução de uma nova classe ContainmentView

### Sonnet

- Permissão para usar o realce da verificação ortográfica num QPlainTextEdit

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
