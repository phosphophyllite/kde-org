---
date: 2013-08-14
hidden: true
title: A Área de Trabalho do Plasma 4.11 Continua a Refinar a Experiência do Utilizador
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`Área de Trabalho Plasma do KDE 4.11` width="600px" >}}

Na versão 4.11 da Área de Trabalho Plasma, a barra de tarefas – um dos elementos mais usados no Plasma –  <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>foi adaptado para o QtQuick</a>. A nova barra de tarefas, embora mantenha a aparência e funcionalidade da sua equivalente anterior, mostra um comportamento mais consistente e fluente. A remodelação também resolveu um conjunto de erros prolongados. O item da bateria (que anteriormente também podia ajustar o brilho do ecrã) agora também suporta o brilho do teclado, podendo lidar com várias baterias nos dispositivos periféricos, como o seu rato e teclado sem fios. Mostra a carga da bateria de cada dispositivo e avisa quando uma delas está fraca. O menu Kickoff agora mostra as aplicações recentemente instaladas nos últimos dias. Por último, as mensagens de notificação agora têm um botão de configuração onde uma pessoa poderá facilmente mudar a configuração para esse tipo de notificação em particular.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Tratamento de notificações melhorado` width="600px" >}}

O KMix, a mesa de mistura do KDE, recebeu grandes desenvolvimentos de performance e estabilidade, assim como um <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>suporte de controlo multimédia completo</a>, baseado na norma MPRIS2.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`A 'applet' de bateria remodelada em acção` width="600px" >}}

## Gestor e Compositor de Janelas KWin

O nosso gestor de janelas, o KWin, recebeu uma vez mais grandes actualizações, desviando-se ainda mais de tecnologias antiquadas e incorporando o protocolo de comunicações 'XCB'. Isto resulta numa gestão de janelas mais suave e mais rápida. Foi introduzido o suporte para o OpenGL 3.1 e para o OpenGL ES 3.0. Esta versão também incorpora o primeiro suporte experimental para o sucessor do X11, o Wayland. Isto permite usar o KWin com o X11 sobre uma arquitectura do Wayland. Para mais informações sobre a utilização deste modo experimental, veja <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>esta publicação</a>. A interface de programação do KWin recebeu grandes melhorias, adicionando o suporte para a interface de configuração, novas animações e efeitos gráficos, assim como outras pequenas melhorias. Esta versão traz um melhor suporte multi-ecrãs (incluindo uma opção de brilho nos extremos para os 'cantos activos'), uma melhor e mais rápida colocação lado-a-lado (com áreas configuráveis) e a evolução normal de correcções de erros e optimizações. Veja mais detalhes <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>aqui</a> e <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>aqui</a>.

## Tratamento de Monitores e Atalhos da Web

A configuração do monitor na Configuração do Sistema foi <a href='http://www.afiestas.org/kscreen-1-0-released/'>substituída pela nova ferramenta KScreen</a>. O KScreen traz um melhor suporte brings inteligente para vários monitores para a área de trabalho do Plasma, configurando automaticamente os ecrãs novos e recordando as configurações manuais dos monitores. Oferece uma interface intuitiva e visual, tratando da reorganização dos monitores através de um simples arrastamento.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`O novo tratamento de monitores do KScreen` width="600px" >}}

Os Atalhos da Web, a forma mais simples de descobrir rapidamente o que precisa na Web, foram reorganizados e melhorados. Muitos foram actualizados para usar as ligações seguras e encriptadas (TLS/SSL), foram adicionados novos atalhos da Web e alguns antigos foram removidos. O processo de adição dos seus próprios atalhos da Web foi também melhorado. Descubra mais detalhes <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>aqui</a>.

Esta versão marca o fim da Área de Trabalho Plasma 1, que faz parte da série de funcionalidades do KDE SC 4. Para facilitar a transição para a próxima geração, esta versão será suportada durante pelo menos 2 anos. O foco no desenvolvimento de funcionalidades será desviado agora para a Área de Trabalho Plasma 2, enquanto as melhorias de performance e as correcções de erros serão concentradas na série 4.11.

#### Instalar o Plasma

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. <br />

##### Localizações dos Pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.11.0'>transferir à vontade</a> o código-fonte completo de 4.11.0. As instruções de compilação e instalação da aplicação do KDE 4.11.0 está disponível na <a href='/info/4/4.11.0#binary'>Página de Informações do 4.11.0</a>.

#### Requisitos do Sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.<br />Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.

## Também Anunciado Hoje:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="As Aplicações do KDE 4.11"/> As Aplicações do KDE 4.11 Dão um Enorme Passo em Frente na Gestão de Informações Pessoais e Tiveram Melhorias em Todo o Lado</a>

Esta versão marca grandes melhorias na plataforma do KDE PIM, ganhando uma performance muito melhor e muitas funcionalidades novas. O Kate melhora a produtividade dos programadores de Python e JavaScript com novos 'plugins', o Dolphin ficou mais rápido e as aplicações educativas tiveram muitas novas funcionalidades.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento do KDE 4.11"/> A Plataforma do KDE 4.11 Oferece uma Melhor Performance</a>

Esta versão da Plataforma do KDE 4.11 continua a focar-se na estabilidade. As funcionalidades novas estão a ser implementadas na nossa futura versão 5.0 da plataforma, mas para a versão estável continuámos a esforçar a optimização da nossa plataforma Nepomuk.
