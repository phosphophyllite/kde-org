---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.
title: KDE Softver Kompilacija 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma Radno okruženje 4.11` >}} <br />

August 14, 2013. The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.<br />

This release is dedicated to the memory of <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, a great Free and Open Source Software champion from India. Atul led the Linux Bangalore and FOSS.IN conferences since 2001 and both were landmark events in the Indian FOSS scene. KDE India was born at the first FOSS.in in December 2005. Many Indian KDE contributors started out at these events. It was only because of Atul's encouragement that the KDE Project Day at FOSS.IN was always a huge success. Atul left us on June 3rd after fighting a battle with cancer. May his soul rest in peace. We are grateful for his contributions to a better world.

Sva ova izdanja su prevedena na 54 jezika; očekujemo da bude dodano još jezika u narednim mjesečnim izdanjima KDE-a s minornim popravkama grešaka. Dokumentacijski tim je ažurirao 91 aplikacijski priručnik za ovo izdanje.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Izdanje KDE Platforme 4.11 nastavlja s fokusiranjem na stabilnost. Nove mogućnosti su implementirane za naša buduća KDE Frameworks 5.0 izdanja, ali za stabilna izdanja uspjeli smo ugurati optimizacije za naš Nepomuk framework.

<br />
When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.<br />

## Širite riječ i vidite šta se dešava: Tag as &quot;KDE&quot;

KDE ohrabruje ljude da šire riječ na društvenim mrežama. Podijelite priče na stranice sa novostima, koristite kanale kao što su delicious, digg, reddit, twitter, identi.ca. Učitajte slike na mreže kao što su Facebook, Flickr, ipernity i Picasa i podijelite ih sa odgovarajućim grupama. Napravite animacije i učitajte ih na YouTube, Blip.tv, i Vimeo. Označite objavljene materijale sa  &quot;KDE&quot;. Ovo olakšava pronalaženje i daje KDE Promo timu način da analizira pokrivenost za 4.11 izdanje KDE softvera.

## Zabave povodom puštanja

Kao i obično, KDE zajednica članova povodom izdanja organizuje zabave širom svijeta. Nekoliko je već u rasporedu i još mnogo njih će biti poslije. Pronađite <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'> listu zabava ovdje</a>. Svi su dobrodošli! Tu će biti kombinacija zanimljivih kompanija i inspirativnih govora kao i hrana i piće. To je sjajna prilika da naučite više o tome šta se dešava u KDE-u, da se uključite ili samo upoznate druge korisnike i saradnike.

Mi ohrabrujemo ljude da organizuju vlastite zabave. Oni su zabavni domaćini i otvoreni su za svakoga. Provjerite <a href='http://community.kde.org/Promo/Events/Release_Parties'> savjete kako organizovati zabavu</a>.

## O ovim najavama izdanja

These release announcements were prepared by Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.

#### Podržite KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
