---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: KDE rilascia Applications 15.04.
layout: application
title: KDE rilascia KDE Applications 15.04.0
version: 15.04.0
---
15 aprile 2015. Oggi KDE ha pubblicato KDE Applications 15.04. Con questa versione un totale di 72 applicazioni è stato portato su <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. La squadra si sta impegnando per portare la migliore qualità sul tuo desktop e queste applicazioni. Contiamo su di te per inviare il tuo riscontro.

Questo rilascio comprende varie aggiunte alla lista delle applicazioni basate su KDE Frameworks 5, inclusi <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a> e <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> e <a href='https://games.kde.org/'>alcuni giochi</a>.

Kdenlive è uno dei migliori software di editing video non lineare disponibile. Di recente ha terminato il suo <a href='https://community.kde.org/Incubator'>processo di incubazione</a> per diventare un progetto KDE ufficiale ed è stato portato su KDE Frameworks 5. La squadra dietro questo capolavoro ha deciso che Kdenlive doveva essere rilasciato insieme a KDE Applications. Alcune nuove funzionalità sono la funzione di salvataggio automatico di nuovi progetti e una stabilizzazione corretta dei filmati.

KDE Telepathy è lo strumento per la messaggistica istantanea. È stato portato su KDE Frameworks 5 e Qt5 ed è un nuovo membro dei rilasci di KDE Applications. È per lo più completo, tranne l'interfaccia utente per le chiamate audio e video che è ancora mancante.

Ove possibile KDE utilizza la tecnologia esistente come è stato fatto con il nuovo KAccounts che viene utilizzato anche in Unity da SailfishOS e Canonical. All'interno di KDE Applications, è attualmente utilizzato solo da KDE Telepathy. Ma in futuro, vedrà un uso più ampio per applicazioni come Kontact e Akonadi.

Nel <a href='https://edu.kde.org/'>modulo KDE Education</a>, Cantor ha ottenuto alcune nuove funzionalità relative al suo supporto Python: un nuovo motore Python 3 e nuove categorie di Ottieni novità. Rocs è stato rivoltato: il nucleo della teoria dei grafi è stato riscritto, la separazione della struttura dei dati è stata rimossa e un documento di grafi più generale come entità centrale è stata introdotta e una revisione importante dell'API di creazione script per gli algoritmi dei grafi che ora fornisce solo una API unificata. KHangMan è stato convertito in QtQuick e ha dato una rinfrescata al processo. E Kanagram ha ricevuto una nuova modalità a 2 giocatori e le lettere sono ora pulsanti su cui è possibile fare clic e possono essere digitati come prima.

Oltre alle solite correzioni di bug <a href='https://www.kde.org/applications/development/umbrello/'>Umbrello </a> questa volta ha ottenuto alcuni miglioramenti di usabilità e stabilità. Inoltre, la funzione Trova può ora essere limitata per categoria: classe, interfaccia, pacchetto, operazioni o attributi.
