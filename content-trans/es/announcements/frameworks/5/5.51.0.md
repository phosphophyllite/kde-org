---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Se han añadido llamadas a KIO::UDSEntry::reserve en los esclavos de E/S «timeline» y «tags».
- [balooctl] Vaciar la línea en buffer «Indexando &lt;archivo&gt;» cuando se inicia la indexación.
- [FileContentIndexer] Conectar la señal «finished» del proceso extractor.
- [PositionCodec] Evitar un cuelgue si existen datos dañados (error 367480).
- Se ha corregido una constante «char» no válida.
- [Balooctl] Eliminar la comprobación del directorio padre (error 396535).
- Permitir la eliminación de carpetas no existentes de las listas de inclusiones y exclusiones (error 375370).
- Usar «String» para almacenar UDS_USER y UDS_GROUP de tipo String (error 398867).
- [tags_kio] Se han corregido paréntesis.
- Excluir archivos de GNOME de la indexación.

### BluezQt

- Implementar las API «Media» y «MediaEndpoint».

### Iconos Brisa

- Se ha corregido «stack-use-after-scope» detectado por ASAN en CI.
- Se ha corregido la falta de hojas de estilos en iconos monocromáticos.
- Se ha cambiado el icono de disco duro a un estilo más adaptable.
- Se han añadido los iconos «firewall-config» y «firewall-applet».
- Se ha hecho visible el bloqueo en el icono de la caja fuerte de Plasma en Brisa oscuro.
- Se ha añadido el símbolo más al icono «document-new.svg» (error 398850).
- Se proporcionan iconos para la escala 2x.

### Módulos CMake adicionales

- Compilar los vínculos de Python con los mismos indicadores SIP que usa PyQt.
- Android: Permitir que se pueda pasar una ruta relativa como directorio de APK.
- Android: Ofrecer un recurso adecuado a las aplicaciones que no poseen un manifiesto.
- Android: Asegurar que se cargan las traducciones de Qm.
- Se han corregido las compilaciones de Android que usan cmake 3.12.1.
- l10n: Se ha corregido la coincidencia de dígitos en el nombre del repositorio.
- Se ha añadido QT_NO_NARROWING_CONVERSIONS_IN_CONNECT a los indicadores de compilación por omisión.
- Vínculos: Se ha corregido el manejo fuentes que contienen utf-8.
- Iterar realmente sobre CF_GENERATED en lugar de comprobar el elemento 0 todo el tiempo.

### KActivities

- Se ha corregido que la referencia colgante con «auto» se convierta en «QStringBuilder».

### KCMUtils

- Gestionar los eventos devueltos.
- Cambiar manualmente el tamaño de KCMUtilDialog a sizeHint() (error 389585).

### KConfig

- Se ha corregido un problema al leer listas de rutas.
- El compilador de kcfg documenta ahora entradas válidas del tipo «Color».

### KFileMetaData

- Se ha eliminado la implementación propia de la conversión de QString en TString para «taglibwriter».
- Se ha aumentado la cobertura de las pruebas de «taglibwriter».
- Se han implementado más etiquetas básicas para «taglibwriter».
- Se ha eliminado el uso de la función propia para convertir TString en QString.
- Se ha cambiado la versión necesaria de «taglib» a 1.11.1.
- Se ha implementado la lectura de las etiquetas «replaygain».

### KHolidays

- Se han añadido los festivos de Costa de Marfil (en francés) (error 398161).
- holiday*hk**: Se ha corregido la fecha del festival Tuen Ng para 2019 (error 398670).

### KI18n

- Delimitar correctamente el cambio de CMAKE_REQUIRED_LIBRARIES.
- Android: Asegurar que se buscan los archivos .mo en la ruta correcta.

### KIconThemes

- Empezar a dibujar emblemas en la esquina inferior derecha.

### KImageFormats

- kimg_rgb: Optimizar poco a poco QRegExp y QString::fromLocal8Bit.
- [EPS] Se ha corregido un cuelgue al cerrar aplicaciones (intentando hacer persistente la imagen del portapapeles) (error 397040).

### KInit

- Disminuir la basura registrada no comprobando la existencia de archivos con nombre vacío (error 388611).

### KIO

- Permitir la redirección no local de file:// a una URL WebDav de Windows.
- [KFilePlacesView] Se ha cambiado el icono para la entrada «Edición» del menú de contexto del panel «Lugares».
- [Panel «Lugares»] Usar un icono más apropiado para la red.
- [KPropertiesDialog] Mostrar información de montaje para las carpetas de / (raíz).
- Se ha corregido el borrado de archivos de DAV (error 355441).
- Evitar QByteArray::remove en AccessManagerReply::readData (error 375765).
- No intentar restaurar lugares del usuario no válidos.
- Permitir que se puede cambiar al directorio superior, incluso cuando hay barras al final de su URL.
- Los fallos del esclavo KIO se manejan ahora con KCrash en lugar de con un código personalizado.
- Se ha corregido que los archivos creados al pegar contenido del portapapeles se muestren solo tras una demora.
- [PreviewJob] Enviar los complementos de miniaturas activos al esclavo de creación de miniaturas (error 388303).
- Se ha mejorado el mensaje de error «espacio en disco insuficiente».
- IKWS: Usar «X-KDE-ServiceTypes» en la generación de archivos de escritorio.
- Se ha corregido la cabecera de destino WebDAV en las operaciones de copiar y mover.
- Advertir al usuario si no hay espacio suficiente antes de las operaciones de copiar o mover (error 243160).
- Mover el módulo de control de SMB a la categoría «Preferencias» de «Red».
- Papelera: Se ha corregido el análisis de la caché de tamaños de directorio.
- kioexec: vigilar la creación y la modificación de archivos temporales (error 397742).
- No dibujar marcos ni sombras alrededor de las imágenes que tienen transparencia (error 258514).
- Se ha corregido el icono del tipo de archivo en el diálogo de propiedades, que se mostraba borroso en pantallas de alta resolución.

### Kirigami

- Abrir correctamente el cajón cuando se arrastra por el asa.
- Margen adicional cuando «globaltoolbar» de «pagerow» es ToolBar.
- Admitir también «Layout.preferredWidth» como tamaño de página.
- Deshacerse de los últimos restos de «controls1».
- Permitirla creación de separadores de acciones.
- Consentir cualquier número de columnas en CardsGridview.
- No destruir activamente los elementos del menú (error 397863).
- Los iconos de «actionButton» son monocromáticos.
- No hacer que los iconos sean monocromáticos cuando no deben serlo.
- Restaurar el tamaño arbitrario *1,5 de los iconos en Mobile.
- Reciclador de delegados: no solicitar el objeto de contexto dos veces.
- Usar la implementación «material ripple» interna.
- Controlar la anchura de la cabecera con «sourcesize» si es horizontal.
- Exponer todas las propiedades de «BannerImage» en las tarjetas.
- Usar «DesktopIcon» incluso en Plasma.
- Cargar de forma correcta las rutas «file://».
- Revertir «Empezar a buscar el contexto desde el propio delegado».
- Añadir un caso de prueba que delimita el problema del ámbito de «DelegateRecycler».
- Definir una altura de forma explícita para «overlayDrawers» (error 398163).
- Empezar a buscar el contexto desde el propio delegado.

### KItemModels

- Usar referencias en bucles «for» para tipos con constructor de copia no trivial.

### KNewStuff

- Añadir el uso de etiquetas de Attica (error 398412).
- [KMoreTools] Se ha proporcionado un icono apropiado al elemento de menú «Configurar...» (error 398390).
- [KMoreTools] Se ha reducido la jerarquía del menú.
- Se ha corregido «Imposible de usar archivo knsrc para envíos desde ubicación no estándar» (error 397958).
- Enlazar las herramientas de pruebas de Make en Windows.
- No romper la compilación con Qt 5.9.
- Añadir el uso de etiquetas de Attica.

### KNotification

- Se ha corregido un fallo causado por la gestión incorrecta del tiempo de vida de notificaciones de sonido basadas en «canberra» (error 398695).

### KNotifyConfig

- Se ha corregido la sugerencia de archivo de interfaz de usuario: KUrlRequester ahora tiene QWidget como clase base.

### Framework KPackage

- Usar referencias en bucles «for» para tipos con constructor de copia no trivial.
- Mover Qt5::DBus a destinos de enlace «PRIVATE».
- Emitir señales cuando se instala o se desinstala un paquete.

### KPeople

- Se ha corregido que no se envíen señales al fusionar dos personas.
- No fallar si se elimina una persona.
- Definir «PersonActionsPrivate» como clase, como declarada anteriormente.
- Hacer pública la API de PersonPluginManager.

### Kross

- Núcleo: Mejor manejo de los comentarios de las acciones.

### KTextEditor

- Pintar el marcador de plegado de código solo en regiones de plegado de código multilínea.
- Inicializar «m_lastPosition».
- Scripting: isCode() devuelve «false» para el texto «dsAlert» (error 398393).
- Usar el resaltado de scripts de R para pruebas de sangría de R.
- Actualizar el script de sangría de R.
- Se han corregido los esquemas de color Solarizado (claro y oscuro) (error 382075).
- Hacer que no sea necesario Qt5::XmlPatterns.

### KTextWidgets

- KTextEdit: carga relajada del objeto QTextToSpeech.

### Framework KWallet

- Registrar los errores de fallos de apertura de carteras.

### KWayland

- No producir un error silencioso si se envía «damage» antes del buffer (error 397834).
- [servidor] No retornar prematuramente al producirse un fallo en el código alternativo de «touchDown».
- [servidor] Se ha corregido el manejo del buffer en acceso remoto cuando la salida no está delimitada.
- [servidor] No intentar crear ofrecimientos de datos sin fuente.
- [servidor] Interrumpir el inicio de arrastre en condiciones correctas y sin mostrar error.

### KWidgetsAddons

- [KCollapsibleGroupBox] Respetar la duración de la animación del widget del estilo (error 397103).
- Eliminar la comprobación antigua de la versión de Qt.
- Compilar

### KWindowSystem

- Usar _NET_WM_WINDOW_TYPE_COMBO en lugar de _NET_WM_WINDOW_TYPE_COMBOBOX.

### KXMLGUI

- Se ha corregido la URL del proveedor de OCS en el diálogo «Acerca de».

### NetworkManagerQt

- Usar el valor de enumeración «AuthEapMethodUnknown» para comparar un «AuthEapMethod».

### Framework de Plasma

- Actualizar las cadenas de versión del tema porque hay nuevos iconos en 5.51.
- Mostrar la ventana de configuración cuando vuelva a usarse.
- Se ha añadido un componente que faltaba: RoundButton.
- Combinar los archivos de iconos OSD y moverlos al tema de iconos de Plasma (error 395714).
- [Deslizador de Plasma Components 3] Se ha corregido el tamaño implícito del asa.
- [Lista desplegable de Plasma Components 3] Cambiar las entradas con la rueda del ratón.
- Permitir el use de iconos de botones cuando están presentes.
- Se han corregido los nombres de las semanas que no se mostraban correctamente en el calendario cuando la semana empieza por un día distinto del lunes o del domingo (error 390330).
- [DialogShadows] Usar un desplazamiento 0 para los bordes desactivados en Wayland.

### Prison

- Se ha corregido la generación de códigos Aztec con una proporción !=1.
- Se ha eliminado la suposición sobre la proporción del código de barras de la integración de QML.
- Se han corregido los fallos técnicos de visualización causados por errores de redondeo en el código 128.
- Se ha añadido soporte para los códigos de barras Code 128.

### Purpose

- Hacer que cmake 3.0 sea la versión mínima de cmake.

### QQC2StyleBridge

- Pequeño relleno predeterminado cuando existe un fondo.

### Solid

- No mostrar un emblema para los discos montados. Solo se muestra en los discos desmontados.
- [Fstab] Se ha eliminado el soporte de AIX.
- [Fstab] Se ha eliminado el soporte de Tru64 (**osf**).
- [Fstab] Mostrar un nombre de recurso compartido no vacío en el caso de que el sistema de archivos raíz se exporte (error 395562).
- Preferir la etiqueta que proporciona el dispositivo también para dispositivos de bucle.

### Sonnet

- Se ha corregido la rotura de la suposición del lenguaje.
- Impedir que el sistema de resaltado borre el texto seleccionado (error 398661).

### Resaltado de sintaxis

- i18n: Se ha corregido la extracción de nombres de temas.
- Fortran: Resaltar las alertas en los comentarios (error 349014).
- Evitar que el contexto principal pueda ser #poped.
- Salvaguardia de transición de estado sin fin.
- YAML: Se han añadido los estilos literal y plegado de código (error 398314).
- Logcat y SELinux: Mejoras para los nuevos esquemas solarizados.
- AppArmor: Se han corregido cuelgues al abrir las reglas (en KF5.50) y mejoras para el nuevo tema solarizado.
- Se ha fusionado git://anongit.kde.org/syntax-highlighting
- Se han actualizado las cosas que debe ignorar git.
- Usar referencias en bucles «for» para tipos con constructor de copia no trivial.
- Corrección: Resaltado de correos electrónicos para paréntesis sin cerrar en la cabecera del asunto (error 398717).
- Perl: Se han corregido llaves, variables, referencias de cadenas y otros (error 391577).
- Bash: Se ha corregido la expansión de parámetros y de llaves (error 387915).
- Se han añadido los temas solarizado claro y oscuro.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
