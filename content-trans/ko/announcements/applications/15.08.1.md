---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE에서 KDE 프로그램 15.08.1 출시
layout: application
title: KDE에서 KDE 프로그램 15.08.1 출시
version: 15.08.1
---
September 15, 2015. Today KDE released the first stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdelibs, kdepim, Kdenlive, Dolphin, Marble, Kompare, Konsole, Ark 및 Umbrello 등에 40개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.12.
