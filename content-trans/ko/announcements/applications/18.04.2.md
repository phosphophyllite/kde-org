---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE에서 KDE 프로그램 18.04.2 출시
layout: application
title: KDE에서 KDE 프로그램 18.04.2 출시
version: 18.04.2
---
June 7, 2018. Today KDE released the second stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- Gwenview에서 그림 작업을 실행 취소한 후 다시 실행할 수 있음
- KGpg에서 버전 헤더가 없는 메시지를 복호화할 때 오류가 발생하지 않음
- Maxima 행렬을 포함하는 Cantor 워크시트를 LaTeX로 내보내는 문제를 수정함
