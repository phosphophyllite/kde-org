---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDEk, KDE Aplikazioak 18.04.1 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 18.04.1 kaleratzen du
version: 18.04.1
---
May 10, 2018. Today KDE released the first stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, Gwenview, JuK, Okular, Umbrello, among others.

Improvements include:

- Duplicate entries in Dolphin's places panel no longer cause crashes
- An old bug with reloading SVG files in Gwenview was fixed
- Umbrello's C++ import now understands the 'explicit' keyword
