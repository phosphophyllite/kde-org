---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: O KDE Lança as Aplicações do KDE 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: O KDE Lança as Aplicações do KDE 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../19.08.0'>Aplicações do KDE 19.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de uma dúzia de correções de erros registradas incluem melhorias no Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, entre outros.

As melhorias incluem:

- No editor de vídeo Kdenlive, as composições já não desaparecem mais ao reabrir um projeto com as faixas travadas
- A visualização de anotações do Okular agora mostra o fuso horário local ao invés de UTC
- O controle via teclado do utilitário de captura de tela Spectacle foi melhorado
