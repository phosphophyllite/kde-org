---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Ícones Breeze

- Muitos ícones novos
- Adição dos ícones de tipos MIME do VirtualBox e de mais alguns tipos MIME que faltavam
- Adição do suporte a ícones do Synaptic e Octopi
- Correção do ícone de recorte (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354061'>354061</a>)
- Correção do nome do audio-headphones.svg (+=d)
- Ícones de classificação com margens menores (1px)

### Integração do Framework

- Remoção nome do arquivo possível em KDEPlatformFileDialog::setDirectory()
- Não filtrar por nome, caso existam tipos MIME

### KActivities

- Remoção da dependência do Qt5::Widgets
- Remoção da dependência do KDBusAddons
- Remoção de dependência do KI18n
- Remoção de inclusões não usadas
- Resultado melhorado dos scripts shell
- Adição do modelo de dados (ActivitiesModel) que mostra as atividades para a biblioteca
- Compilação apenas da biblioteca por padrão
- Remoção das componentes do serviço e do espaço de trabalho a partir da compilação
- Mudança da biblioteca de 'src/lib/core' para 'src/lib'
- Correção de avisos do CMake
- Correção da falha no menu de contexto das atividades (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351485'>351485</a>)

### KAuth

- Correção de bloqueio do <i>kded5</i> quando um programa que usa o <i>kauth</i> é fechado

### KConfig

- KConfigIniBackend: Correção da dissociação dispendiosa na pesquisa

### KCoreAddons

- Correção da migração da configuração do Kdelibs4 para o Windows
- Adição de uma API para obter a informação da versão em execução dos Frameworks
- KRandom: Não usar 16K do /dev/urandom como base para o rand() (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359485'>359485</a>)

### KDeclarative

- Não chamar um ponteiro para um objeto nulo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=347962'>347962</a>)

### KDED

- Possibilidade de compilação com o -DQT_NO_CAST_FROM_ASCII

### KDELibs 4 Support

- Correção do gerenciamento de sessões nos aplicativos baseados no KApplication (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354724'>354724</a>)

### KDocTools

- Uso de caracteres Unicode para as chamadas de atenção

### KFileMetaData

- O KFileMetadata consegue agora pesquisar e armazenar informações sobre o e-mail original em que um arquivo salvo foi associado como anexo

### KHTML

- Correção da atualização do cursor na janela
- Limite no uso da memória em strings
- Visualizador de miniaplicativos Java do KHTML: Reparação da chamada inválida D-Bus ao <b>kpasswdserver</b>

### KI18n

- Uso de uma macro de importação portável para o <i>nl_msg_cat_cntr</i>
- Ignorar a pesquisa do '.' e '..' para descobrir as traduções de um aplicativo
- Restrição do uso do <i>_nl_msg_cat_cntr</i> para as implementações do GNU gettext
- Adição do KLocalizedString::languages()
- Colocação das chamadas do Gettext apenas se o catálogo for localizado

### KIconThemes

- Garantia de que a variável está sendo inicializada

### KInit

- kdeinit: Preferência no carregamento das bibliotecas a partir do RUNPATH
- Implementação da utilização do QUrl::fromStringList

### KIO

- Correção da quebra de conexão do <i>app-slave</i> do KIO se a variável <i>appName</i> tiver uma '/' (erro <a href='https://bugs.kde.org/show_bug.cgi?id=357499'>357499</a>)
- Tentativa de vários métodos de autenticação em caso de falha
- Ajuda: Correção do mimeType() no get()
- KOpenWithDialog: Apresentação do nome e comentário do tipo MIME no texto da opção "Lembrar" (erro <a href='https://bugs.kde.org/show_bug.cgi?id=110146'>110146</a>)
- Uma série de alterações para evitar uma nova listagem de uma pasta após renomear um arquivo em mais casos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359596'>359596</a>)
- http: Mudança do nome do <i>m_iError</i> para <i>m_kioError</i>
- kio_http: leitura e eliminação do conteúdo após um erro 404 com <i>errorPage=false</i>
- kio_http: correção de detecção do tipo MIME quando a URL termina com '/'
- FavIconRequestJob: Adição do método <i>hostUrl()</i> para que o Konqueror possa descobrir para o que foi a tarefa no <i>slot</i>
- FavIconRequestJob: correção do bloqueio ao interromper uma tarefa, no caso de um <i>favicon</i> ser muito grande
- FavIconRequestJob: correção do <i>errorString()</i>, que tinha apenas a URL
- KIO::RenameDialog: Retorno do suporte para a visualização, adição das legendas de data e tamanho (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356278'>356278</a>)
- KIO::RenameDialog: Remodelação de código duplicado
- Correção de conversões inválidas de caminho-para-QUrl
- Uso do <b>kf5.kio</b> no nome da categoria para corresponder a outras categorias

### KItemModels

- KLinkItemSelectionModel: Adição de um novo construtor padrão
- KLinkItemSelectionModel: Permissão para definir o modelo de seleção associada
- KLinkItemSelectionModel: Tratamento das alterações no modelo <i>selectionModel</i>
- KLinkItemSelectionModel: Não armazenar localmente o modelo
- KSelectionProxyModel: Correção de erro na iteração
- Restauração do estado do <b>KSelectionProxyModel</b> quando necessário
- Adição de uma propriedade que indica se os modelos formam uma cadeia conectada
- KModelIndexProxyMapper: Simplificação da lógica de verificação da conexão

### KJS

- Limite no uso da memória em strings

### KNewStuff

- Apresentação de um aviso em caso de erro no "Mecanismo"

### Package Framework

- Marcação do KDocTools como opcional no KPackage

### KPeople

- Correção do uso de API obsoleta
- Adição do <i>actionType</i> ao plugin <i>declarative</i>
- Inversão da lógica de filtragem no <b>PersonsSortFilterProxyModel</b>
- Pequena melhoria na usabilidade do exemplo em QML
- Adição do <i>actionType</i> ao <b>PersonActionsModel</b>

### KService

- Simplificação do código, redução das eliminações de referências de ponteiros, melhorias relacionadas ao contêiner
- Adição do programa de testes <i>kmimeassociations_dumper</i>, inspirado no erro <a href='https://bugs.kde.org/show_bug.cgi?id=359850'>359850</a>
- Correção dos aplicativos Chromium/Wine que não carregavam em algumas distribuições (erro <a href='https://bugs.kde.org/show_bug.cgi?id=213972'>213972</a>)

### KTextEditor

- Correcção do realce de todas as ocorrências no ReadOnlyPart
- Não interagir sobre uma <b>QString</b> como se fosse uma <b>QStringList</b>
- Inicialização adequada de <b>QMaps</b> estáticos
- Preferência para o <b>toDisplayString(QUrl::PreferLocalFile)</b>
- Suporte do envio de caracteres substitutos a partir do método de introdução
- Não falhar durante o desligamento, quando a animação de texto estiver ainda em execução

### KWallet Framework

- Confirmação de que o <b>KDocTools</b> é pesquisado
- Não passar um número negativo ao D-Bus porque falha na <i>libdbus</i>
- Limpeza dos arquivos <i>cmake</i>
- KWallet::openWallet(Synchronous): Não expirar ao fim do tempo-limite de 25 segundos

### KWindowSystem

- Suporte para o _NET_WM_BYPASS_COMPOSITOR (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349910'>349910</a>)

### KXMLGUI

- Uso do nome não-nativo do idioma como alternativa
- Correção do gerenciamento de sessões que não funcionava no KF5 / Qt5 (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354724'>354724</a>)
- Esquemas de atalhos: Suporte para esquemas instalados globalmente
- Uso do <b>qHash(QKeySequence)</b> do Qt ao compilar com o Qt 5.6+
- Esquemas de atalhos: Correção do erro em que dois <b>KXMLGUIClients</b> com o mesmo nome substituem o arquivo de esquema um do outro
- kxmlguiwindowtest: Adição da caixa de diálogo de atalhos para testar o editor de esquemas de atalhos
- Esquemas de atalhos: Melhoria da usabilidade na alteração de textos na interface do usuário
- Esquemas de atalhos: Melhoria na lista de esquemas (tamanho automático, não limpar em caso de esquemas desconhecidos)
- Esquemas de atalhos: Não incluir o nome do cliente GUI no início do nome do arquivo
- Esquemas de atalhos: Criação de uma pasta ao tentar salvar um novo esquema de atalhos
- Esquemas de atalhos: Retorno da margem do layout porque parecia muito desorganizada
- Correção do vazamento de memória na rotina de inicialização do <b>KXmlGui</b>

### Plasma Framework

- IconItem: Não sobrescrever o código ao usar o <b>QIcon::name()</b>
- ContainmentInterface: Correção do uso do <i>right()</i> e <i>bottom()</i> do <b>QRect</b>
- Remoção efetiva de código duplicado para lidar com <b>QPixmaps</b>
- Adição de documentação da API para o <i>IconItem</i>
- Correção da folha de estilo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359345'>359345</a>)
- Não limpar a máscara da janela em cada alteração de geometria, quando a composição estiver ativa e nenhuma máscara definida
- Applet: Não falhar com a remoção do painel (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345723'>345723</a>)
- Theme: Eliminação do cache de imagens ao mudar de tema (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359924'>359924</a>)
- IconItemTest: Ignorar quando o <i>grabToImage</i> falhar
- IconItem: Correção da mudança de cor dos ícones SVG carregados a partir do tema de ícones
- Correção da resolução do <i>iconPath</i> do SVG no <i>IconItem</i>
- Se o caminho for passado, escolher a parte final (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359902'>359902</a>)
- Adição das propriedades <i>configurationRequired</i> e <i>reason</i>
- Mudança do <i>contextualActionsAboutToShow</i> para o <i>Applet</i>
- ScrollViewStyle: Não usar as margens do item invertido
- DataContainer: Correção das verificações do <i>slot</i> antes de conectar/desconectar
- ToolTip: Evitar alterações múltiplas de geometria ao mudar o conteúdo
- SvgItem: Não usar o <i>Plasma::Theme</i> na tarefa de renderização
- AppletQuickItem: Correção de pesquisa do próprio layout associado (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358849'>358849</a>)
- Expansão menor para a barra de tarefas
- ToolTip: Parar de mostrar o tempo se o <i>hideTooltip</i> for chamado(erro <a href='https://bugs.kde.org/show_bug.cgi?id=358894'>358894</a>)
- Desativação da animação dos ícones nas dicas do Plasma
- Eliminação das animações das dicas
- O tema padrão segue o esquema de cores
- Correção do <i>IconItem</i> que não carregava os ícones fora do tema com o nome (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359388'>359388</a>)
- Preferência para outros contêineres que não sejam da área de trabalho no <i>containmentAt()</i>
- WindowThumbnail: Eliminação da imagem GLX no <i>stopRedirecting()</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=357895'>357895</a>)
- Remoção do filtro de miniaplicativos antigas
- ToolButtonStyle: Não se basear num ID externo
- Não assumir a descoberta de um Corona (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359026'>359026</a>)
- Calendar: Adição de botões adequados para voltar/avançar e um botão "Hoje" (erros <a href='https://bugs.kde.org/show_bug.cgi?id=336124'>336124</a>, <a href='https://bugs.kde.org/show_bug.cgi?id=348362'>348362</a> e <a href='https://bugs.kde.org/show_bug.cgi?id=358536'>358536</a>)

### Sonnet

- Não desativar a detecção do idioma só porque um idioma está definido
- Desabilitar a desativação automática da revisão de ortografia por padrão
- Correção das quebras de texto
- Correção dos caminhos de pesquisa do Hunspell onde o '/' está ausente (erros <a href='https://bugs.kde.org/show_bug.cgi?id=359866'>359866</a>)
- Adição da &lt;pasta do aplicativo&gt;/../share/hunspell ao caminho de pesquisa dos dicionários

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
