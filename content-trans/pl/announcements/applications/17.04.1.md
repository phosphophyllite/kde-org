---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE wydało Aplikacje KDE 17.04.1
layout: application
title: KDE wydało Aplikacje KDE 17.04.1
version: 17.04.1
---
11 maja 2017. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../17.04.0'>Aplikacji KDE 17.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, dolphin, gwenview, kate, kdenlive oraz innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.32.
