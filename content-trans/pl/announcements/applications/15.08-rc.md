---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: KDE wydało Aplikacje 15.08 (kandydat do wydania).
layout: application
release: applications-15.07.90
title: KDE wydało kandydata do wydania dla Aplikacji 14.08
---
6 sierpień 2015. Dzisiaj KDE wydało kandydata do wydania nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Ze względu na obecność wielu programów opartych na Szkieletach KDE 5, wydanie Aplikacji 15.08 wymaga dokładnego przetestowania w celu utrzymania, a nawet poprawienia jakości wrażeń użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie wydania i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
