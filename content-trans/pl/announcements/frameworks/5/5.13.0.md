---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nowe szkielety

- KFileMetadata: biblioteka metadanych i wydobywania
- Baloo: szkielet indeksowania i wyszukiwania plików

### Zmiany wpływające na wszystkie szkielety

- Wymaganie wersji Qt zostało podniesione z 5.2 do 5.3
- Tekst wynikowy diagnozy został skategoryzowany, aby domyślnie wyglądał na odszumiony
- Dokumentacja została przejrzana i uaktualniona

### Integracja Szkieletów

- Naprawiono usterkę w oknie dialogowym plików przeznaczonym tylko dla katalogów
- Nie polegaj na options()-&gt;initialDirectory() dla Qt &lt; 5.4

### Narzędzia KDE Doxygen

- Dodano strony instrukcji dla skryptów kapidox i uaktualniono dane o opiekunie w setup.py

### KBookmarks

- KBookmarkManager: używaj KDirWatch zamiast QFileSystemWatcher do wykrywania miejsca utworzenia user-places.xbel.

### KCompletion

- Poprawki HiDPI dla KLineEdit/KComboBox
- KLineEdit: Nie zezwalaj użytkownikowi na usunięcie tekstu, gdy edytowany wiersz jest tylko-do-odczytu

### KConfig

- Nie zalecaj używania przestarzałego API
- Nie twórz przestarzałego kodu

### KCoreAddons

- Dodano Kdelibs4Migration::kdeHome() dla przypadków nie pokrytych przez zasoby
- Naprawiono ostrzeżenie tr()
- Naprawiono budowanie KCoreAddons na Clang+ARM

### KDBusAddons

- KDBusService: opisano jak wnieść aktywne okno w Activate()

### KDeclarative

- Naprawiono przestarzałe wywołanie KRun::run
- To samo zachowanie MouseArea przy mapowaniu współrzędnych odfiltrowanych zdarzeń podrzędnych
- Wykryto utworzenie wstępnej ikony twarzy
- Nie odświeżaj całego okna, gdy wyświetlamy tylko ploter (błąd 348385)
- właściwie dodawaj właściwość kontekstu userPaths
- Nie dław się przy pustym QIconItem

### Obsługa KDELibs 4

- Przeniesiono kconfig_compiler_kf5 do libexec, zamiast tego użyj kreadconfig5 do testu findExe
- Dokumentacja (suboptymalna) zamienników dla KApplication::disableSessionManagement

### KDocTools

- zmieniono zdanie o zgłaszaniu błędów, ack'ed by dfaure
- dostosowano niemiecki user.entities do en/user.entities
- Uaktualniono general.entities: zmieniono z aplikacji na nazwę produktu w części opisowej dla szkieletów + plazmy 
- Uaktualniono en/user.entities
- Uaktualniono szablony podręczników i stron instrukcji
- Użyto CMAKE_MODULE_PATH w cmake_install.cmake
- BŁĄD: 350799 (błąd 350799)
- Uaktualnienie pliku general.entities
- Szukanie żądanych modułów pearla.
- Przestrzeń nazw makra pomocniczego we wgranym pliku makr.
- Dostosowano tłumaczenia nazw klawiszy do standardowych tłumaczeń dostarczanych przez Termcat

### KEmotikony

- Wgrano wystrój Bryzy
- Kemoticons: ustawiono emotikony Bryzy jako domyślne zamiast poprzednich ikon Szkła
- Zestaw emotikon Bryzy został stworzony przez Uri Herrera

### KHTML

- KHtml stało się użyteczne bez szukania prywatnych zależności

### KIconThemes

- Usuwaj tymczasowe przydziały dla ciągów znaków.
- Usunięto wpis diagnostyczny dotyczący wystroju drzewa

### KIdleTime

- Wgrywane są prywatne nagłówki dla wtyczek platformy.

### KIO

- Niszczenie niepotrzebnych owijaczy QUrl

### KItemModels

- Nowy pośrednik: KExtraColumnsProxyModel, umożliwia dodawanie kolumn do istniejącego modelu.

### KNotification

- Naprawiono początkową współrzędną Y dla zapasowych okien wysuwnych
- Zmniejszono zależności i przeniesiono do Tier 2
- nieznane elementy powiadomień są łapane (nullptr deref) (błąd 348414)
- Usunięto całkiem bezużyteczne ostrzeżenia

### Pakiety Szkieletów

- sprawiono aby napisy po filmem, były napisami ;)
- kpackagetool: Naprawiono wyświetlanie tekstu niełacińskiego na stdout

### KLudzie

- Dodano AllPhoneNumbersProperty
- PersonsSortFilterProxyModel jest teraz dostępny do wykorzystania w QML

### Kross

- krosscore: Wgrano nagłówek CamelCase "KrossConfig"
- Naprawiono test Python2 tak, aby uruchamiał się na PyQt5

### KService

- Naprawiono kbuildsycoca --global
- KToolInvocation::invokeMailer: naprawiono załączniki w przypadku występowania wielu załączników

### KTextEditor

- chroń domyślny poziom dziennika dla Qt &lt; 5.4.0, naprawiono nazwę dziennika cat
- Dodano hl dla Xonotic (błąd 342265)
- Dodano Groovy HL (błąd 329320)
- uaktualniono podświetlanie J (błąd 346386)
- Kompiluj na MSVC2015
- mniejsze wykorzystanie iconloader, naprawiono więcej ikon pixelated
- włącz/wyłącz przycisk znajdź wszystkie po zmianie wzorca
- Ulepszone wyszukiwanie i pasek zastępowania
- usunięto bezużyteczną linijkę z trybu powermode
- szczuplejszy pasek wyszukiwania
- vi: Naprawiono zły odczyt flagi markType01
- Use correct qualification to call base method.
- Usunięto sprawdzenia, QMetaObject::invokeMethod chroni się sam przed tym.
- usunięto problemy HiDPI związane z wybierakami kolorów
- Cleanup coe: QMetaObject::invokeMethod is nullptr safe.
- więcej komentarzy
- change the way the interfaces are null safe
- only output warnings and above per default
- usunięto zadania z przeszłości
- Use QVarLengthArray to save the temporary QVector iteration.
- Move the hack to indent group labels to construction time.
- Usunięto kilka poważnych problemów związanych z KateCompletionModel w trybie drzewa.
- Naprawiono zepsute projekty modelu, które opierały się na zachowaniu z Qt 4.
- obey umask rules when saving new file (bug 343158)
- Dodano meson HL
- Jako, że Varnish 4.x wprowadza różne zmiany w składni względem Varnish 3.x, stworzyłem dodatkowe, osobne pliki do podświetleń dla Varnish 4 (varnish4.xml,  varnishtest4.xml).
- usunięto problemy z HiDPI
- vimode: don't crash if the &lt;c-e&gt; command gets executed in the end of a document. (bug 350299)
- Support QML multi-line strings.
- naprawiono składnie w oors.xml
- Dodano CartoCSS hl autorstwa Lukas Sommer (błąd 340756)
- naprawiono liczby zmiennoprzecinkowe HL, użyto wbudowanych liczb zmiennoprzecinkowych takich jak w C (błąd 348843)
- split directions did got reversed (bug 348845)
- Bug 348317 - [PATCH] Katepart syntax highlighting should recognize u0123 style escapes for JavaScript (bug 348317)
- dodano *.cljs (błąd 349844)
- Uaktualniono plik podświetlania GLSL.
- dostosowano domyślne kolory tak, aby były bardziej rozróżnialne

### KTextWidgets

- Usunięcie starego podświetlania

### Szkielet Portfela

- Naprawiono budowanie na Windowsie
- Print a warning with error code when opening the wallet by PAM fails
- Return the backend error code rather than -1 when opening a wallet failed
- Make the backend's "unknown cipher" a negative return code
- Watch for PAM_KWALLET5_LOGIN for KWallet5
- Naprawiono usterkę przy porażce sprawdzenia MigrationAgent::isEmptyOldWallet()
- KWallet can now be unlocked by PAM using kwallet-pam module

### KWidgetsAddons

- New API taking QIcon parameters to set the icons in the tab bar
- KCharSelect: Naprawiono kategorię unicode oraz użyto boundingRect do obliczania szerokości
- KCharSelect: dostosowano szerokość komórki do zawartości
- KMultiTabBar margins now are ok on HiDPI screens
- KRuler: przedawniono niezaimplementowany KRuler::setFrameStyle(), oczyszczono komentarze
- KEditListWidget: usunięto margines, więc teraz wszystko wyrównuje się lepiej z innymi elementami interfejsu

### KWindowSystem

- Harden NETWM data reading (bug 350173)
- guard for older Qt versions like in kio-http
- Private headers for platform plugins are installed.
- Platform specific parts loaded as plugins.

### KXMLGUI

- Naprawiono zachowanie metody  KShortcutsEditorPrivate::importConfiguration

### Szkielety Plazmy

- Using a pinch gesture one can now switch between the different zoom levels of the calenda
- comment about code duplication in icondialog
- Slider groove color was hardcoded, modified to use color scheme
- Use QBENCHMARK instead of a hard requirement on the machine's performance
- Calendar navigation has been significantly improved, providing a year and decade overview
- PlasmaCore.Dialog now has an 'opacity' property
- Make some space for the radio button
- Don't show the circular background if there's a menu
- Dodano definicję X-Plasma-NotificationAreaCategory
- Set notifications and osd to show on all desktops
- Print useful warning when we can not get valid KPluginInfo
- Naprawiono potencjalne niekończące się nawracanie w PlatformStatus::findLookAndFeelPackage()
- Zmieniono nazwę software-updates.svgz na software.svgz

### Sonnet

- Dodano w CMake, aby umożliwić budowę wtyczki Voikko.
- Zaimplementowano fabrykę Sonnet::Client factory dla sprawdzania pisowni Voikko.
- Zaimplementowano sprawdzanie pisowni oparte na Voikko (Sonnet::SpellerPlugin)

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
