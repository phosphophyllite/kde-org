---
aliases:
- ../announce-applications-14.12-beta3
custom_spread_install: true
date: '2014-11-20'
description: KDE Ships Applications 14.12 Beta 2.
layout: application
title: KDE toob välja rakenduste 14.12 kolmanda beetaväljalaske
---
20. november 2014 KDE laskis täna välja rakenduste uute versioonide beetaväljalaske. Kuna API, sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Et paljud rakendused on juba viidud KDE Frameworks 5 peale, vajavad KDE rakendused 14.12 põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda  meeskonnaga beetat paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.

#### KDE rakenduste 14.12 beeta3 binaarpakettide paigaldamine

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 Beta 3 (internally 14.11.95) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### KDE rakenduste 14.12 beeta3 kompileerimine

The complete source code for KDE Applications 14.12 Beta 3 may be <a href='http://download.kde.org/unstable/applications/14.11.95/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-14.11.95'>KDE Applications Beta 3 Info Page</a>.
