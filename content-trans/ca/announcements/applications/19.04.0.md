---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: Es distribueixen les aplicacions 19.04 del KDE.
layout: application
release: applications-19.04.0
title: KDE distribueix les aplicacions 19.04.0 del KDE
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

La comunitat KDE es complau d'anunciar la publicació de les Aplicacions 19.04 del KDE.

La nostra comunitat treballa contínuament per millorar el programari inclòs a les sèries de les Aplicacions del KDE. Conjuntament amb les noves funcionalitats, millorem el disseny, la usabilitat i l'estabilitat de totes les utilitats, jocs, i eines de creativitat. El nostre objectiu és fer més fàcil la vida fent que el programari KDE sigui agradable d'utilitzar. Esperem que us agradin les millores noves i les esmenes d'errors que trobareu a 19.04!

## Novetats a les Aplicacions 19.04 del KDE

S'han resolt més de 150 errors. Aquestes esmenes tornen a implementar funcionalitats inactivades, normalitzen dreceres i solucionen fallades, fent que les Aplicacions del KDE siguin més fàcils d'emprar i permetent-vos que sigueu més productiu.

### Gestió de fitxers

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

El <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> és el gestor de fitxers del KDE. També connecta a serveis de xarxa, com els servidors SSH, FTP i Samba, i ve amb eines avançades per cercar i organitzar les dades.

Característiques noves:

+ S'ha ampliat el funcionament de les miniatures, de manera que el Dolphin ara pot mostrar miniatures per a diversos tipus nous de fitxers: fitxers de <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a>, fitxers d'<a href='https://phabricator.kde.org/D18738'>eBook .epub i .fb2</a>, fitxers del <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a>, i fitxers <a href='https://phabricator.kde.org/D19679'>PCX</a>. Addicionalment, les miniatures dels fitxers de text ara mostren el <a href='https://phabricator.kde.org/D19432'>ressaltat de sintaxi</a> per al text dins la miniatura. </li>
+ Ara es pot triar <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>quina subfinestra de vista dividida tancar</a> en fer clic al botó «Tanca la divisió». </li>
+ Aquesta versió del Dolphin presenta la <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>col·locació intel·ligent de pestanyes</a>. En obrir una carpeta a una pestanya nova, aquesta es col·locarà immediatament a la dreta de l'actual, en lloc de sempre al final de la barra de pestanyes. </li>
+ Ara l'<a href='https://phabricator.kde.org/D16872'>etiquetatge d'elements</a> és molt més pràctic, ja que les etiquetes es poden afegir o eliminar usant el menú contextual. </li>
+ Els paràmetres <a href='https://phabricator.kde.org/D18697'>d'ordenació predeterminats s'han millorat</a> per a les carpetes d'ús més freqüent. De manera predeterminada, la carpeta de Baixades ara s'ordena per la data amb l'agrupació activada, i la vista de Documents recents (accessible navegant a «recentdocuments:/») s'ordena per data amb una vista de llista seleccionada. </li>

Les esmenes d'errors inclouen:

+ En usar una versió moderna del protocol SMB, podreu <a href='https://phabricator.kde.org/D16299'>descobrir comparticions del Samba</a> per a màquines Mac i Linux. </li>
+ Torna a funcionar adequadament la <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>recol·locació d'elements al plafó Llocs</a> quan hi ha elements ocults. </li>
+ Després d'obrir una pestanya nova al Dolphin, ara la vista d'aquesta pestanya nova guanya automàticament el <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>focus del teclat</a>. </li>
+ Ara el Dolphin us avisa si intenteu sortir mentre el <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>plafó del terminal està obert</a> amb un programa en execució. </li>
+ S'han solucionat moltes fuites de memòria, millorant el rendiment global del Dolphin.</li>

L'<a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> permet que altres aplicacions del KDE llegeixin l'àudio dels CD i el converteixin automàticament en altres formats.

+ Ara l'AudioCD-KIO admet l'extracció a l'<a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ S'ha fet que el <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>text d'informació dels CD</a> sigui realment transparent per a la visualització. </li>

### Edició de vídeo

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Aquesta és una versió de l'editor de vídeo del KDE que farà història. El codi principal del <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> s'ha reescrit i els seus interiors han canviat en més d'un 60%%, millorant globalment la seva arquitectura.

Les millores inclouen:

+ La línia de temps s'ha reescrit fent ús del QML.
+ Quan es posa un clip a la línia de temps, l'àudio i el vídeo sempre estan en pistes separades.
+ Ara la línia de temps admet la navegació amb el teclat: els clips, composicions i fotogrames clau es poden moure amb el teclat. També es pot ajustar l'alçada de les pistes.
+ En aquesta versió del Kdenlive, l'enregistrament d'àudio de la pista d'entrada ve amb una funcionalitat nova de <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>veu en off</a>.
+ S'ha millorat la copia/enganxat: funciona entre les finestres diferents del projecte. També s'ha millorat la gestió dels clips intermediaris, de manera que els clips ara es poden eliminar individualment.
+ La versió 19.04 veu el retorn del funcionament de les pantalles externes de monitor BlackMagic i també hi ha guies noves predefinides en el monitor.
+ S'ha millorat la gestió dels fotogrames clau, aportant un aspecte i un flux de treball més coherent. El titulador també s'ha millorat fent que els botons d'alineació es fixin a zones segures, afegint guies i colors de fons configurables, i mostrant els elements que manquen.
+ S'ha esmenat un error de corrupció de la línia de temps que desubicava o perdia clips i que s'activava en moure un grup de clips.
+ S'ha esmenat un error de les imatges JPG que renderitzava les imatges com a pantalles blanques al Windows. També s'han esmenat els errors que afectaven la captura de pantalla al Windows.
+ A banda de tot el que s'ha indicat, també s'han afegit moltes millores petites d'usabilitat que faran més fàcil i suau l'ús del Kdenlive.

### Oficina

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

L'<a href='https://kde.org/applications/graphics/okular/'>Okular</a> és el visualitzador de documents polivalent del KDE. Ideal per a llegir i anotar PDF, també pot obrir fitxers ODF (com els usats pel LibreOffice i OpenOffice), llibres electrònics publicats com a fitxers «ePub», fitxers més habituals de llibre de còmic, fitxers PostScript, i molts més.

Les millores inclouen:

+ Per ajudar a assegurar que els documents sempre tenen un encaix perfecte, s'han afegit <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>opcions d'escalat</a> al diàleg d'impressió de l'Okular.
+ Ara l'Okular permet la visualització i verificació de les <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>signatures digitals</a> als fitxers PDF.
+ Gràcies a la integració millorada entre aplicacions, ara l'Okular permet editar documents LaTeX al <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ La implementació millorada per la <a href='https://phabricator.kde.org/D18118'>navegació de pantalla tàctil</a> vol dir que sereu capaç de moure's enrere i endavant usant una pantalla tàctil en el mode presentació.
+ Els usuaris que prefereixin manipular els documents des de la línia d'ordres podran portar a terme la <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>cerca de text</a> intel·ligent amb l'indicador nou de la línia d'ordres que permet obrir un document i ressaltar totes les ocurrències d'una peça de text indicada.
+ Ara l'Okular mostra adequadament els enllaços als <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>documents Markdown</a> que s'estenen per més d'una línia.
+ Les <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>eines de retallat</a> tenen noves icones sofisticades.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

El <a href='https://kde.org/applications/internet/kmail/'>KMail</a> és el client de correu electrònic del KDE que protegeix la privadesa. Part del <a href='https://kde.org/applications/office/kontact/'>paquet de treball en grup Kontact</a>, el KMail admet tots els sistemes de correu electrònic i permet organitzar els missatges en bústies virtuals compatides o en comptes separats (a elecció). Admet totes les classes d'encriptatge i signatura de missatges, i permet compartir dades com els contactes, dates de reunions, i informació de viatges amb altres aplicacions del Kontact.

Les millores inclouen:

+ Encara més, la gramàtica! Aquesta versió del KMail aporta la implementació del «languagetool» (verificador gramatical) i el «grammalecte» (verificador gramatical per al francès).
+ Ara es detecten els números de telèfon als correus electrònics i es poden marcar directament via el <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ Ara el KMail té una opció per a <a href='https://phabricator.kde.org/D19189'>iniciar-se directament a la safata del sistema</a> sense obrir la finestra principal.
+ S'ha millorat el funcionament del connector Markdown.
+ La recuperació de correus via IMAP ja no es torna a encallar quan falla l'inici de sessió.
+ També s'han efectuat nombroses esmenes al dorsal Akonadi del KMail per millorar-ne la fiabilitat i el rendiment.

El <a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> és el component de calendari del Kontact, que gestiona tots els vostres esdeveniments.

+ Els esdeveniments recurrents del <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> es tornen a sincronitzar correctament.
+ La finestra de recordatori d'esdeveniments ara recorda mostrar-se a <a href='https://phabricator.kde.org/D16247'>tots els escriptoris</a>.
+ S'ha modernitzat l'aspecte de la <a href='https://phabricator.kde.org/T9420'>vista d'esdeveniments</a>.

El <a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> és l'assistent de viatge nou de trinca del Kontact que ajudarà a obtenir la ubicació i us aconsellarà en el camí.

- Hi ha un extractor genèric nou per als bitllets RCT2 (p. ex. usats per companyies ferroviàries com DSB, ÖBB, SBB, NS).
- S'han millorat molt la detecció i desambiguació del nom de l'aeroport.
- S'han afegit extractors personalitzats nous per als proveïdors anteriorment no admesos (p. ex. BCD Travel, NH Group), i s'ha millorat les variacions de format/idioma dels proveïdors ja admesos (p. ex. SNCF, Easyjet, Booking.com, Hertz).

### Desenvolupament

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

El <a href='https://kde.org/applications/utilities/kate/'>Kate</a> és l'editor de text ple de funcionalitats del KDE, ideal per programar gràcies a les funcionalitats com les pestanyes, mode de vista dividida, ressaltat de sintaxi, un plafó de terminal integrat, compleció de paraules, cerca i substitució amb expressions regulars, i moltes més a través d'una infraestructura flexible de connectors.

Les millores inclouen:

- Ara el Kate pot mostrar tots els caràcters invisibles d'espai en blanc, no només alguns.
- Es pot activar i desactivar fàcilment l'ajust estàtic de paraules usant la seva pròpia entrada de menú en funció del document, sense haver de canviar la configuració predeterminada global.
- Els menús contextuals de fitxer i pestanya ara inclouen una pila d'accions útils noves, com el reanomenat, eliminat, obertura de la carpeta contenidora, còpia del camí del fitxer, comparació [amb un altre fitxer obert], i propietats.
- Aquesta versió del Kate es distribueix amb més connectors activats de manera predeterminada, incloent-hi la popular i útil funcionalitat de terminal en línia.
- En sortir, el Kate ja no demana reconèixer els fitxers que s'han modificat al disc per part d'un altre procés, com un canvi del control de codi font.
- La vista en arbre de connectors ara mostra correctament tots els elements de menú per a les entrades del «git» que tenen dièresi als seus noms.
- En obrir fitxers múltiples usant la línia d'ordres, els fitxers s'obren en pestanyes noves en el mateix ordre com s'ha especificat a la línia d'ordres.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

El <a href='https://kde.org/applications/system/konsole/'>Konsole</a> és l'emulador de terminal del KDE. Permet pestanyes, fons translúcids, mode de vista dividida, esquemes de color personalitzables i dreceres de teclat, punts de directoris i SSH, i moltes altres funcionalitats.

Les millores inclouen:

+ La gestió de pestanyes ha rebut diverses millores que us ajudaran a ser més productiu. Les pestanyes noves es poden crear amb un clic del botó del mig a les parts buides de la barra de pestanyes, i també hi ha una opció que permet tancar les pestanyes fent-hi clic amb el botó del mig. Els botons de tancament es mostren a les pestanyes de manera predeterminada, i les icones només es mostraran en usar un perfil amb una icona personalitzada. Per acabar, però no menys important, la drecera Ctrl+Tab permet commutar ràpidament entre la pestanya actual i l'anterior.
+ El diàleg de perfil d'edició ha rebut un gran <a href='https://phabricator.kde.org/D17244'>redisseny de la interfície d'usuari</a>.
+ L'esquema de color Brisa ara s'usa com a esquema de color predeterminat del Konsole, i s'ha millorat el seu contrast i coherència amb el tema global del Brisa.
+ S'han resolt els problemes en mostrar text en negreta.
+ Ara el Konsole mostra correctament l'estil subratllat del cursor.
+ S'ha millorat la visualització dels <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>caràcters de quadre i línia</a>, així com els <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>caràcters d'Emoji</a>.
+ Les dreceres de commutació de perfil ara canvien entre el perfil de la pestanya actual en lloc d'obrir una pestanya nova amb l'altre perfil.
+ Les pestanyes inactives que reben una notificació i tenen el color del títol canviat, ara tornen a restaurar al color normal de text de títol quan s'activa la pestanya.
+ La característica «Varia el fons per a cada pestanya» ara funciona quan el color base del fons és molt fosc o negre.

El <a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> és un sistema de traduccions assistit per ordinador que està enfocat en la productivitat i el control de la qualitat. Està destinat a la traducció de programari, però també integra eines de conversió externes per traduir documents d'oficina.

Les millores inclouen:

- Ara el Lokalize permet veure el codi font de la traducció amb un editor personalitzat.
- S'ha millorat la ubicació de l'Acoblador de ginys, i la manera en què la configuració es desa i es restaura.
- Ara es conserva la posició en els fitxers «.po» en filtrar missatges.
- S'han solucionat diversos errors de la IU (comentaris del desenvolupador, commutació de RegExp en substitucions massives, comptador de missatges buits no enllestits,…).

### Utilitats

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

El <a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> és un visualitzador i organitzador avançat d'imatges amb eines d'edició intuïtives i d'ús senzill.

Les millores inclouen:

+ La versió del Gwenview que es distribueix amb les Aplicacions 19.04 inclou el <a href='https://phabricator.kde.org/D13901'>funcionament de la pantalla tàctil</a> complet, amb gestos per passar el dit, fer zoom, fer una panoràmica, i més.
+ Una altra millora afegida al Gwenview és el <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>funcionament d'alt ppp</a> complet, que farà que les imatges siguin millors en pantalles de resolució alta.
+ Ha millorat el funcionament dels <a href='https://phabricator.kde.org/D14583'>botons de retrocedir i avançar</a> que permet navegar entre les imatges prement aquests botons.
+ Ara podeu usar el Gwenview per obrir fitxers d'imatge amb el <a href='https://krita.org/'>Krita</a> – l'eina de pintura digital preferida de tothom.
+ Ara el Gwenview accepta <a href='https://phabricator.kde.org/D6083'>miniatures grans de 512 px</a>, permetent previsualitzar les imatges més fàcilment.
+ Ara el Gwenview usa la <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>drecera de teclat Ctrl+L estàndard </a> per moure el focus al camp de l'URL.
+ Ara podeu usar la <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>funcionalitat filtre per nom</a> amb la drecera Ctrl+I, com al Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

L'<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> és l'aplicació de captura de pantalla del Plasma. Podeu capturar els escriptoris complets estesos per diverses pantalles, pantalles individuals, finestres, seccions de finestres, o regions personalitzades usant la funcionalitat de selecció de rectangle.

Les millores inclouen:

+ S'ha ampliat el mode de Regió rectangular amb opcions noves. El quadre arrossegat es pot configurar amb <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>accepta automàticament</a> en lloc de demanar que primer s'ajusti. També hi ha una opció predeterminada nova per recordar el quadre de selecció de la <a href='https://phabricator.kde.org/D19117'>regió rectangular</a> actual, però només fins que es tanqui el programa.
+ Podeu configurar què passa quan es prem la drecera de captura de pantalla <a href='https://phabricator.kde.org/T9855'>mentre ja s'està executant l'Spectacle</a>.
+ L'Spectacle permet canviar el <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>nivell de compressió</a> per als formats d'imatge amb pèrdua.
+ El desament dels paràmetres mostra com quedarà el <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>nom de fitxer d'una captura de pantalla</a>. També podeu ajustar fàcilment la <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>plantilla del nom de fitxer</a> a les vostres preferències senzillament fent clic a les variables de substitució.
+ L'Spectacle ja no mostra més les opcions «Pantalla completa (Tots els monitors)» i «Pantalla actual» quan l'ordinador només té una pantalla.
+ El text d'ajuda en el mode de Regió rectangular ara es mostra al mig de la pantalla primària, en lloc de dividir-se entre les pantalles.
+ Quan s'executa al Wayland, l'Spectacle només inclou les característiques que funcionen.

### Jocs i programari educatiu

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Les sèries d'aplicacions inclouen nombrosos <a href='https://games.kde.org/'>jocs</a> i <a href='https://edu.kde.org/'>aplicacions educatives</a>.

El <a href='https://kde.org/applications/education/kmplot'>KmPlot</a> és un traçador de funcions matemàtiques. Disposa d'un analitzador integrat potent. Els gràfics es poden acolorir i la vista és escalable, permetent fer zoom al nivell que calgui. Es poden representar diferents funcions simultàniament i combinar-les per a crear-ne de noves.

+ Es pot fer zoom mantenint premuda la tecla Ctrl i usant la <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>roda del ratolí</a>.
+ Aquesta versió del Kmplot presenta l'opció de <a href='https://phabricator.kde.org/D17626'>vista prèvia de la impressió</a>.
+ El valor arrel o la parella (x,y) ara es pot copiar al <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>porta-retalls</a>.

El <a href='https://kde.org/applications/games/kolf/'>Kolf</a> és un joc de golf en miniatura.

+ S'ha restaurat el <a href='https://phabricator.kde.org/D16978'>funcionament del so</a>.
+ El Kolf s'ha adaptat amb èxit des de les «kdelibs4».
