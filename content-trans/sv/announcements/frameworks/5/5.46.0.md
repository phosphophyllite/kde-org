---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Undvik oändliga snurror när webbadressen hämtas från DocumentUrlDB (fel 393181)
- Lägg till Baloo D-Bus-signaler för flyttade eller borttagna filer
- Installera pro-fil för qmake-stöd och dokumentera den i matainfo.yaml
- baloodb: lägg till rensningskommando
- balooshow: Färglägg bara om ansluten till terminal
- Ta bort FSUtils::getDirectoryFileSystem
- Undvik hårdkodning av filsystem som stöder CoW
- Tillåt inaktivering av CoW att misslyckas när det inte stöds av filsystemet
- databasesanitizer: Använd flaggor för filtrering
- Rätta sammanfogning av termer i AdvancedQueryParser
- Använd QStorageInfo istället för en hemmagjord implementering
- sanitizer: Förbättra enhetslistning
- Verkställ omedelbart termInConstruction när termen är fullständig
- Hantera intilliggande specialtecken riktigt (fel 392620)
- Lägg till testfall för tolkning av dubbla inledande '((' (fel 392620)
- Använd statbuf konsekvent

### Breeze-ikoner

- Lägg till plasma-browser-integration ikon i systembrickan
- Lägg till Virt-manager ikon tack vare ndavis
- Lägg till video-card-inactive
- overflow-menu som view-more-symbolic, och horisontell
- Använd den lämpligare ikonen "två skjutreglage" för "anpassa"

### Extra CMake-moduler

- Inkludera FeatureSummary innan anrop till set_package_properties
- Installera inte insticksprogram inne i bibliotek för Android
- Gör det möjligt att bygga flera apk från ett projekt
- Kontrollera om androiddeployqt-programmets paket har symbolen main()

### KCompletion

- [KLineEdit] Använd Qt:s inbyggda funktion för rensningsknapp
- Rätta KCompletionBox på Wayland

### KCoreAddons

- [KUser] Kontrollera om .face.icon faktiskt är läsbar innan den returneras
- Gör signaler från KJob öppna så att anslutningssyntaxen i Qt5 kan fungera

### KDeclarative

- Läs in NV-grafikåterställning baserad på inställning
- [KUserProxy] Justera till kontotjänst (fel 384107)
- Plasma mobiloptimeringar
- Gör plats för sidfot och sidhuvud
- Ny storleksändringsprincip (fel 391910)
- Stöd åtgärdssynlighet
- Stöd Nvidia återställningsunderrättelser i QtQuickViews

### KDED

- Lägg till plattformsdetektering och justeringar i kded (automatisk inställning av $QT_QPA_PLATFORM)

### KFileMetaData

- Lägg till beskrivning och syfte för Xattr-beroende
- extractors: Dölj varningar från systemdeklarationsfiler
- Rätta detektering av taglib vid kompilering för Android
- Installera pro-fil för qmake-stöd och dokumentera den i matainfo.yaml
- Gör konkatenerade strängar radbrytningsbara
- ffmpegextractor: Tysta varningar som avråder från användning
- taglibextractor: Rätta fel för tom genre
- Hantera fler taggar i taglibextractor

### KHolidays

- holidays/plan2/holiday_sk_sk - Rättning av lärarnas dag (fel 393245)

### KI18n

- [Dokumentation av programmeringsgränssnitt] Ny markör för användargränssnitt @info:placeholder
- [Dokumentation av programmeringsgränssnitt] Ny markör för användargränssnitt @item:valuesuffix
- Behöver inte köra tidigare iterationers kommandon igen (fel 393141)

### KImageFormats

- [XCF/GIMP loader] Öka maximal tillåten bildstorlek till 32767x32767 på 64-bitars plattformar (fel 391970)

### KIO

- Jämn skalning av miniatyrbilder i filväljare (fel 345578)
- KFileWidget: Justera grafisk filnamnskomponent perfekt med ikonvy
- KFileWidget: Spara bredd för panelen Platser också efter panelen dolts
- KFileWidget: Förhindra att bredden på panelen Platser iterativt växer en bildpunkt
- KFileWidget: Inaktivera zoomknappar när minimum eller maximum nåtts
- KFileWidget: Ställ in minimal storlek för zoomreglage
- Markera inte filändelse
- concatPaths: Behandla tom path1 riktigt
- Förbättra rutlayout för ikoner i dialogrutan filväljare (fel 334099)
- Dölj KUrlNavigatorProtocolCombo om bara ett protokoll stöds
- Visa bara scheman som stöds i KUrlNavigatorProtocolCombo
- Filväljaren läser miniatyrbilder för förhandsgranskning från Dolphins inställningar (fel 318493)
- Lägg till Skrivbord och Nerladdningar till standardlistan av Platser
- KRecentDocument lagrar nu QGuiApplication::desktopFileName istället för applicationName
- [KUrlNavigatorButton] Gör inte heller stat för MTP
- getxattr har 6 parametrar i macOS (fel 393304)
- Lägg till menyalternativet "Uppdatera" i den sammanhangsberoende menyn för KDirOperator (fel 199994)
- Spara inställningar för dialogrutans visning även vid avbryt (fel 209559)
- [KFileWidget] Hårdkoda exempel-användarnamn
- Visa inte applikationen "Öppna med" för kataloger, bara för filer
- Detektera felaktig parameter i findProtocol
- Använd texten "Annat program..." i undermenyn "Öppna med"
- Koda miniatyrbilders webbadress riktigt (fel 393015)
- Finjustera kolumnbredder i trädvy för fildialogrutorna öppna och spara (fel 96638)

### Kirigami

- Varna inte när Page {} används utanför en pageStack
- Omarbeta InlineMessages för att korrigera ett antal problem
- Lås till Qt 5.11
- Basera på enheter för storlek på verktygsknapp
- Färglägg stängningsikon när musen hålls över den
- Visa en marginal under sidfoten vid behov
- Rätta isMobile
- Tona också vid animering av öppna och stäng
- Inkludera bara D-Bus grejer på Unix - inte Android, inte Apple
- Bevaka tabletMode från KWin
- Visa åtgärder när musen hålls över dem i skrivbordsläge (fel 364383)
- Hantera i övre verktygsrad
- Använd en grå stängningsknapp
- Mindre beroende på programfönster
- Färre varningar utan programfönster
- Fungera riktigt utan programfönster
- Använd inte en storlek som inte är ett heltal för avdelare
- Visa inte åtgärderna om de är inaktiverade
- Markeringsbara objekt i FormLayout
- Använd olika ikoner i färguppsättningsexempel
- Inkludera bara ikoner på Android
- Få det att fungera med Qt 5.7

### KNewStuff

- Rätta dubbla marginaler omkring DownloadDialog
- Rätta tips i användargränssnittsfiler om delklasser för egna grafiska komponenter
- Erbjud inte QML-insticksprogram som länkmål

### Ramverket KPackage

- Använd KDE_INSTALL_DATADIR istället för FULL_DATADIR
- Lägg till bidragswebbadress i testdata

### KPeople

- Rätta filtrering av PersonSortFilterProxyModel

### Kross

- Gör också installation av översatt dokumentation valfri

### Kör program

- Stöd för jokertecken i D-Bus körprogram

### KTextEditor

- Optimering av KTextEditor::DocumentPrivate::views()
- [ktexteditor] Mycket snabbare positionFromCursor
- Implementera enkelklick på radnummer för att markera textrad
- Rätta saknad markering av fetstil/kursiv/... för moderna Qt-versioner (&gt;= 5.9)

### Plasma ramverk

- Rätta händelsemarkering som inte visas i kalender med teman air och oxygen
- Använd "Anpassa %1..." som text för inställningsåtgärd av miniprogram
- [Button Styles] Fyll i höjd och vertikal justering (fel 393388)
- Lägg till ikonen video-card-inactive för systembrickan
- Korrigera utseende för platta knappar
- [Containment Interface] Gå inte till redigeringsläge om oföränderlig
- Säkerställ att largespacing är en exakt multiple av small
- Anropa addContainment med riktiga parametrar
- Visa inte bakgrunden för Button.flat
- Säkerställ att omgivningen vi skapade har aktiviteten vi begärde
- Lägg till en version av containmentForScreen med aktivitet
- Ändra inte minneshantering för att dölja ett objekt (fel 391642)

### Syfte

- Säkerställ att vi ger inställningsinsticksprogram ett visst vertikalt utrymme
- Konvertera insticksprograminställning för KDEConnect till QQC2
- Konvertera AlternativesView till QQC2

### QQC2StyleBridge

- Exportera layoutvaddering från qstyle, starta från Control
- [ComboBox] Rätta hantering av mushjul
- Gör så att musområdet inte påverkar kontroller
- Rätta acceptableInput

### Solid

- Uppdatera monteringsplats efter monteringsåtgärder (fel 370975)
- Invalidera egenskapscache när ett gränssnitt tas bort
- Undvik att skapa duplicerade egenskapsposter i cachen
- [UDisks] Optimera flera egenskapskontroller
- [UDisks] Korrekt hantering av flyttbara filsystem (fel 389479)

### Sonnet

- Rätta aktivera/inaktivera borttagningsknapp
- Rätta aktivera/inaktivera tilläggsknapp
- Leta i underkataloger efter ordlistor

### Syntaxfärgläggning

- Uppdatera projektwebbadress
- 'Rubrik' är en kommentar, så basera den på dsComment
- Lägg till färgläggning för GDB-kommandolistningar och gdbinit-filer
- Lägg till syntaxfärgläggning för Logcat

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
