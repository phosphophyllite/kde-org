---
aliases:
- ../../kde-frameworks-5.62.0
date: 2019-09-14
layout: framework
libCount: 70
---
### Attica

- Rätta attica pkgconfig-filen

### Baloo

- Rätta en krasch i Peruse utlöst av baloo

### Breeze-ikoner

- Lägg till nya aktivitets- och virtuella skrivbordsikoner
- Gör så att små senast dokumentikoner ser ut som dokument och förbättra klockemblem
- Skapa ny ikon "Recent folders" (fel 411635)
- Lägg till ikonen "preferences-desktop-navigation" (fel 402910)
- Lägg till 22 bildpunkters dialogskript, ändra åtgärds- och platsikoner för skript så att de motsvarar det
- Förbättra ikonen "user-trash"
- Använd tom/fylld stil för svartvit tom/full papperskorg
- Gör så att underrättelseikoner använder konturstil
- Gör så att user-trash ikoner ser ut som papperskorgar (fel 399613)
- Lägg till breeze ikoner för ROOT cern-filer
- Ta bort applets/22/computer (fel 410854)
- Lägg till view-barcode-qr ikoner
- Krita har avdelats från Calligra och använder nu namnet Krita istället för calligrakrita (fel 411163)
- Lägg till battery-ups ikoner (fel 411051)
- Gör "monitor" en länk till ikonen "computer"
- Lägg till FictionBook 2 ikoner
- Lägg till ikon för kuiviewer, behöver uppdatering -&gt; fel 407527
- Symbolisk länk för "port" till "anchor", vilket visar en lämpligare ikonografi
- Ändra radio till enhetsikon, lägg till fler storlekar
- Gör så att ikonen <code>pin</code> pekar på en ikon som ser ut som ett stift, inte något orelaterat.
- Rätta saknad siffra och bildpunktsperfekt justering av djupåtgärdsikoner (fel 406502)
- Gör så att 16 bildpunkters folder-activities se mer ut som större storlekar
- lägg till latte-dock ikon från latte dock arkivet för kde.org/applications
- ikon för kdesrc-build använd av kde.org/applications som ska ritas om
- Byt namn på media-show-active-track-amarok till media-track-show-active

### Extra CMake-moduler

- ECMAddQtDesignerPlugin: skicka kodexempel indirekt via argument med variabelnamn
- Behåll 'lib' som förvalt LIBDIR på Arch Linux-baserade system
- Aktivera autorcc som förval
- Definiera installationsplats  för JAR/AAR-filer för Android
- Lägg till ECMAddQtDesignerPlugin

### KActivitiesStats

- Lägg till Term::Type::files() och Term::Type::directories() för att bara filtrera kataloger eller undanta dem
- Lägg till @since 5.62 för nytillagda set-funktioner
- Lägg till riktig loggning genom att använda ECMQtDeclareLoggingCategory
- Lägg till set-funktion för frågefälten Type, Activity, Agent och UrlFilter
- Använd särskilda värdekonstanter i terms.cpp
- Tillåt att dataintervall filtrerar resurshändelser genom att använda Date Term

### KActivities

- [kactivities] Använd aktivitetsikon

### KArchive

- Rätta att skapa arkiv för Android-innehåll: webbadresser

### KCompletion

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### KConfig

- Rätta minnesläcka i KConfigWatcher
- Inaktivera KCONFIG_USE_DBUS på Android

### KConfigWidgets

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)
- [KColorSchemeManager] Optimera generering av förhandsgranskning

### KCoreAddons

- KProcessInfo::name() returnerar nu bara namnet på det körbara programmet. För fullständig kommandorad använd KProcessInfo::command()

### KCrash

- Undvik att aktivera kcrash om det bara inkluderas via ett insticksprogram (fel 401637)
- Inaktivera kcrash vid körning under rr

### KDBusAddons

- Rätta kapplöpning vid automatiska omstarter av kcrash

### KDeclarative

- Varna om KPackage är ogiltig
- [GridDelegate] Markera inte avmarkerat objekt när någon av dess åtgärdsknappar klickas (fel 404536)
- [ColorButton] Skicka vidare accepterad signal från ColorDialog
- använd nollbaserat koordinatsystem på diagrammet

### KDesignerPlugin

- Avråd från kgendesignerplugin, ta bort packe med insticksprogram för alla KF5-komponenter

### KDE WebKit

- Använd ECMAddQtDesignerPlugin istället för privat kopia

### KDocTools

- KF5DocToolsMacros.cmake: Använd KDEInstallDirs variabler som inte avråds från (fel 410998)

### KFileMetaData

- Implementera skrivning av bilder

### KHolidays

- Visa filnamn när vi returnerar ett fel

### KI18n

- Landsanpassa långa nummersträngar (fel 409077)
- Stöd att skicka mål till makrot ki18n_wrap_ui

### KIconThemes

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### KIO

- Ångra filer flyttade till papperskorg på skrivbordet har rättats (fel 391606)
- kio_trash: dela upp copyOrMove, för ett bättre fel än "ska aldrig inträffa"
- FileUndoManager: klarare assert när inspelning glömts bort
- Rätta avslutning och krasch i kio_file när put() misslyckas i readData
- [CopyJob] Rätta krasch vid kopiering av en katalog som redan finns när "Hoppa över" klickas (fel 408350)
- [KUrlNavigator] Lägg till Mime-typer som stöds av krarc till isCompressedPath (fel 386448)
- Lägg till dialogruta för att ange körrättigheter för körbar fil vid försök att köra den.
- [KPropertiesDialog] Kontrollera alltid om monteringspunkten är null (fel 411517)
- [KRun] Kontrollera Mime-typ för isExecutableFile först
- Lägg till en ikon för papperskorgens rot och en riktig beteckning (fel 392882)
- Lägg till stöd för att hantera QNAM SSL-fel i KSslErrorUiData
- Gör så att FileJob beter sig konsekvent
- [KFilePlacesView] Använd asynkron KIO::FileSystemFreeSpaceJob
- byt namn på intern 'kioslave' körbart hjälpprogram till 'kioslave5' (fel 386859)
- [KDirOperator] Avkorta beteckningar som är för långa för att få plats i mitten (fel 404955)
- [KDirOperator] Lägg till följande nya katalogalternativ
- KDirOperator: Aktivera bara menyn "Create New" om det markerade objektet är en katalog
- KIO FTP: Rätta att filkopiering hänger vid kopiering till en befintlig fil (fel 409954)
- KIO: konvertera till KWindowSystem::setMainWindow som inte avråds från
- Gör bokmärkesnamn på filer konsekventa
- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)
- [KDirOperator] Använd mer mänskligt läsbara beskrivningar av sorteringsordning
- [Rättighetseditor] Konvertera ikoner till QIcon::fromTheme() (fel 407662)

### Kirigami

- Ersätt egen överflödesknapp med PrivateActionToolButton i ActionToolBar
- Om ett undermenyalternativ har en ikonuppsättning, säkerställ att den också visas
- [Separator] Matcha Breeze kantfärger
- Lägg till komponenten Kirigami ListSectionHeader
- Rätta sammanhangsberoende menyknappar för sidor som inte visas
- Rätta PrivateActionToolButton med meny som inte rensar markerat tillstånd riktigt
- tillåt att ange egen ikon för vänster lådhandtag
- Omarbeta visibleActions logik i SwipeListItem
- Tillåt användning av QQC2 åtgärder i Kirigami komponenter och låt nu K.Action vara baserad på QQC2.Action
- Kirigami.Icon: Rätta inläsning av större bilder när källan är en webbadress (fel 400312)
- Lägg till ikon som används av Kirigami.AboutPage

### KItemModels

- Lägg till Q_PROPERTIES-gränssnitt i KDescendantsProxyModel
- Konvertera från metoder som avråds från Qt

### KItemViews

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### KNotification

- Undvik att duplicerade underrättelser visas på Windows och ta bort blanktecken
- Använd 1024x1024 programikon som reservikon i Snore
- Lägg till parametern <code>-pid</code> till Snore gränssnittsanrop
- Lägg till snoretoast gränssnitt för KNotifications på Windows

### KPeople

- Gör det möjligt att ta bort kontakter från gränssnitt
- Gör det möjligt att ändra kontakter

### KPlotting

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### Kör program

- Säkerställ att vi kontrollerar om det är dags för rivning efter ett jobb avslutas
- Lägg till signalen done i FindMatchesJob istället för att använda QObjectDecorator felaktigt

### KTextEditor

- Tillåt att anpassa egenskaper för KSyntaxHighligting-teman
- Rättning för alla teman: tillåt att stänga av egenskaper i XML-färgläggningsfiler
- förenkla isAcceptableInput + tillåt alla grejer för inmatningsmetoder
- förenkla typeChars, inget behov av returkod utan filtrering
- Härma QInputControl::isAcceptableInput() vid filtrering av inskrivna tecken (fel 389796)
- försök att sanera radslut vid inklistring (fel 410951)
- Rättning: tillåt att stänga av egenskaper i XML-färgläggningsfiler
- förbättra ordkomplettering att använda färgläggning för att detektera ordgränser (fel 360340)
- Mer konvertering från QRegExp till QRegularExpression
- kontrollera om kommandot diff kan startas för jämförelse av växlingsfil på rätt sätt (fel 389639)
- KTextEditor: Rätta flimmer för vänsterkant vid byte mellan dokument
- Konvertera några fler QRegExps till QRegularExpression
- Tillåt 0 i linjeintervall i vim-läge
- Använd CMake find_dependency istället för find_package i CMake inställningsfilsmallen

### KTextWidgets

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### KUnitConversion

- Lägg effektenheter med decibel (dBW och multipler)

### Ramverket KWallet

- KWallet: rätta start av kwalletmanager, skrivbordsfilen har siffran '5' i sig

### Kwayland

- [server] Omge proxyRemoveSurface med smart pekare
- [server] Använd aktuellt läge lagrat i cache oftare och kontrollera giltighet
- [server] Lagra aktuellt läge i cache
- Implementera zwp_linux_dmabuf_v1

### KWidgetsAddons

- [KMessageWidget] Skicka grafisk komponent till standardIcon()
- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### KWindowSystem

- KWindowSystem: lägg till cmake alternativet KWINDOWSYSTEM_NO_WIDGETS
- Avråd från slideWindow(QWidget *widget)
- Lägg till överlagring av KWindowSystem::setMainWindow(QWindow *)
- KWindowSystem: lägg till överlagring av setNewStartupId(QWindow *...)

### KXMLGUI

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### NetworkManagerQt

- Byt namn på WirelessDevice::lastRequestScanTime till WirelessDevice::lastRequestScan
- Lägg till egenskaperna lastScanTime och lastRequestTime till WirelessDevice

### Plasma ramverk

- Gör så att underrättelseikoner använder konturstil
- gör storleken på verktygsknapparna mer koherent
- Tillåt miniprogram/omgivningar/skrivbordsunderlägg för att skjuta upp UIReadyConstraint
- Gör så att underrättelseikoner ser ut som klockor (fel 384015)
- Rätta felaktig initial flikposition för vertikala flikrader (fel 395390)

### Syfte

- Rätta Telegram skrivbordsinsticksprogram på Fedora

### QQC2StyleBridge

- Förhindra dragning av QQC2 kombinationsruta utanför menyn

### Solid

- Gör så att seriell egenskap för batteri konstant
- Exponera teknologiegenskap i batterigränssnitt

### Sonnet

- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### Syntaxfärgläggning

- C och ISO C++: lägg till digraphs (folding och preprocessor) (fel 411508)
- Markdown, TypeScript och Logcat: några rättningar
- Formatklass: lägg till funktioner för att veta om XML-filer ställer in stilegenskaper
- kombinera test.m grejer i befintliga highlight.m
- Stöd för inbyggd Matlab-strängar
- Gettext: Lägg till "Translated String" stil och spellChecking egenskap (fel 392612)
- Ställ in OpenSCAD indentering till C-stil istället för ingen
- Möjlighet att ändra definitionsdata efter inläsning
- Färgläggningsindexerare: kontrollera kateversion
- Markdown: flera förbättringar och rättningar (fel 390309)
- JSP: stöd för &lt;script&gt; och &lt;style&gt; ; använd IncludeRule ##Java (fel 345003)
- LESS: importera CSS-nyckelord, ny färgläggning och några förbättringar
- Javascript: ta bort onödigt "Conditional Expression" sammanhang
- Ny syntax: SASS. Några rättningar för CSS och SCSS (fel 149313)
- Använd CMake find_dependency i CMake inställningsfil istället för find_package
- SCSS: rätta interpolation (#{...}) och lägg till färg för interpolation
- rätta egenskapen additionalDeliminator (fel 399348)
- C++: kontrakt ingår inte i C++20
- Gettext: rätta "tidigare oöversatt sträng" och andra förbättringar/rättningar
- Jam: Rätta local med variabel utan initiering och färglägg SubRule
- implicit reserv om fallthroughContext finns
- Lägg till vanliga GLSL-filändelser (.vs, .gs, .fs)
- Latex: flera rättningar (matematikläge, nästlade verbatim, ...) (fel 410477)
- Lua: rätta färg för slut med flera nivåer av villkor och funktionsnästling
- Färgläggningsindexerare: alla varningar är allvarliga fel

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
