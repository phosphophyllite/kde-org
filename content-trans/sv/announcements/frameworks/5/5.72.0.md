---
aliases:
- ../../kde-frameworks-5.72.0
date: 2020-07-04
layout: framework
libCount: 70
---
### KDAV : Ny modul

DAV-protokollimplementering med KJobs.

Kalendrar och uppgifter stöds, med användning av antingen GroupDAV eller CalDAV, och kontakter stöds med användning av GroupDAV eller CardDAV.

### Baloo

+ [Indexers] Ignorera namnbaserad Mime-typ för initiala indexeringsbeslut (fel 422085)

### BluezQt

+ Exponera tjänsttillkännagörandedata för en enhet

### Breeze-ikoner

+ Rättade tre 16 bildpunkters ikoner för application-x-... så att de är mer bildpunktsperfekta (fel 423727)
+ Lägg till stöd för &gt; 200 % skala för ikoner som ska förbli svartvita
+ Klipp ut mitten av window-close-symbolic
+ uppdatering av ikoner för cervisia program och åtgärd
+ Lägg till README avsnitt om webbteckensnittet
+ Använd grunt-webfonts istället för grunt-webfont och inaktivera generering för symboliskt länkade ikoner
+ lägg till kup-ikon från kup-arkiv

### Extra CMake-moduler

+ Ta bort stöd för png2ico
+ Hantera att Qt:s CMake-kod ändrar CMAKE_SHARED_LIBRARY_SUFFIX
+ Lägg till FindTaglib sökmodul

### KDE Doxygen-verktyg

+ Stöd "repo_id" i metainfo.yaml för att överskrida gissat arkivnamn

### KCalendarCore

+ Kontrollera skrivfel i save() om disken är full (fel 370708)
+ Korrigera ikonnamn för upprepade uppgifter
+ Rätta serialisering av upprepade uppgifters startdatum (fel 345565)

### KCMUtils

+ Rätta ändrad signal för insticksprogramväljare

### KCodecs

+ Lägg till något n, annars kan texten vara mycket stor i meddelanderutor

### KConfig

+ Skicka också locationType till KConfigSkeleton vid konstruktion från grupp
+ Gör texten för "Byt programspråk..." mer konsekvent

### KConfigWidgets

+ Gör texten för "Byt programspråk..." mer konsekvent

### KCoreAddons

+ KRandom::random -&gt; QRandomGenerator::global()
+ Avråd från KRandom::random

### KCrash

+ Lägg till saknad deklaration av environ, annars bara tillgänglig på GNU

### KDocTools

+ Använd konsekvent stil för FDL-anmärkning (fel 423211)

### KFileMetaData

+ Anpassa kfilemetadata till "audio/x-speex+ogg" som nyligen ändrats i shared-mime-info

### KI18n

+ Lägg också till citationstecken omkring rich text taggad med &lt;filename&gt;

### KIconThemes

+ Använd inte tilldelning i resetPalette
+ Returnera QPalette() om vi inte har någon egen palett
+ Respectera QIcon::fallbackSearchpaths() (fel 405522)

### KIO

+ [kcookiejar] Rätta läsning av kakinställningen "Acceptera för session" (fel 386325)
+ KDirModel: rätta hasChildren() regression för träd med dolda filer, symboliska länkar, rör och teckenenheter (fel 419434)
+ OpenUrlJob: rätta stöd för skalskript med ett mellanslag i filnamnet (fel 423645)
+ Lägg till webbgenvägar för DeepL och ArchWiki
+ [KFilePlacesModel] Visa AFC-enheter (Apple File Conduit)
+ Koda också mellanslag för webbgenvägarnas webbadresser (fel 423255)
+ [KDirOperator] Aktivera faktiskt dirHighlighting normalt
+ [KDirOperator] Markera föregående katalog vid gå bakåt/uppåt (fel 422748)
+ [Trash] Hantera ENOENT fel vid namnbyte av filer (fel 419069)
+ Fil I/O-slav: ställ in nanosekunders tidsstämpel vid kopiering (fel 411537)
+ Justera webbadresser för Qwant sökleverantörer (fel 423156)
+ Fil I/O-slave: Lägg till stöd för kopiering av referenslänk (fel 326880)
+ Rätta inställning av förvald genväg i inställningsmodulen för webbgenvägar (fel 423154)
+ Säkerställ läsbarhet av webbgenvägarnas inställningsmodul genom att ställa in minimal bredd (fel 423153)
+ FileSystemFreeSpaceJob: skicka fel om I/O-slaven inte tillhandahåller metadata
+ [Trash] Ta bort trashinfo-filer som refererar till filer/kataloger som inte finns (fel 422189)
+ kio_http: Gissa mime-type själva om servern returnerar application/octet-stream
+ Avråd från signalerna totalFiles och totalDirs, inte skickade
+ Rätta återinläsning av nya webbgenvägar (fel 391243)
+ kio_http: Rätta status för namnbyte med överskrivning aktiverad
+ Tolka inte namn som dold fil eller filsökväg (fel 407944)
+ Visa inte borttagna/dolda webbgenvägar (fel 412774)
+ Rätta krasch när post tas bort (fel 412774)
+ Tillåt inte att befintliga genvägar tilldelas
+ [kdiroperator] Använd bättre verktygstips för åtgärderna bakåt och framåt (fel 422753)
+ [BatchRenameJob] Använd KJob::Items när förloppsinformation rapporteras (fel 422098)

### Kirigami

+ [aboutpage] Använd OverlaySheet för licenstext
+ rätta ikoner för hopdraget läge
+ Använd lätta avskiljare för DefaultListItemBackground
+ Lägg till egenskapen weight i Separator
+ [overlaysheet] Undvik bråkdelshöjd för contentLayout (fel 422965)
+ Rätta pageStack.layers dokumentation
+ Återintroducera lagerstöd i Page.isCurrentPage
+ stöd ikonfärg
+ använd den interna komponenten MnemonicLabel
+ Reducera global verktygsrads vaddering till vänster när titel inte används
+ Ställ bara in implicit{Width,Height} för Separator
+ Uppdatera KF5Kirigami2Macros.cmake att använda https med git-arkiv för att undvika fel vid åtkomstförsök
+ Rättning: inläsning av avatar
+ Gör PageRouterAttached#router skrivbar
+ Rätta att OverlaySheet stängs vid klick inne i layout (fel 421848)
+ Lägg till saknad identifierare till GlobalDrawerActionItem i GlobalDrawer
+ Rätta att OverlaySheet är för hög
+ Rätta PlaceholderMessage kodexempel
+ återge kant längst ner i grupper
+ använd en LSH-komponent
+ Lägg till  bakgrund i huvuden
+ Bättre hantering av hopdragning
+ Bättre presentation för objekt i listhuvud
+ Lägg till egenskapen bold i BasicListItem
+ Rätta Kirigami.Units.devicePixelRatio=1.3 när den ska vara 1.0 vid 96 punkter/tum
+ Dölj OverlayDrawer grepp när den inte är interaktiv
+ Justera beräkningar av ActionToolbarLayoutDetails för att använda skärmegendom bättre
+ ContextDrawerActionItem: Föredra textegenskap över verktygstips

### KJobWidgets

+ Integrera KJob::Unit::Items (fel 422098)

### KJS

+ Rätta krasch vid användning av KJSContext::interpreter

### KNewStuff

+ Flytta förklarande text från bladhuvud till listhuvud
+ Rätta sökvägar för installationsskript och uppackning
+ Dölj ShadowRectangle för ej inlästa förhandsgranskningar (fel 422048)
+ Tillåt inte innehåll att flöda över i rutnätsdelegater (fel 422476)

### KNotification

+ Använd inte notifybysnore.h för MSYS2

### KParts

+ Avråd från PartSelectEvent och co

### KQuickCharts

+ Dra ihop värdet Label i LegendDelegate när det inte finns tillräcklig bredd
+ Rätta felet "C1059: icke-konstant uttryck ..."
+ Ta hänsyn till linjebredd vid gränskontroller
+ Använd inte fwidth vid återgivning av linjer i linjediagram
+ Skriv om removeValueSource så att den inte använder förstörda QObjects
+ Använd insertValueSource i Chart::appendSource
+ Initiera ModelHistorySource::{m_row,m_maximumHistory} riktigt

### Kör program

+ Rätta RunnerContextTest så att inte närvaro av .bashrc antas
+ Använd inbäddad JSON-metadata för binära insticksprogram och egna för D-Bus insticksprogram
+ Skicka queryFinished när alla jobb för aktuell fråga är klara (fel 422579)

### KTextEditor

+ Gör så att "goto line" fungerar bakåt (fel 411165)
+ rätta krasch när en vy tas bort om intervall fortfarande är aktiva (fel 422546)

### Ramverket KWallet

+ Introducera tre nya metoder som returnerar alla "poster" i en katalog

### KWidgetsAddons

+ Rätta KTimeComboBox för landsinställningar med ovanliga tecken i format (fel 420468)
+ KPageView: ta bort osynlig bildpunktsavbildning på höger sida om rubriken
+ KTitleWidget: flytta från QPixmap egenskap till QIcon egenskap
+ Avråd från användning av KMultiTabBarButton/KMultiTabBarTab programmeringsgränssnitt vid användning av QPixmap
+ KMultiTabBarTab: gör så att styleoption tillståndslogik följer QToolButton ännu mer
+ KMultiTabBar: visa inte markerade knappar i QStyle::State_Sunken
+ [KCharSelect] Ge initialt fokus till radeditorn för sökning

### KWindowSystem

+ [xcb] Skicka korrekt skalad ikongeometri (fel 407458)

### KXMLGUI

+ Använd kcm_users istället för user_manager
+ Använd nytt KTitleWidget::icon/iconSize programmeringsgränssnitt
+ Flytta "Byt programspråk" till menyn Inställningar (fel 177856)

### Plasma ramverk

+ Visa tydligare varning om begärd inställningsmodul inte kan hittas
+ [spinbox] Använd inte QQC2-objekt när vi ska använda PlasmaComponents (fel 423445)
+ Plasma components: återställ förlorad färgkontroll för TabButton beteckning
+ Introducera PlaceholderMessage
+ [calendar] Reducera månadsbeteckningens storlek
+ [ExpandableListItem] Använd samma logik för synlighet av åtgärder och pilknappar
+ [Dialog Shadows] Konvertera till KWindowSystem programmeringsgränssnitt för skuggor (fel 416640)
+ Skapa symbolisk länk för widgets/plasmoidheading.svgz i breeze ljus/mörk
+ Rätta Kirigami.Units.devicePixelRatio=1.3 när den ska vara 1.0 vid 96 punkter/tum
+ Lägg till egenskaper för att komma åt ExpandableListItem inläsningsobjekt

### Prison

+ Avråd från användning AbstractBarcode::minimumSize() också för kompilatorn

### Syfte

+ Använd explicit Kirigami enheter istället för att implicit använda Plasma enheter
+ Konvertera jobbdialogrutor till Kirigami.PlaceholderMessage

### QQC2StyleBridge

+ Ställ in selectByMouse till true för SpinBox
+ Rätta menyikoner som blir suddiga med vissa teckenstorlekar (fel 423236)
+ Förhindra delegater och rullningslister att överlappa i kombinationsrutors meddelanderutor
+ Rätta Slider vertikal implicitWidth och en bindningssnurra
+ Gör verktygstips mer konsekventa med Breeze komponentstil verktygstips
+ Ställ in editable till true som förval för SpinBox
+ Rätta Connections varning i Menu.qml

### Solid

+ Aktivera gränssnittet UDisks2 på FreeBSD ovillkorligt

### Sonnet

+ Rätta "Använd QCharRef med en indexpekare utanför det giltiga intervallet av en QString"
+ Återställ förvalt automatiskt detekteringsbeteende
+ Rätta förvalt språk (fel 398245)

### Syndikering

+ Rätta några BSD-2-Clause licenshuvuden

### Syntaxfärgläggning

+ CMake: Uppdatera för CMake 3.18
+ Lägg till färgläggning för språket Snort/Suricata
+ Lägg till språket YARA
+ ta bort binär json som avråds ifrån till förmån för cbor
+ JavaScript/TypeScript: färglägg etiketter i mallar

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
