---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nya ramverk

- KFileMetadata: Filmetadata- och extraheringsbibliotek
- Baloo: filindexering och sökramverk

### Ändringar som påverkar alla ramverk

- Versionskravet för Qt har knuffats från 5.2 till 5.3
- Felsökningsutmatning har konverterats till kategoriserad utmatning, vilket normalt ger mindre oväsen
- Docbook-dokumentation har granskats och uppdaterats

### Integrering med ramverk

- Rätta krasch i fildialogrutan för enbart kataloger
- Lita inte på options()-&gt;initialDirectory() för Qt &lt; 5.4

### KDE Doxygen-verktyg

- Lägg till manualsidor för kapidox-skript och uppdatera underhållsinformation i setup.py

### KBookmarks

- KBookmarkManager: Använd KDirWatch istället för QFileSystemWatcher för att detektera att user-places.xbel skapas.

### KCompletion

- Rättningar av KLineEdit/KComboBox för hög upplösning
- KLineEdit: Låt inte användaren ta bort text när radeditorn är skrivskyddad

### KConfig

- Rekommendera inte användning av föråldrat programmeringsgränssnitt
- Skapa inte föråldrad kod

### KCoreAddons

- Lägg till Kdelibs4Migration::kdeHome() för fall som inte täcks av resurser
- Rätta tr() varning
- Rätta byggning av KCoreAddons med Clang+ARM

### KDBusAddons

- KDBusService: dokumentera hur det aktiva fönstret höjs, i Activate()

### KDeclarative

- Rätta föråldrat anrop till KRun::run
- Samma beteende som MouseArea för att avbilda koordinater för filtrerade underliggande händelser
- Detektera att ursprunglig ansiktsikon skapas
- Uppdatera inte hela fönstret när plotter återges (fel 348385)
- Lägg till sammanhangsegenskapen userPaths
- Kvävs inte av tom QIconItem

### Stöd för KDELibs 4

- kconfig_compiler_kf5 flyttad till libexec, använd istället kreadconfig5 för testen findExe
- Dokumentera de (suboptimala) ersättningarna av KApplication::disableSessionManagement

### KDocTools

- Ändra mening om felrapportering, godkänd av dfaure
- Anpassa tyska user.entities till en/user.entities
- Uppdatera general.entities: Ändra taggar för ramverk och Plasma från program till produktnamn
- Uppdatera en/user.entities
- Uppdatera bok- och dokumentmallar
- Använd CMAKE_MODULE_PATH i cmake_install.cmake
- Fel: 350799 (fel 350799)
- Uppdatera general.entities
- Sök efter Perl-moduler som krävs
- Lägg till namnrymd för ett hjälpmakro i den installerade makrofilen.
- Anpassade översättningar av tangentnamn till standardöversättningarna tillhandahållna av Termcat

### KEmoticons

- Installera Breeze-tema
- Kemoticons: Gör Breeze-smilisar standard istället för Glass
- Breeze-smilispaket skapat av Uri Herrera

### KHTML

- Låt KHTML vara användbart utan att söka efter privata beroenden

### KIconThemes

- Ta bort tillfälliga strängtilldelningar.
- Ta bort felsökningsposten för tematräd

### KIdleTime

- Privata deklarationsfiler för plattformsinsticksprogram installeras.

### KIO

- Ta bort onödiga QUrl-omgivningar

### KItemModels

- Ny proxy: KExtraColumnsProxyModel, gör det möjligt att lägga till kolumner i en befintlig modell.

### KNotification

- Rätta Y-startposition för reservmeddelanderutor
- Reducera beroenden och flyttta till lager 2
- Fånga okända underrättelseposter (referens via nullpekare) (fel 348414)
- Ta bort i stort sett onödiga varningsmeddelanden

### Paketet Framework

- Gör undertexterna till undertexter
- kpackagetool: Rätta utmatning av icke latinsk text till standardutmatningen

### KPeople

- Lägg till AllPhoneNumbersProperty
- PersonsSortFilterProxyModel nu tillgänglig för användning i QML

### Kross

- krosscore: Installera deklarationsfil med blandat skiftläge "KrossConfig"
- Rätta Python2-tester för att köra med PyQt5

### KService

- Rätta kbuildsycoca --global
- KToolInvocation::invokeMailer: Rätta bilagor när det finns flera

### KTextEditor

- Skydda förvald loggnivå för Qt &lt; 5.4.0, rätta loggnamn för konkatenering
- Lägg till hl för Xonotic (fel 342265)
- Lägg till Groovy HL (fel 329320)
- Uppdatera J-färgläggning (fel 346386)
- Gör så att kompilering med MSVC2015 fungerar
- Mindre användning av ikonladdning, rätta flera taggiga ikoner 
- Aktivera eller inaktivera knappen Sök alla vid mönsterändringar
- Förbättrade raden Sök och ersätt
- Ta bort oanvändbar linjal från effektläge
- Smalare sökrad
- vi: Rätta felläsning av flaggan markType01
- Använd riktig kvalificering för att anropa basmetod.
- Ta bort kontroller, QMetaObject::invokeMethod skyddar redan sig själv.
- Rätta problem med färgväljare för hög upplösning
- Städa upp coe: QMetaObject::invokeMethod är säker för nullpekare.
- Fler kommentarer
- Ändra sättet som gränssnitten är säkra för null
- Mata bara normalt ut varningar och högre
- Ta bort att göra från det förgångna
- Använd QVarLengthArray för att spara den tillfälliga QVector-iterationen.
- Flytta fixen att indentera gruppbeteckningar till konstruktionstid.
- Rätta några allvarliga problem med KateCompletionModel i trädläge.
- Rätta felaktig modellkonstruktion som förlitade sig på Qt 4-beteende.
- Lyd umask-regler när en ny fil sparas (fel 343158)
- Lägg till meson färgläggning
- Eftersom Varnish 4.x introducerar diverse syntaxändringar jämfört med Varnish 3.x, har ytterligare, separata syntaxfärgläggningsfiler skrivits för Varnish 4 (varnish4.xml, varnishtest4.xml).
- Rätta problem med hög upplösning
- vimode: Krascha inte om kommandot &lt;c-e&gt; utförs i slutet av ett dokument (fel 350299).
- Stöd QML-strängar med flera rader.
- Rätta syntax för oors.xml
- Lägg till CartoCSS färgläggning av Lukas Sommer (fel 340756)
- Rätta flyttalsfärgläggning, använd inbyggd Float såsom i C (fel 348843)
- Dela riktningar blev omvänd (fel 348845)
- Fel 348317 - [PROGRAMFIX] Kate-delprogrammets syntaxfärgläggning ska känna igen undantagsstil med u0123 för Javascript (fel 348317)
- Lägg till *.cljs (fel 349844)
- Uppdatering av GLSL-färgläggningsfilen.
- Rättade standardfärger så att de är mer särskiljbara

### KTextWidgets

- Ta bort gammal färgläggning

### Ramverket KWallet

- Rätta byggning på Windows
- Skriv ut en varning med felkod när öppna plånboken via PAM misslyckas
- Returnera gränssnittets felkod istället för -1 när öppna en plånbok misslyckas
- Gör gränssnittets "okänt chiffer" till en negativ returkod
- Övervaka PAM_KWALLET5_LOGIN för KWallet5
- Rätta krasch när kontrollen MigrationAgent::isEmptyOldWallet() misslyckas
- Plånboken kan nu låsas upp av PAM med modulen kwallet-pam

### KWidgetsAddons

- Nytt programmeringsgränssnitt som accepterar QIcon-parametrar för att ställa in ikonerna i flikraden
- KCharSelect: Rättade unicode-kategori och använd boundingRect för breddberäkningar
- KCharSelect: Rätta cellbredd för att passa innehållet
- Marginaler för KMultiTabBar är nu ok på skärmar med hög upplösning
- KRuler: Avråd från användning av oimplementerad KRuler::setFrameStyle(), städa kommentarer
- KEditListWidget: Ta bort marginal så att den inriktas bättre med andra grafiska komponenter

### KWindowSystem

- Härda NETWM-dataläsning (fel 350173)
- Skydda för äldre Qt-versioner som i kio-http
- Privata deklarationsfiler för plattformsinsticksprogram installeras.
- Plattformsspecifika delar laddade som insticksprogram.

### KXMLGUI

- Rätta beteende hos metoden KShortcutsEditorPrivate::importConfiguration

### Plasma ramverk

- Man kan nu byta mellan olika zoomnivåer i kalender med en knipgest
- Kommentar om kodduplicering i ikondialogrutan
- Färgen på skjutreglagets fåra var hårdkodad, ändrad att använda färgschema
- Använd QBENCHMARK istället för ett hårt krav på datorns prestanda
- Navigering i kalendern har förbättats väsentligt, och tillhandahåller en årsöversikt och tioårsöversikt
- PlasmaCore.Dialog har nu egenskapen 'opacity'
- Lämna lite utrymme för alternativknappen
- Visa inte den cirkulära bakgrunden om det finns en meny
- Lägg till definitionen X-Plasma-NotificationAreaCategory
- Ställ in underrättelser och skärmmeddelanden att visas på alla skrivbord
- Skriv ut användbar varning när man inte kan hämta giltig KPluginInfo
- Rätta möjlig oändlig rekursion i PlatformStatus::findLookAndFeelPackage()
- Byt namn på software-updates.svgz till software.svgz

### Sonnet

- Lägg till delar i CMake för att göra det möjligt att bygga insticksprogrammet Voikko.
- Implementera Sonnet::Client för Voikko stavningskontroll.
- Implementera stavningskontroll baserad på Voikko (Sonnet::SpellerPlugin)

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
