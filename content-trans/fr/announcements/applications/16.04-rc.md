---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE publie la version candidate des applications de KDE en version 16.04
layout: application
release: applications-16.03.90
title: KDE publie la version candidate des applications de KDE en version 16.04
---
07 Avril 2016. Aujourd'hui, KDE a publié la version candidate des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Avec de nombreuses applications reposant sur l'environnement de développement version 5, les mises à jour des applications 16.04 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.

#### Installation des paquets binaires de la version candidate des applications KDE 16.04

<em>Paquets</em>Quelques fournisseurs de systèmes d'exploitation Linux / Unix fournissent gracieusement pour certaines versions de leurs distributions, des paquets binaires pour les applications 16.04 en version candidate (en interne 16.03.90). Dans d'autres cas, des bénévoles de la communauté le font aussi. Des paquets binaires supplémentaires, et également des mises à jour des paquets actuels, seront éventuellement mis à disposition dans les prochaines semaines.

<em>Emplacements des paquets</em>. Pour obtenir une liste à jour des paquets binaires disponibles, connus par l'équipe de publication KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Wiki de la communauté</a>.

#### Compilation de la version candidate 16.04 des applications KDE

Le code source complet de la version candidate des applications de KDE 16.04 peut être <a href='http://download.kde.org/unstable/applications/16.03.90/src/'> librement téléchargé</a>. Les instructions de compilation et d'installation sont disponibles sur la <a href='/info/applications/applications-16.03.90'>page d'informations de la version candidate des applications KDE</a>.
