---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE publie les applications de KDE en version 15.12.3
layout: application
title: KDE publie les applications de KDE en version 15.12.3
version: 15.12.3
---
15 Mars 2016. Aujourd'hui, KDE publie la troisième mise à jour de stabilisation pour les <a href='../15.12.0'>applications 15.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Plus de 15 corrections de bogues apportent des améliorations à kdepim, akonadi, ark, kblocks, kcalc, ktouch and umbrello et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.18 de KDE.
