---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE stelt KDE Applicaties 17.08.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.08.0 beschikbaar
version: 17.08.0
---
17 augustus 2017. KDE Applications 17.08 is hier. We er voor gezorgd dat zowel de toepassingen als de onderliggende bibliotheken stabieler zijn en gemakkelijker te gebruiken. Door plooien glad te strijken en te luisteren naar uw terugkoppeling, hebben we de suite KDE Applications minder gevoelig voor uitglijders gemaakt en veel vriendelijker. Veel plezier met uw nieuwe apps.

### Meer overgezet naar KDE Framework 5

We zijn blij dat de volgende toepassingen die gebaseerd waren op kdelibs4 nu gebaseerd zijn op KDE Frameworks 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat en umbrello. Dank aan de hardwerkende ontwikkelaars die hun tijd en en werk vrijwillig hebben gegeven om dit te laten gebeuren.

### Wat is er nieuw in KDE Applicaties 17.08

#### Dolphin

Dolphin ontwikkelaars rapporteren dat Dolphin nu 'Tijd van verwijderen' in de Prullenbak en 'Tijd van aanmaak' als het OS dat ondersteunt zoals in BSD's.

#### KIO-extras

Kio-Extras levert nu betere ondersteuning voor Samba-shares.

#### KAlgebra

De ontwikkelaars van KAlgebra hebben gewerkt aan verbeteren van hun Kirigami front-end op het bureaublad en hebben code-aanvulling geïmplementeerd.

#### Kontact

- In KMailtransport hebben ontwikkelaars ondersteuning opnieuw geactiveerd voor akonadi-transport, ondersteuning voor plug-ins gemaakt en ondersteuning voor en ondersteuning voor e-mailtransport via sendmail opnieuw gemaakt.
- In SieveEditor zijn veel bugs in autocreate scripts gerepareerd en gesloten. Samen met algemene reparatie van bugs is een regelbewerker voor reguliere expressies toegevoegd.
- In KMail is de mogelijkheid om een externe bewerker opnieuw gemaakt als een plug-in.
- De Akonadi-import-assistent heeft nu de 'converter converteer alles' als een plug-in, zodat ontwikkelaars nieuwe converters gemakkelijk kunnen maken.
- Toepassingen hangen nu af van Qt 5.7. Ontwikkelaars hebben heel wat fouten in de compilatie op Windows gerepareerd. Alles van kdepim compileert op Windows nog niet maar de ontwikkelaars hebben veel voortgang gemaakt. Om te beginnen hebben de ontwikkelaars er een craft-recept voor gemaakt. Veel reparaties zijn gedaan door code te moderniseren (C++11). Wayland ondersteuning op Qt 5.9. Kdepim-runtime voegt een facebook-hulpbron toe.

#### Kdenlive

In Kdenlive heeft het team het gebroken 'Freeze effect' gerepareerd. In recente versies was het onmogelijk het bevroren frame voor het freeze-effect te wijzigen. Nu is een sneltoets voor Extract Frame feature toegestaan. De gebruiker kan schermafdrukken van zijn tijdlijn met ee sneltoets en een naam wordt nu gesuggereerd gebaseerd op framenummer <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Gedownloadde transitie lumas verschijnen niet in interface: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Probeelm met geluidskliks (voor nu, vereist bouwen van de afhankelijkheid MLT uit git tot een MLT uitgave): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Ontwikkelaars hebben het overzetten van de X11 plug-in naar Qt5 voltooid en krfb werkt weer met een X11-backend die veel sneller is dan de Qt plug-in. Er is een nieuwe pagina Instellingen, waarmee de gebruiker de voorkeurs framebuffer-plug-in kan wijzigen.

#### Konsole

Konsole biedt nu onbeperkte terugrol tot verder dan de 2GB (32bit) limiet. Konsole biedt gebruikers om elke lokatie in te voeren voor opslaan van terugrolbestanden. Er is ook een regressie gerepareerd, Konsole kan opnieuw KonsolePart gebruiken om de dialoog Profiel beheren aan te roepen.

#### KAppTemplate

In KAppTemplate is er nu een optie om nieuwe sjablonen te installeren vanaf het bestandssysteem. Meer sjablonen zijn verwijderd uit KAppTemplate en in plaats daarvan geïntegreerd in gerelateerde producten; sjabloon voor ktexteditor-plug-in en plug-in voor kpartsapp (nu overgebracht naar Qt5/KF5) zijn onderdeel geworden van KDE Frameworks KTextEditor en KParts sinds 5.37.0. Deze wijzigingen zouden aanmaken van sjablonen in KDE toepassingen moeten vereenvoudigen.

### Op bugs jagen

Meer dan 80 bugs zijn opgelost in toepassingen inclusief de Kontact Suite, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole en meer!

### Volledige log met wijzigingen
