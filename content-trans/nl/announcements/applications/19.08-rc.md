---
aliases:
- ../announce-applications-19.08-rc
date: 2019-08-02
description: KDE stelt Applications 19.08 Release Candidate beschikbaar.
layout: application
release: applications-19.07.90
title: KDE stelt de Release Candidate van KDE Applicaties 19.08 beschikbaar
version_number: 19.07.90
version_text: 19.08 Release Candidate
---
2 augustus 2019. Vandaag heeft KDE de Release Candidate van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Kijk in de <a href='https://community.kde.org/Applications/19.08_Release_Notes'>uitgavenotities van de gemeenschap</a> voor informatie over tarballs en bekende problemen. Een meer complete aankondiging zal beschikbaar zijn voor de uiteindelijke uitgave

De uitgave KDE Applications 19.08 heeft grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritiek in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de uitgavekandidaat te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.
