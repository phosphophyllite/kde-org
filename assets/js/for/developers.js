function reveal() {
  const yakuake = document.querySelector(".yakuake");
  const windowHeight = window.innerHeight;
  const elementTop = yakuake.getBoundingClientRect().top;
  const elementVisible = 20;

  const yakuakeImg = document.querySelector(".yakuake figure");
  if (elementTop < windowHeight - elementVisible) {
    yakuakeImg.classList.add("active");
  } else {
    yakuakeImg.classList.remove("active");
  }
}

window.addEventListener("scroll", reveal);
